import os
from openai import OpenAI
import argparse

# Set up argument parsing
parser = argparse.ArgumentParser(description="Transcribe audio using OpenAI's Whisper model.")
parser.add_argument("filename", type=str, help="The path to the audio file (e.g., mp3) to be transcribed.")
args = parser.parse_args()

client = OpenAI()

# Get the OpenAI API key from the environment variable
client.api_key = os.getenv("OPENAI_API_KEY")

if client.api_key is None:
    raise ValueError("API key not found. Please set the OPENAI_API_KEY environment variable.")

audio_file= open(args.filename, "rb")
transcription = client.audio.transcriptions.create(
  model="whisper-1", # Per https://platform.openai.com/docs/api-reference/audio, this is the only available (and it's actually v2 large)
  file=audio_file,
  response_format="verbose_json",
  timestamp_granularities=["word"]
)

print(transcription)
