import argparse
import os

def process_subtitles(input_filename):
    """Process subtitle file to condense speeches by person into single paragraphs with initial timestamps."""
    with open(input_filename, 'r') as file:
        lines = file.readlines()

    # Initialize processed data storage
    processed_data = []
    current_person = None
    current_text = []
    current_timestamp = None

    for i, line in enumerate(lines):
        stripped_line = line.strip()

        # Check for timestamp line format (e.g., '00:00:00,06 --> 00:00:04,2')
        if '-->' in stripped_line:
            timestamp = stripped_line.split('-->')[0].strip()
            if current_timestamp is None:
                current_timestamp = timestamp  # Set initial timestamp for the new block

        # Skip index numbers and empty lines
        elif stripped_line.isdigit() or not stripped_line:
            continue
        
        # Check if the line is a speaker line
        elif ':' in stripped_line and any(char.isalpha() for char in stripped_line.split(':')[0]):
            # Extract the name to see if it's a new speaker
            new_person = stripped_line.split(':')[0].strip()

            # Check if this is a new speaker and save previous speaker's text
            if new_person != current_person and current_person is not None:
                processed_data.append(f"{current_timestamp}\n{current_person}: {' '.join(current_text)}")
                current_timestamp = timestamp  # Update timestamp for new speaker
                current_text = []  # Reset text list for new speaker
            
            current_person = new_person
            current_text.append(' '.join(stripped_line.split(':')[1:]).strip())

        else:
            # Continue accumulating text for the current person
            current_text.append(stripped_line)

    # Add the last processed chunk
    if current_person and current_timestamp:
        processed_data.append(f"{current_timestamp}\n{current_person}: {' '.join(current_text)}")

    return '\n\n'.join(processed_data)

def main():
    """Main function to handle file input and output through command line."""
    parser = argparse.ArgumentParser(description="Process and condense subtitles")
    parser.add_argument('-i', '--input', help="Input subtitle file name", required=True)
    parser.add_argument('-o', '--output', help="Output file name to save processed subtitles", required=True)
    args = parser.parse_args()

    if not os.path.exists(args.input):
        print(f"Error: The file {args.input} does not exist.")
        return

    # Process subtitles
    processed_output = process_subtitles(args.input)

    # Save the processed data to a file
    if processed_output.strip():
        with open(args.output, 'w') as file:
            file.write(processed_output)
            print(f"Processed subtitles saved to {args.output}")
    else:
        print("No data processed. Check the format of your input file.")

if __name__ == '__main__':
    main()
