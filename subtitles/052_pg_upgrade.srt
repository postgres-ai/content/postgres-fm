1
0:0:0,06 --> 0:0:1,0999999
And I'm alone again.

2
0:0:1,5 --> 0:0:3,54
Hello, hello, this is episode number
52.

3
0:0:3,9399998 --> 0:0:6,72
I screwed up a little bit in the
beginning because I forgot to

4
0:0:7,24 --> 0:0:9,3
unmute myself in Zoom.

5
0:0:10,16 --> 0:0:15,08
Thank you, live audience, which
I have in YouTube, who helped

6
0:0:15,08 --> 0:0:16,6
me to unmute myself.

7
0:0:17,1 --> 0:0:19,6
But unfortunately, I didn't notice
it in the beginning.

8
0:0:19,6 --> 0:0:21,18
But okay, let's proceed.

9
0:0:21,18 --> 0:0:26,3
So we have PGAppgrade, which is
kind of a very good tool to upgrade.

10
0:0:26,72 --> 0:0:31,1
Like in ancient times, we needed
to dump restore our database

11
0:0:31,24 --> 0:0:33,28
to perform a major upgrade.

12
0:0:33,82 --> 0:0:35,78
But now we have PgUpgrade, great.

13
0:0:36,28 --> 0:0:40,68
But actually, we discussed it with
Michael some time ago.

14
0:0:40,68 --> 0:0:43,34
We had an episode about minor and
major upgrades.

15
0:0:43,48 --> 0:0:47,96
We discussed high-level problems
that we usually have and so

16
0:0:47,96 --> 0:0:48,28
on.

17
0:0:48,28 --> 0:0:53,14
But let me discuss particular problems
when you use PageUpgrade

18
0:0:53,3 --> 0:0:55,16
to run major upgrades.

19
0:0:56,52 --> 0:1:3,68
So problem number 1, if you have
a huge database and some significant

20
0:1:3,74 --> 0:1:9,26
traffic to it, and you want to
avoid downtime, there is a well-known

21
0:1:9,56 --> 0:1:13,979996
idea, let's use logical to avoid
downtime.

22
0:1:15,06 --> 0:1:18,84
Of course, if you use Page Upgrade
as is, it will take significant

23
0:1:18,88 --> 0:1:19,38
time.

24
0:1:19,44 --> 0:1:22,26
And there is another recipe described
in the documentation to

25
0:1:22,26 --> 0:1:29,3
use the "-k", or "--link", option,
which significantly speeds

26
0:1:29,3 --> 0:1:32,38
up the process, because we switch
to using hard links.

27
0:1:32,92 --> 0:1:36,48
Most of files in Pagedata is not
going to change during upgrade,

28
0:1:36,58 --> 0:1:37,98
so hard links help.

29
0:1:39,86 --> 0:1:43,82
But some people say, okay, we don't
want even some minutes of

30
0:1:43,82 --> 0:1:48,04
like, it will take usually up to
few 10 minutes or 15 minutes,

31
0:1:48,04 --> 0:1:51,52
depending on cluster size and additional
steps you perform.

32
0:1:52,58 --> 0:1:57,479996
They say, we want to do it faster,
let's involve logical replication.

33
0:1:57,86 --> 0:2:0,12
And here we have a couple of dangers.

34
0:2:1,16 --> 0:2:5,28
First of all, if cluster is really
huge, it's very challenging

35
0:2:5,28 --> 0:2:7,28
to initialize Logical Replica.

36
0:2:8,22 --> 0:2:13,3
Because when you initialize it
in a traditional way, it's fully

37
0:2:13,3 --> 0:2:13,8
automated.

38
0:2:13,98 --> 0:2:20,52
If you just create a replication
subscription and start a subscription

39
0:2:20,6 --> 0:2:26,18
to a new empty cluster, if it's
already a new Postgres, great,

40
0:2:26,18 --> 0:2:29,44
it will be done automatically,
but you will find it very challenging

41
0:2:29,44 --> 0:2:33,58
for really big databases, like
for example, 5, 10, 15 terabytes

42
0:2:33,7 --> 0:2:39,76
under some significant load, like
at least thousands or a few

43
0:2:39,76 --> 0:2:41,92
dozens of thousands of TPS.

44
0:2:42,7 --> 0:2:43,94
And why is it so?

45
0:2:43,94 --> 0:2:47,96
Because initialization at a logical
level involves basically

46
0:2:47,96 --> 0:2:53,12
a kind of dump-restore process,
which takes long, and also it

47
0:2:53,12 --> 0:2:57,26
affects the health of the source
cluster, which currently is

48
0:2:57,26 --> 0:2:59,48
your main cluster under use, right?

49
0:3:1,06 --> 0:3:5,74
And before Postgres 16, which is
not yet released, yesterday

50
0:3:5,74 --> 0:3:13,64
we had only beta 2, and also the
branch RL16 stable was stamped,

51
0:3:13,98 --> 0:3:18,06
meaning that we're already close
to release, but still not there.

52
0:3:18,54 --> 0:3:23,24
And PostgreSQL 16 will allow you
to logically replicate from

53
0:3:23,24 --> 0:3:23,74
standbys.

54
0:3:23,86 --> 0:3:25,12
Right now it's not possible.

55
0:3:25,52 --> 0:3:29,6
So it's possible only from primary,
but even if it's from standby,

56
0:3:30,1 --> 0:3:31,38
the problem will remain.

57
0:3:31,64 --> 0:3:37,54
So you affect how the vacuum workers
work and they cannot remove

58
0:3:38,1 --> 0:3:42,84
freshly dead tuples because you're
still taking data and you

59
0:3:42,84 --> 0:3:44,34
need that snapshot.

60
0:3:44,44 --> 0:3:50,32
So After the moment you started,
if some tuples became dead,

61
0:3:50,38 --> 0:3:53,9
Autovacuum cannot remove that tuples,
and you will see in logs

62
0:3:53,9 --> 0:3:58,02
that some number of tuples are
dead but cannot yet be removed.

63
0:3:58,32 --> 0:3:58,68
Why?

64
0:3:58,68 --> 0:4:2,36
Because we still hold snapshot,
because we're still initializing

65
0:4:2,52 --> 0:4:3,22
our replica.

66
0:4:3,58 --> 0:4:7,9
And sometimes it's even not possible
to finish.

67
0:4:8,32 --> 0:4:11,84
Like, on the primary you produce
a lot of load, so it's kind

68
0:4:11,84 --> 0:4:13,16
of difficult.

69
0:4:13,66 --> 0:4:15,92
And I actually didn't do it myself,
never.

70
0:4:15,92 --> 0:4:19,84
I just know it feels not right
approach, but my colleagues did

71
0:4:19,84 --> 0:4:25,52
and they tried and they fit, even
on smaller database, like about

72
0:4:25,52 --> 0:4:26,26
1 terabyte.

73
0:4:27,44 --> 0:4:29,34
So how to do it better?

74
0:4:29,34 --> 0:4:30,18
There's a recipe.

75
0:4:30,18 --> 0:4:31,66
So we call it physical-to-logical.

76
0:4:32,9 --> 0:4:35,94
And it's quite straightforward.

77
0:4:38,3 --> 0:4:41,66
We can first of all have a secondary
cluster.

78
0:4:42,66 --> 0:4:45,3
I'm going to use Patroni terminology
here.

79
0:4:45,36 --> 0:4:48,92
So we have a secondary cluster
with its own standby leader.

80
0:4:49,28 --> 0:4:54,14
They are all standbys, but 1 of
them is leader because we have

81
0:4:54,14 --> 0:4:55,14
cascaded replication.

82
0:4:55,64 --> 0:4:59,98
So standby leader is replicating
using stimuli replication from

83
0:5:0,46 --> 0:5:1,6
main primary.

84
0:5:2,08 --> 0:5:5,26
And then it has its own standby
nodes, cascaded replication.

85
0:5:5,98 --> 0:5:9,86
And Patroni can do that, you can
say I'm having a secondary cluster

86
0:5:9,86 --> 0:5:13,94
here, so it shouldn't have a real
primary, but it has its own

87
0:5:13,94 --> 0:5:14,88
standby leader.

88
0:5:16,24 --> 0:5:20,4
This streaming replication is asynchronous,
normal streaming

89
0:5:20,44 --> 0:5:20,94
replication.

90
0:5:21,66 --> 0:5:27,1
And then we create a slot and we
create publication on the source,

91
0:5:27,1 --> 0:5:28,54
on the primary, as I said.

92
0:5:28,94 --> 0:5:33,78
And once we do it, walls are starting
to accumulate on the primary

93
0:5:33,78 --> 0:5:34,44
more and more.

94
0:5:34,44 --> 0:5:39,92
If we keep this slot unused, we
know we will be out of disk space

95
0:5:39,92 --> 0:5:42,5
on the main primary, which we are
using right now.

96
0:5:42,5 --> 0:5:46,42
So we should move fast, or we should
have a lot of disk space.

97
0:5:46,64 --> 0:5:49,78
And of course, it's not good to
spend a lot of time accumulating

98
0:5:50,08 --> 0:5:51,98
this lag basically, right?

99
0:5:52,38 --> 0:5:55,58
If no 1 is using this slot, we
have LSN position.

100
0:5:55,76 --> 0:5:59,7
We know LSN position when we create
a slot using SQL approach.

101
0:6:0,04 --> 0:6:3,4
You can do it in 2 ways, but SQL
approach is fine as well here.

102
0:6:3,4 --> 0:6:6,42
So you say, select pg, create replication
slot, it will return

103
0:6:6,42 --> 0:6:7,08
you lsn.

104
0:6:7,96 --> 0:6:10,94
So the slot is at this position.

105
0:6:11,18 --> 0:6:11,68
Good.

106
0:6:12,34 --> 0:6:13,64
Now we have a trick.

107
0:6:13,66 --> 0:6:18,62
We, on our future primary, which
is currently a standby leader.

108
0:6:19,02 --> 0:6:21,0
We just adjust configuration.

109
0:6:21,42 --> 0:6:27,88
We say, instead of replicating
everything using physical replication,

110
0:6:29,18 --> 0:6:33,34
we have a setting which is called
recovery target LSN.

111
0:6:34,02 --> 0:6:38,94
And we put this LSN from the slot
position, we put it right there,

112
0:6:38,94 --> 0:6:41,96
restart server, and let it catch
up and pause.

113
0:6:42,26 --> 0:6:45,26
There is another setting, I don't
remember the name, which says

114
0:6:45,92 --> 0:6:51,18
which action to perform when something
like this is achieved.

115
0:6:51,18 --> 0:6:53,86
Like, okay, we achieved this LSN
and we pause.

116
0:6:54,96 --> 0:6:55,46
Great.

117
0:6:55,46 --> 0:7:0,46
So, and not replicating at physical
level anything additional.

118
0:7:0,82 --> 0:7:5,24
By the way, we found that if you
use wall shipping here, sometimes

119
0:7:5,6 --> 0:7:8,54
it replaces slightly more than
needed.

120
0:7:9,18 --> 0:7:11,1
So we need to explore this probably.

121
0:7:11,12 --> 0:7:15,14
But if you use streaming replication,
this works really good.

122
0:7:15,8 --> 0:7:21,14
So At this point, we can shut down.

123
0:7:22,28 --> 0:7:23,4
And here we have a trick.

124
0:7:23,4 --> 0:7:25,24
Here we have a problem, number
2.

125
0:7:25,24 --> 0:7:29,9
So first of all, this already allows
us to switch from physical

126
0:7:29,9 --> 0:7:31,04
replication to logical.

127
0:7:31,84 --> 0:7:36,36
And This is how we can very quickly
initialize logical replica

128
0:7:36,82 --> 0:7:41,88
based on physical replica in cases
when you need all of database

129
0:7:41,88 --> 0:7:44,04
or majority of tables to be replicated.

130
0:7:44,04 --> 0:7:49,24
Of course, with logical you can
replicate only some tables, and

131
0:7:49,24 --> 0:7:52,54
this recipe is not good in this
case, if you need only, for example,

132
0:7:52,54 --> 0:7:54,3
10% of all your tables.

133
0:7:54,62 --> 0:8:1,66
But if you need everything, and
In the case of a major upgrade,

134
0:8:1,96 --> 0:8:2,94
we need everything.

135
0:8:3,16 --> 0:8:6,88
In this case, this physical to
logical recipe is really good.

136
0:8:8,18 --> 0:8:10,61
But we have now a second problem
here.

137
0:8:10,61 --> 0:8:12,68
The first problem we already solved,
right?

138
0:8:12,98 --> 0:8:18,42
Instead of regular insertion, we
use physical replicants, which

139
0:8:18,42 --> 0:8:19,64
are physical to logical.

140
0:8:19,64 --> 0:8:26,54
By the way, when we create a slot,
when we create a subscription,

141
0:8:27,88 --> 0:8:31,36
we need to say explicitly that
we already have everything.

142
0:8:31,4 --> 0:8:32,62
So we just need CDC.

143
0:8:32,72 --> 0:8:34,52
Logical consists of 2 stages.

144
0:8:34,7 --> 0:8:35,76
First, 2 steps.

145
0:8:35,76 --> 0:8:41,06
First is full initialization, so
bring snapshot of data.

146
0:8:41,26 --> 0:8:45,92
And second is CDC, change data
capture, so replaying changes

147
0:8:45,92 --> 0:8:46,62
from slot.

148
0:8:47,04 --> 0:8:50,54
So in case of physical to logical
conversion, we don't need the

149
0:8:50,54 --> 0:8:53,3
first 1 because physical already
brought us everything.

150
0:8:53,56 --> 0:8:56,52
And how we initialized replica,
physical replica doesn't matter.

151
0:8:56,52 --> 0:9:2,06
It could be a PGBase backup, it
could be PGBackrest or VolG fetching

152
0:9:2,62 --> 0:9:4,2
whole database from backups.

153
0:9:4,6 --> 0:9:7,66
It could be cloud snapshots or
anything else.

154
0:9:8,44 --> 0:9:10,94
So now we have a second problem.

155
0:9:11,72 --> 0:9:13,52
This problem is quite interesting.

156
0:9:16,54 --> 0:9:17,66
It feels like this.

157
0:9:17,74 --> 0:9:24,14
If you first initialize logical
replication and then shut down

158
0:9:24,78 --> 0:9:29,66
your logical replica, run pgupgrade
on it and start again, there

159
0:9:29,66 --> 0:9:31,18
are risks of data corruption.

160
0:9:31,72 --> 0:9:38,76
This is quite interesting and it
was discussed in Hacker's mailing

161
0:9:38,76 --> 0:9:41,42
list few months ago.

162
0:9:41,42 --> 0:9:44,8
There is a discussion, I'm going
to provide a link in the description.

163
0:9:46,6 --> 0:9:48,54
So it's quite kind of interesting.

164
0:9:48,66 --> 0:9:54,5
So when we run pgUpgrade, it doesn't
play with logical well.

165
0:9:55,08 --> 0:10:0,2
Instead of this, let's just swap
the order of our actions.

166
0:10:0,44 --> 0:10:5,86
We know that our future primary
is at the position of the slot.

167
0:10:6,06 --> 0:10:9,9
But instead of starting to use
logical replication right away,

168
0:10:10,24 --> 0:10:13,4
let's postpone it and first let's
run PgUpgrade.

169
0:10:14,24 --> 0:10:21,02
We run PgUpgrade, we start our
new cluster, which has all data.

170
0:10:21,26 --> 0:10:25,06
And we know it was frozen at the
position we need.

171
0:10:25,38 --> 0:10:27,26
It's synchronized with the slot,
right?

172
0:10:27,44 --> 0:10:31,56
So we started, it has timeline
1 or 2, So it's kind of like a

173
0:10:31,56 --> 0:10:32,46
fresh database.

174
0:10:33,64 --> 0:10:38,5
And then we can, after it, we can
start using the logical replication,

175
0:10:38,68 --> 0:10:43,86
create subscription, and of course,
during the PGA upgrade, the

176
0:10:43,86 --> 0:10:47,86
slot will accumulate some lag,
because the LSN position is frozen,

177
0:10:48,26 --> 0:10:49,2
it's behind.

178
0:10:49,34 --> 0:10:54,84
So of course, PgUpgrade should
be done as fast as possible, for

179
0:10:54,84 --> 0:10:57,84
example, involving hardlinks, the
option "-k".

180
0:10:58,7 --> 0:11:0,56
In this case, it will take a few
minutes.

181
0:11:3,48 --> 0:11:8,0
We know that in new cluster after
PgUpgrade we don't have statistics,

182
0:11:8,04 --> 0:11:9,44
so we need to run analyze.

183
0:11:10,52 --> 0:11:14,56
I would say it's not a good idea
to run analyze right away.

184
0:11:15,06 --> 0:11:20,1
We can start logical first and
run analyze logical subscription

185
0:11:20,16 --> 0:11:24,4
first, start this CDC process,
change that capture process.

186
0:11:25,2 --> 0:11:29,62
And in parallel to run analyze
using many workers, for example,

187
0:11:29,62 --> 0:11:34,9
vacuumdb, dash dash analyze, dash
J, some, maybe even number

188
0:11:34,9 --> 0:11:37,62
of process, course we have.

189
0:11:37,96 --> 0:11:43,26
And if disks are very good, SSD
and so on, it's, it should be

190
0:11:43,26 --> 0:11:43,76
fine.

191
0:11:44,34 --> 0:11:49,24
And during that we already, the
logical application already working,

192
0:11:49,64 --> 0:11:50,94
it's catching up.

193
0:11:51,22 --> 0:11:55,22
We don't have this risk of data
corruption discussed in the hackers

194
0:11:55,24 --> 0:11:56,1
mailing list.

195
0:11:57,38 --> 0:12:2,74
And for logical, Of course, all
tables should have primary key

196
0:12:2,78 --> 0:12:5,82
or replica identity full.

197
0:12:6,58 --> 0:12:9,84
If we have primary key, we don't
need actually statistics to

198
0:12:9,84 --> 0:12:10,76
work with primary key.

199
0:12:10,76 --> 0:12:15,92
So all inserts, updates, deletes
will work even before we collect

200
0:12:15,92 --> 0:12:16,42
statistics.

201
0:12:16,44 --> 0:12:20,14
So we can run analyze in parallel
or slightly later and so on,

202
0:12:20,14 --> 0:12:21,1
it will be fine.

203
0:12:21,22 --> 0:12:27,14
We just shouldn't run regular user
queries on this node yet,

204
0:12:27,34 --> 0:12:30,28
until we collect statistics.

205
0:12:30,72 --> 0:12:32,8
So in this case, this is a good
recipe.

206
0:12:33,58 --> 0:12:38,24
But there is another problem, and
this problem is not only related

207
0:12:38,24 --> 0:12:40,68
to this recipe involving logical.

208
0:12:41,52 --> 0:12:44,76
By the way, I forgot to mention
that a couple of weeks ago a

209
0:12:44,76 --> 0:12:50,78
new tool was published, which is
written in Ruby, and it allows

210
0:12:50,86 --> 0:12:56,6
to automate the process involving
logical and run Postgres measure

211
0:12:56,6 --> 0:12:57,94
upgrade using logical.

212
0:12:58,22 --> 0:13:1,74
It was discussed on Hacker News,
It was discussed on Twitter,

213
0:13:1,86 --> 0:13:8,32
and at least 5 people sent me this
tool because they know I'm

214
0:13:8,32 --> 0:13:10,08
interested in Postgres upgrades.

215
0:13:10,58 --> 0:13:11,4
Thank you all.

216
0:13:15,26 --> 0:13:18,12
But I think This tool is okay,
I think.

217
0:13:18,82 --> 0:13:22,76
I briefly checked it and I didn't
see this physical to logical

218
0:13:22,82 --> 0:13:23,32
conversion.

219
0:13:23,36 --> 0:13:26,82
So it means that if you have a
really big database, probably

220
0:13:26,82 --> 0:13:28,38
this tool won't work well.

221
0:13:28,78 --> 0:13:36,34
And second, I'm not sure the order
of actions, so you should

222
0:13:36,34 --> 0:13:37,0
be careful.

223
0:13:37,2 --> 0:13:40,48
Because if order of actions logical
first, then upgrade and continue

224
0:13:40,48 --> 0:13:42,54
logical, there are risks of corruption.

225
0:13:43,14 --> 0:13:47,16
So I would encourage you to check
this, explore this, and communicate

226
0:13:47,16 --> 0:13:49,34
with author, maybe I will do it
as well.

227
0:13:49,94 --> 0:13:53,2
It's kind of very tricky parts
and dangerous parts.

228
0:13:53,24 --> 0:13:56,74
That's why I call this episode
PgUpgrade Tricky and Dangerous

229
0:13:56,8 --> 0:13:57,3
Parts.

230
0:13:58,1 --> 0:14:3,54
So, finally, Let's discuss the
final, maybe the most dangerous

231
0:14:3,54 --> 0:14:4,04
part.

232
0:14:4,22 --> 0:14:6,14
Because it's not only about logical.

233
0:14:6,88 --> 0:14:9,82
It's about anyone who uses PIDGY-upgrade.

234
0:14:11,12 --> 0:14:16,76
Even, like, okay, most of people
use with hard links, with option-k

235
0:14:17,12 --> 0:14:18,22
right now, or dash-link.

236
0:14:19,74 --> 0:14:21,96
But it's not even about that.

237
0:14:22,92 --> 0:14:26,52
The problem is related to how we
upgrade standby nodes.

238
0:14:27,7 --> 0:14:33,0
And Postgres documentation describes
the recipe involving rsync.

239
0:14:33,62 --> 0:14:41,04
It says, okay, once you upgraded
the primary, to upgrade the

240
0:14:41,04 --> 0:14:45,04
standbys, which already have some
data, just run rsync to eliminate

241
0:14:45,06 --> 0:14:49,08
deviation we accumulated during
running PgUpgrade.

242
0:14:49,28 --> 0:14:50,02
And that's it, right?

243
0:14:50,02 --> 0:14:51,0
It's quite obvious.

244
0:14:51,68 --> 0:14:56,26
But the problem is, if you really
want to minimize downtime,

245
0:14:56,82 --> 0:15:1,28
in case of without logical, in
downtime, usually a few minutes,

246
0:15:1,28 --> 0:15:4,9
we want to achieve a few minutes
even with clusters huge, many

247
0:15:4,9 --> 0:15:7,74
many many terabytes, we want just
a few minutes of downtime.

248
0:15:8,42 --> 0:15:14,92
If you want to minimize it, you
cannot use rsync in best mode.

249
0:15:14,92 --> 0:15:17,86
A best mode for rsync would be
checksum.

250
0:15:18,24 --> 0:15:23,7
So rsync checksum would check the
content of all files.

251
0:15:24,06 --> 0:15:25,98
But it would take a lot.

252
0:15:26,4 --> 0:15:31,78
Maybe it's easier to recreate a
replica than just wait until

253
0:15:31,78 --> 0:15:33,46
rsync checksum is working.

254
0:15:33,52 --> 0:15:36,3
Unfortunately, rsync is a single-threaded
thing.

255
0:15:36,6 --> 0:15:40,74
So if you want to benefit from
the fact that you have fast disks

256
0:15:40,74 --> 0:15:46,58
and a lot of cores, you need to
parallelize it somehow yourself.

257
0:15:47,14 --> 0:15:49,76
Maybe with GNU parallel or something
like that.

258
0:15:49,76 --> 0:15:51,76
So it's like kind of a task.

259
0:15:52,2 --> 0:15:54,06
And documentation doesn't describe
it.

260
0:15:54,92 --> 0:16:1,5
But if you just follow the documentation,
it says rsync.

261
0:16:1,84 --> 0:16:2,34
Okay.

262
0:16:2,44 --> 0:16:3,38
There's another option.

263
0:16:3,38 --> 0:16:6,52
So if you don't specify checksum,
you just use rsync.

264
0:16:6,9 --> 0:16:11,2
It will compare 2 things, size
of the file and modification time.

265
0:16:11,2 --> 0:16:15,72
It's much better, but still it's
a question to me, will it be

266
0:16:15,72 --> 0:16:16,22
reliable?

267
0:16:17,02 --> 0:16:21,74
But the least reliable approach
is to use the option size only.

268
0:16:21,88 --> 0:16:23,9
So we will compare only size.

269
0:16:24,64 --> 0:16:29,6
And in this case, guess what documentation
describes?

270
0:16:29,6 --> 0:16:31,16
Documentation describes size only.

271
0:16:32,04 --> 0:16:39,88
And imagine some table, which is
quite big, say 100 gigabytes.

272
0:16:40,8 --> 0:16:47,22
Postgres stores tables and indexes
using files, 1 gigabyte files,

273
0:16:47,46 --> 0:16:49,16
many, many files, 1 gigabyte files.

274
0:16:49,16 --> 0:16:53,98
You can find them using OID of
table or index, and you can see

275
0:16:54,02 --> 0:16:58,4
OID.1, OID.2, so many files, all
of them are 1 gigabyte.

276
0:16:59,18 --> 0:17:5,28
So if some writes happened during
your upgrade, which you are

277
0:17:5,28 --> 0:17:11,54
not propagated to standbys, and
then you follow Postgres documentation

278
0:17:12,04 --> 0:17:16,1
recipe, official recipe, which
is currently official, and just

279
0:17:16,1 --> 0:17:18,5
run rsync size only, as it describes.

280
0:17:19,46 --> 0:17:23,84
Rsync size only, we'll see, okay,
we have blah blah blah dot

281
0:17:23,84 --> 0:17:24,64
to file.

282
0:17:25,16 --> 0:17:28,12
1 gigabyte on source, 1 gigabyte
on target, no difference.

283
0:17:31,56 --> 0:17:39,24
So, this is super interesting,
because I suspect many of the

284
0:17:39,24 --> 0:17:44,02
clusters upgraded recently, or
maybe not that recently, have

285
0:17:44,02 --> 0:17:44,94
corrupted standbys.

286
0:17:47,22 --> 0:17:51,84
And we had it also with some quite
big database last week.

287
0:17:52,12 --> 0:17:56,04
We upgraded using our logical recipe,
this kind of magic and

288
0:17:56,04 --> 0:17:56,69
so on.

289
0:17:56,69 --> 0:18:4,62
And fortunately, we saw corruption
on replicas, on new replicas,

290
0:18:5,02 --> 0:18:9,44
when we moved read-only traffic,
user traffic, there, and we

291
0:18:9,44 --> 0:18:13,14
started seeing corruption errors
in logs.

292
0:18:13,14 --> 0:18:16,64
Corruption errors are very important
to monitor, and we have

293
0:18:16,64 --> 0:18:19,64
3 error codes.

294
0:18:19,64 --> 0:18:21,38
Postgres has 3 error codes.

295
0:18:21,38 --> 0:18:25,4
You should always monitor and have
alerts on each.

296
0:18:27,72 --> 0:18:31,72
It's xx001, xx002.

297
0:18:32,48 --> 0:18:35,46
This is the very bottom of the
error code page.

298
0:18:35,46 --> 0:18:37,32
In the documentation you can find
this.

299
0:18:37,54 --> 0:18:39,0
And the last 3 lines.

300
0:18:39,24 --> 0:18:41,5
This is the most...

301
0:18:45,06 --> 0:18:47,3
You should have alerts on this,
definitely.

302
0:18:48,1 --> 0:18:51,98
But a funny thing during major
operations, such as major upgrade,

303
0:18:51,98 --> 0:18:54,36
you probably silence all alerts.

304
0:18:54,52 --> 0:18:59,12
So you need to have manual checks
as well of logs.

305
0:18:59,6 --> 0:19:1,42
And This is how we found out.

306
0:19:2,8 --> 0:19:6,94
Accidentally we were checking logs
and saw a couple of lines

307
0:19:6,94 --> 0:19:9,64
and we saw this, wow, it looks
like corruption.

308
0:19:9,64 --> 0:19:14,98
And then we found out that all
standbys, we had maybe 5 standbys,

309
0:19:16,56 --> 0:19:21,06
for some files, 1 gigabyte files,
have slightly different content

310
0:19:21,06 --> 0:19:23,0
rather than new future primary.

311
0:19:23,16 --> 0:19:24,22
We rolled back.

312
0:19:24,64 --> 0:19:28,84
So fortunately, no data loss, nothing
like that happened.

313
0:19:29,06 --> 0:19:34,02
So we had only a few errors, like
30 maybe errors or so for user

314
0:19:34,02 --> 0:19:36,1
traffic and that's it, which is
great.

315
0:19:36,6 --> 0:19:40,24
But then I started to think, this
is official recipe.

316
0:19:41,04 --> 0:19:44,8
And the documentation, this recipe,
how to perform upgrades,

317
0:19:44,8 --> 0:19:49,02
it mentions that you should check
with pg-control-data, you should

318
0:19:49,02 --> 0:19:56,36
check the last checkpoint position
when you stop standby and

319
0:19:56,48 --> 0:20:0,54
primary on the future cluster,
new cluster.

320
0:20:1,12 --> 0:20:7,84
But it says something like, they
might have differences if, for

321
0:20:7,84 --> 0:20:12,62
example, you stopped standbys first
or standbys are still running,

322
0:20:12,62 --> 0:20:13,72
something like that.

323
0:20:13,78 --> 0:20:17,58
But it doesn't say that you shouldn't
proceed if you see a difference,

324
0:20:17,64 --> 0:20:18,5
first of all.

325
0:20:18,74 --> 0:20:19,94
And it doesn't mention...

326
0:20:21,2 --> 0:20:25,46
Right now, the connection is very,
very uncertain about the order

327
0:20:25,46 --> 0:20:29,76
of how we should stop servers and
how we should proceed with

328
0:20:29,76 --> 0:20:33,12
this dangerous rsync size only.

329
0:20:33,66 --> 0:20:37,06
So that's why I'm thinking many
clusters were upgraded like that.

330
0:20:37,06 --> 0:20:40,44
And if you don't have monitoring
of corruption, probably you

331
0:20:40,44 --> 0:20:42,66
still have corruption on standby
nodes.

332
0:20:42,72 --> 0:20:44,1
It sounds very dangerous.

333
0:20:44,88 --> 0:20:49,12
I wrote to hackers yesterday, to
the mailing list, and Robert

334
0:20:49,12 --> 0:20:53,68
has already admitted that this
recipe looks not right, it should

335
0:20:53,68 --> 0:20:54,34
be improved.

336
0:20:54,88 --> 0:20:57,94
But let's discuss how to do it
better.

337
0:20:59,14 --> 0:21:3,56
Thinking how to do it better, talking
to guys and to Andrei Borodin

338
0:21:3,56 --> 0:21:12,16
as well, checking some interesting
slides.

339
0:21:12,8 --> 0:21:17,8
Standby leader, our new primary,
which was already upgraded before

340
0:21:17,8 --> 0:21:18,24
upgrade.

341
0:21:18,24 --> 0:21:24,36
If we keep it running, and when
we keep running also all standbys,

342
0:21:24,38 --> 0:21:30,24
and then we shut down primary,
but don't shut down all its standbys,

343
0:21:30,58 --> 0:21:34,3
Meaning that we need to disable
autofillover, such as Patroni,

344
0:21:34,9 --> 0:21:36,46
to put it to maintenance mode.

345
0:21:36,46 --> 0:21:39,86
We shouldn't allow autofillover
to happen in this case.

346
0:21:40,12 --> 0:21:43,62
But the idea is we keep standbys
running and allow them to fully

347
0:21:43,62 --> 0:21:45,14
catch up all lags.

348
0:21:46,02 --> 0:21:50,78
When you shut down some server,
primary, a wall sender has some

349
0:21:50,78 --> 0:21:56,28
logic to make an attempt to write
everything to standbys, to

350
0:21:56,28 --> 0:21:58,54
send all changes before shutting
down.

351
0:21:58,86 --> 0:22:0,84
And then we also can involve pgControlData.

352
0:22:1,56 --> 0:22:3,9
So the idea is let's keep standbys
running.

353
0:22:4,34 --> 0:22:7,5
We check also with pg-control data
checkpoint position, which

354
0:22:7,5 --> 0:22:8,14
is good.

355
0:22:8,8 --> 0:22:13,78
Then we upgrade primary, and then
probably we still keep running

356
0:22:13,78 --> 0:22:14,28
standbys.

357
0:22:14,5 --> 0:22:19,04
And then probably 1 by 1, we shut
1 down, do rsync, or probably

358
0:22:19,04 --> 0:22:23,9
we shut all of them down and rsync
in parallel with all nodes.

359
0:22:24,58 --> 0:22:30,74
And this recipe actually I saw
in Yandex.Mail presentation, and

360
0:22:30,74 --> 0:22:35,3899
I learned yesterday that this recipe
with some work was done

361
0:22:35,3899 --> 0:22:35,865
from...

362
0:22:35,865 --> 0:22:40,7
Some work was coming from the guys
from Yandex Mail in the past,

363
0:22:40,76 --> 0:22:45,18
related to the use of hard links
and vSAR snippet and so on.

364
0:22:45,18 --> 0:22:51,42
But I think it's super important
to ensure that standbys received

365
0:22:51,42 --> 0:22:56,18
all changes from primary, and if
they didn't, probably you will

366
0:22:56,18 --> 0:22:56,82
have corruption.

367
0:22:57,28 --> 0:22:58,4
Actually that's it.

368
0:22:58,86 --> 0:23:1,6
I hope it was interesting, dangerous
enough.

369
0:23:2,46 --> 0:23:3,54
Dangerous I mean...

370
0:23:5,34 --> 0:23:10,22
The lesson here is definitely to
have monitoring and alerting

371
0:23:10,28 --> 0:23:16,82
for corruption errors, XXOO1, XXOO1
or XXOO2, these 3 errors

372
0:23:17,78 --> 0:23:20,78
probably involve some additional
checks.

373
0:23:21,0 --> 0:23:27,62
For example, using Amtcheck, my
colleagues learned that Amtcheck

374
0:23:27,62 --> 0:23:33,48
also found corruption on replicas
if you run it, because there

375
0:23:33,48 --> 0:23:37,86
was a deviation between files in
index and files in data.

376
0:23:38,76 --> 0:23:44,14
And then also, when you run pgUpgrade,
the safest, I think, is

377
0:23:44,14 --> 0:23:45,66
just to initialize the replicas.

378
0:23:45,66 --> 0:23:49,46
If you can afford it, just re-initialize
it from scratch, avoiding

379
0:23:49,64 --> 0:23:50,14
this.

380
0:23:50,5 --> 0:23:58,48
But if you cannot afford it, at
least be very careful with this

381
0:23:59,2 --> 0:24:2,54
risk of deviation.

382
0:24:3,1 --> 0:24:10,96
Or maybe run rsync without data
size only option, because this

383
0:24:10,96 --> 0:24:12,78
size only option definitely doesn't...

384
0:24:13,08 --> 0:24:17,86
I cannot guarantee that without
this option, with default behavior

385
0:24:17,86 --> 0:24:21,42
of rsync, when it checks size and
modification time, you will

386
0:24:21,42 --> 0:24:21,88
be fine.

387
0:24:21,88 --> 0:24:23,14
I cannot say yet.

388
0:24:23,36 --> 0:24:25,32
So I think some work should be
done.

389
0:24:25,32 --> 0:24:29,32
And if you have ideas, please join
the discussion on the hackers

390
0:24:29,64 --> 0:24:31,6
mailing list, which I started yesterday.

391
0:24:32,28 --> 0:24:37,04
So yeah, let me check a couple
of questions I have in chat because

392
0:24:37,04 --> 0:24:38,18
it's a live session.

393
0:24:39,92 --> 0:24:43,26
Our, our expecting to have modification
and official documentation

394
0:24:43,26 --> 0:24:45,64
to ensure safer upgrades in old
means.

395
0:24:45,9 --> 0:24:47,62
Well, Postgres is open source.

396
0:24:47,62 --> 0:24:49,3
You can propose your own modification.

397
0:24:49,64 --> 0:24:53,3
I'm still thinking I have questions
here, more than answers.

398
0:24:53,4 --> 0:24:56,54
Once I have some answer, I'm definitely
going to propose if I

399
0:24:56,54 --> 0:25:2,48
have something, some idea which
how to improve, but I guess the

400
0:25:2,48 --> 0:25:5,58
documentation should be changed
for all Postgres versions.

401
0:25:6,98 --> 0:25:7,88
So, okay.

402
0:25:8,04 --> 0:25:9,12
Thank you so much.

403
0:25:9,24 --> 0:25:14,88
It was some strange episodes again,
and next week I hope Michael

404
0:25:15,06 --> 0:25:18,58
is returning and we will have anniversary
1 year for our podcast.

405
0:25:18,84 --> 0:25:19,78
Thank you so much.

406
0:25:19,78 --> 0:25:24,24
Subscribe, share in your social
networks and working groups where

407
0:25:24,24 --> 0:25:27,26
you work with your colleagues who
are also interested in Pogues,

408
0:25:27,7 --> 0:25:31,78
and send us comments with your
feedback and ideas for future.

409
0:25:31,98 --> 0:25:32,52
Thank you.
