1
00:00:00,099999994 --> 00:00:03,1799998
Hello and welcome to PostgresFM,
a weekly show about

2
00:00:03,1799998 --> 00:00:04,08
all things PostgreSQL.

3
00:00:04,2799997 --> 00:00:07,7
I am Michael, founder of pgMustard
and today I am not joined

4
00:00:07,7 --> 00:00:08,5
by Nikolay.

5
00:00:09,0 --> 00:00:12,3
He is finally taking a well-earned
break.

6
00:00:12,44 --> 00:00:14,16
So this week you've just got me.

7
00:00:14,28 --> 00:00:17,86
I am in the process of getting
a couple of guests as well, which

8
00:00:17,86 --> 00:00:18,96
I'm excited about.

9
00:00:18,96 --> 00:00:22,66
So hopefully for the next couple
of weeks we'll be able to give

10
00:00:22,66 --> 00:00:23,74
you those conversations.

11
00:00:24,2 --> 00:00:27,54
But in the meantime I'm going to
record an episode on pretty

12
00:00:27,54 --> 00:00:31,22
much the only topic I feel comfortable
sharing knowledge on my

13
00:00:31,22 --> 00:00:33,42
own, which is EXPLAIN.

14
00:00:34,3 --> 00:00:37,04
And due to the nature of the product
I've been working on for

15
00:00:37,04 --> 00:00:41,12
the past few years, I have spent
a lot of time looking at EXPLAIN

16
00:00:41,12 --> 00:00:42,88
plans and helping folks with those.

17
00:00:42,88 --> 00:00:46,32
So yeah, I've given a couple of
talks on this in the past, but

18
00:00:46,32 --> 00:00:49,34
I realized we hadn't actually done
a podcast episode on it.

19
00:00:49,34 --> 00:00:53,36
We've done a few related ones,
so long-term listeners will be

20
00:00:53,36 --> 00:00:56,76
familiar with some of the things
we've talked about previously,

21
00:00:56,76 --> 00:01:02,04
but we haven't done a very, an
intro level one or even I have a

22
00:01:02,04 --> 00:01:04,3
process I'd like to recommend people
use.

23
00:01:04,3 --> 00:01:06,72
I haven't discussed that yet, so
I'm hoping to give you a couple

24
00:01:06,72 --> 00:01:07,62
of those things.

25
00:01:07,9 --> 00:01:11,82
But on the topic of guests, before
I move on, if you have any

26
00:01:11,82 --> 00:01:15,36
recommendations or requests on
either the guest front or the

27
00:01:15,36 --> 00:01:17,36
topic front, those are always welcome.

28
00:01:17,36 --> 00:01:20,52
Please send those to us either
privately or publicly.

29
00:01:21,02 --> 00:01:22,5
They'd be very, very welcome.

30
00:01:23,24 --> 00:01:27,479996
So EXPLAIN first, what is EXPLAIN
and what does it do?

31
00:01:27,88 --> 00:01:32,38
So EXPLAIN is a statement that
you put in front of your query

32
00:01:32,7 --> 00:01:36,1
and PostgreSQL will return the query
execution plan.

33
00:01:36,28 --> 00:01:38,18
So by default you just get the
plan.

34
00:01:38,32 --> 00:01:42,34
So what PostgreSQL would do, what
choices it would make to execute

35
00:01:42,38 --> 00:01:43,1
that query.

36
00:01:43,18 --> 00:01:47,92
It doesn't execute the query if
you just use EXPLAIN and it provides

37
00:01:48,04 --> 00:01:51,1
back the estimated costs involved.

38
00:01:51,34 --> 00:01:54,44
So you might have heard that PostgreSQL
has a cost-based optimizer.

39
00:01:55,08 --> 00:02:00,76
This gives you some insight into
the costs that it's calculating

40
00:02:00,76 --> 00:02:02,72
and why it's making certain choices.

41
00:02:02,72 --> 00:02:06,68
So why it's choosing a certain
scan type over another scan

42
00:02:06,68 --> 00:02:14,12
type or the join order it's
chosen or the join type it's

43
00:02:14,12 --> 00:02:15,7
doing for a specific join.

44
00:02:15,96 --> 00:02:20,5
So all of those are based on what's
possible and also what it

45
00:02:20,5 --> 00:02:21,68
thinks will be fastest.

46
00:02:22,36 --> 00:02:25,9
Now, the costs are in an arbitrary
unit, but the idea is the

47
00:02:25,9 --> 00:02:29,74
lower the cost, the faster the
optimizer thinks it would be.

48
00:02:30,02 --> 00:02:34,3
So, it is correlated to time, but
they're not in a unit of time.

49
00:02:34,74 --> 00:02:39,3
So, EXPLAIN on its own can be useful
to quickly see, for example,

50
00:02:39,4 --> 00:02:44,72
if you have a query that's timing
out or taking absolutely ages,

51
0:2:45,04 --> 0:2:49,62
you can quickly have a look at
the query plan and just check

52
0:2:49,84 --> 0:2:52,0
maybe why it might be slow.

53
0:2:52,3 --> 0:2:56,82
But if we want to know for sure
why a query is slow and it's

54
0:2:56,82 --> 0:3:0,26
executing in a reasonable amount
of time, chances are we're going

55
0:3:0,26 --> 0:3:4,54
to want to use some EXPLAIN parameters
to not just get the plan

56
0:3:4,54 --> 0:3:9,82
back but also run the query behind
the scenes and get execution

57
0:3:9,92 --> 0:3:10,42
statistics.

58
0:3:11,14 --> 0:3:15,24
So the most common of those parameters
that you might have heard

59
0:3:15,24 --> 0:3:16,82
alongside EXPLAIN is ANALYZE.

60
0:3:17,24 --> 0:3:21,06
So and this is different from the
ANALYZE command for getting query

61
0:3:21,06 --> 0:3:21,56
statistics.

62
0:3:21,6 --> 0:3:24,96
This is EXPLAIN ANALYZE specifically,
but there are loads more

63
0:3:24,96 --> 0:3:25,92
parameters as well.

64
0:3:25,92 --> 0:3:29,6
In fact, we did a whole episode
on why we believe, or Nikolay

65
0:3:29,6 --> 0:3:32,88
and I believe, that BUFFERS should
be turned on by default with ANALYZE.

66
0:3:32,88 --> 0:3:37,16
So BUFFERS is another parameter
that gives you extra runtime

67
0:3:37,46 --> 0:3:41,52
or execution statistics alongside
the timings that you get with

68
0:3:41,52 --> 0:3:42,02
ANALYZE.

69
0:3:42,08 --> 0:3:45,86
So with BUFFERS, we also get information
about the blocks read,

70
0:3:45,86 --> 0:3:46,36
written.

71
0:3:46,56 --> 0:3:50,7
So the amount of work being done
by the query, not just the timings.

72
0:3:51,22 --> 0:3:52,36
So that can really help.

73
0:3:52,36 --> 0:3:54,56
But there are a bunch of other
parameters as well.

74
0:3:54,66 --> 0:3:58,76
I like to recommend people use
as many of them as they can.

75
0:3:58,86 --> 0:4:1,16
Most of them don't have that much
additional overhead.

76
0:4:1,64 --> 0:4:2,78
ANALYZE can.

77
0:4:2,78 --> 0:4:6,1
In fact, that's something worth
bearing in mind, that there are

78
0:4:6,1 --> 0:4:9,78
a couple of caveats when it comes
to EXPLAIN ANALYZE.

79
0:4:9,8 --> 0:4:13,94
It can both overreport and underreport the amount of time that

80
0:4:13,94 --> 0:4:15,78
a query would take in the wild.

81
0:4:16,3 --> 0:4:20,76
So for example, it's been referred
to as the observer effect

82
0:4:20,76 --> 0:4:28,68
before, but if we have a slow system
clock, the overhead of checking

83
0:4:28,68 --> 0:4:34,8
the timings can add significant
overhead to the total execution

84
0:4:34,8 --> 0:4:38,88
time when we're running EXPLAIN
with ANALYZE and specifically

85
0:4:38,88 --> 0:4:40,02
with timings on.

86
0:4:40,76 --> 0:4:43,82
So, that's how it can overreport.

87
0:4:44,06 --> 0:4:48,66
That's the easiest way it can overreport
on the execution time.

88
0:4:48,84 --> 0:4:53,42
But it can also underreport if
for example, when we're

89
0:4:53,42 --> 0:4:56,64
running EXPLAIN ANALYZE, Postgres
doesn't have to transmit the data

90
0:4:56,64 --> 0:4:57,3
to us.

91
0:4:57,54 --> 0:5:3,54
So if we're requesting thousands
of rows, and we're not close

92
0:5:3,54 --> 0:5:7,08
to the database, so perhaps it's
a user doing a big report from

93
0:5:7,08 --> 0:5:10,68
a different continent than the
database is on, your monitoring

94
0:5:10,68 --> 0:5:14,84
might be showing that that query
is taking a few seconds to execute,

95
0:5:15,06 --> 0:5:18,0
whereas maybe EXPLAIN ANALYZE would
report it only taking a few

96
0:5:18,0 --> 0:5:21,96
hundred milliseconds because it
doesn't have to transmit that

97
0:5:21,96 --> 0:5:23,16
data across the network.

98
0:5:23,44 --> 0:5:27,58
So it can overreport and underreport,
but generally it's generally

99
0:5:27,92 --> 0:5:32,94
that's pretty uncommon and also
directionally you'll still see

100
0:5:33,16 --> 0:5:37,22
where the slowest parts of that
query plan are, even if the numbers

101
0:5:37,22 --> 0:5:38,16
aren't precise.

102
0:5:39,08 --> 0:5:40,62
So yeah, so those are the caveats.

103
0:5:41,5 --> 0:5:46,64
In terms of the process I recommend,
the number 1, I think I've

104
0:5:46,64 --> 0:5:49,4
mentioned it before in the podcast,
but it's so important, it's

105
0:5:49,4 --> 0:5:50,14
worth repeating.

106
0:5:51,02 --> 0:5:57,6
You need to be running on a realistic
size dataset to get realistic

107
0:5:57,68 --> 0:5:58,68
performance data.

108
0:5:59,54 --> 0:6:2,98
Now, it doesn't have to be the
exact same data.

109
0:6:3,58 --> 0:6:5,7
The important part is the number
of rows.

110
0:6:6,42 --> 0:6:11,4
So for example, if you were testing
out a new feature and your

111
0:6:11,58 --> 0:6:16,82
local dataset only has a couple
of dozen rows, but production

112
0:6:16,86 --> 0:6:21,38
has a few million, chances are
the Postgres plan is going to

113
0:6:21,38 --> 0:6:26,82
be making different choices on
join order or join algorithm,

114
0:6:27,24 --> 0:6:28,38
even scan type.

115
0:6:28,38 --> 0:6:30,92
So it could even choose to do a
sequential scan if your data

116
0:6:30,92 --> 0:6:33,74
is small enough, even if there
is a good index available.

117
0:6:34,2 --> 0:6:38,0
So that can really surprise people,
but it's really important.

118
0:6:38,72 --> 0:6:43,08
As the datasets get bigger, this
becomes slightly less important.

119
0:6:43,08 --> 0:6:46,28
So if you're testing on tens of
millions of rows instead of hundreds

120
0:6:46,28 --> 0:6:49,54
of millions, there's less of a
chance of an issue but still a

121
0:6:49,54 --> 0:6:51,9
chance of an issue getting a different
plan.

122
0:6:52,48 --> 0:6:55,56
So very much worth bearing that
in mind.

123
0:6:55,56 --> 0:6:59,08
If you can test on a realistic
size dataset, please do.

124
0:6:59,24 --> 0:7:2,12
There's lots of tools available
for doing that and for masking

125
0:7:2,12 --> 0:7:3,3
data these days.

126
0:7:3,96 --> 0:7:7,12
But that's, in fact, I think if
we've done an episode on that,

127
0:7:7,12 --> 0:7:8,4
I think we have on benchmarking.

128
0:7:8,54 --> 0:7:10,22
So I'll link that one up.

129
0:7:10,52 --> 0:7:13,38
So that's tip number 1, or step
1 in the process.

130
0:7:13,38 --> 0:7:15,82
Make sure you are, or the person
that you're helping, is testing

131
0:7:15,82 --> 0:7:17,8
on a realistic dataset.

132
0:7:19,6 --> 0:7:21,18
Use as many parameters as possible.

133
0:7:21,18 --> 0:7:22,62
I've hinted at that already.

134
0:7:22,68 --> 0:7:25,74
Mentioned ANALYZE and BUFFERS,
but there are also parameters

135
0:7:26,1 --> 0:7:29,68
for settings, which tells you any
non-default planner-related

136
0:7:30,26 --> 0:7:31,36
settings that you have.

137
0:7:31,36 --> 0:7:36,22
So for example, if you've changed
work mem or random page cost,

138
0:7:36,22 --> 0:7:39,18
these are common ones to have changed,
those will show up.

139
0:7:39,48 --> 0:7:44,18
But also if you, for example, been
testing a few locally and

140
0:7:44,18 --> 0:7:46,64
you've forgotten that you've still
got those set, they'll show

141
0:7:46,64 --> 0:7:49,22
up in your EXPLAIN plan as well
which can be helpful.

142
0:7:50,46 --> 0:7:53,16
Another important one is, well, another
one I find really useful

143
0:7:53,16 --> 0:7:56,32
is verbose, especially when helping
other people, you get to

144
0:7:56,32 --> 0:7:59,18
see information about the schema,
for example, you get fully

145
0:7:59,18 --> 0:8:4,46
qualified object names, you also
get the output of each operation

146
0:8:4,54 --> 0:8:9,18
in the query execution plan, what
data it's sending to its parent

147
0:8:9,4 --> 0:8:13,58
operation, which can be useful
for advising certain optimizations.

148
0:8:15,06 --> 0:8:18,5
A couple of others, a new one is
the write-ahead logging information,

149
0:8:18,74 --> 0:8:22,0
or I say new, I think it's a few
versions old now.

150
0:8:22,2 --> 0:8:24,78
You can also specify different
formats.

151
0:08:25,12 --> 0:08:28,58
So JSON format is particularly
popular these days amongst tools,

152
0:08:29,04 --> 0:08:34,00
but text format is generally still
the most concise and most

153
0:08:34,00 --> 0:08:36,56
readable amongst especially people
that are most familiar with

154
0:08:36,56 --> 0:08:36,90
it.

155
0:08:36,90 --> 0:08:40,32
They'll often ask to see the text
format plan because it's smaller

156
0:08:40,32 --> 0:08:42,54
and more familiar to them.

157
0:08:43,28 --> 0:08:46,26
Now there are a couple of other
parameters as well.

158
0:08:47,22 --> 0:08:50,58
Less important because people generally
leave them on or leave

159
0:08:50,58 --> 0:08:53,68
the defaults in place but timing
can be really useful if you

160
0:08:53,68 --> 0:08:57,66
want to, for example, turn timing
information off but have ANALYZE

161
0:08:57,80 --> 0:08:58,30
on.

162
0:08:58,52 --> 0:09:04,64
So you still get the full execution
timing statistics but

163
0:09:04,64 --> 0:09:08,82
you don't measure the per node
timing, which is where the main

164
0:09:08,82 --> 0:09:13,50
overhead is so I've seen some people
with some tool vendors recommend

165
0:09:13,50 --> 0:09:17,32
turning that off with auto_explain
but worth measuring if it

166
0:09:17,32 --> 0:09:21,30
does actually have any overhead
on your system before doing so

167
0:09:21,88 --> 0:09:24,24
because timing information could
be really useful when looking

168
0:09:24,24 --> 0:09:25,82
at speeding things up.

169
0:09:26,44 --> 0:09:28,00
Costs you can turn off as well.

170
0:09:28,00 --> 0:09:30,36
The only time I've seen that done
and the only time I've done

171
0:09:30,36 --> 0:09:34,74
it myself is when writing blog
posts or sharing query plans where

172
0:09:34,74 --> 0:09:39,22
I just want to make them slightly
less intimidating or draw attention

173
0:09:39,22 --> 0:09:42,70
to the more important parts. So
those are on by default and the

174
0:09:42,70 --> 0:09:45,76
other one that's on by default is
or at least when you use EXPLAIN

175
0:09:45,76 --> 0:09:50,14
(ANALYZE) is summary, which puts
the information at the end.

176
0:09:50,14 --> 0:09:52,80
So like planning timings and execution
times, which are very

177
0:09:52,80 --> 0:09:53,30
useful.

178
0:09:53,48 --> 0:09:56,24
So generally people don't turn
that off, but it means you can

179
0:09:56,24 --> 0:09:59,10
turn that on, for example, with
EXPLAIN, which gives you the

180
0:09:59,10 --> 0:10:00,04
planning time.

181
0:10:00,18 --> 0:10:03,62
Because remember when we just do
EXPLAIN, we only plan the query,

182
0:10:03,68 --> 0:10:06,26
we don't, Postgres doesn't execute
that.

183
0:10:06,26 --> 0:10:09,72
So you can get the planning time
just from EXPLAIN alone if you

184
0:10:09,72 --> 0:10:11,64
use the summary parameter.

185
0:10:11,94 --> 0:10:15,36
But generally I recommend putting
as many of those on as you

186
0:10:15,42 --> 0:10:19,84
can, especially if you're using
a tool to parse it or if you

187
0:10:19,84 --> 0:10:22,20
are getting help from others, the
more information the better

188
0:10:22,20 --> 0:10:22,94
for them.

189
0:10:23,80 --> 0:10:25,96
Cool, so that's the second step.

190
0:10:25,96 --> 0:10:29,94
The third one is glance at the end.

191
0:10:30,06 --> 0:10:33,50
So query plans, especially once
your queries get a bit more complex,

192
0:10:33,82 --> 0:10:34,96
can get quite long.

193
0:10:34,96 --> 0:10:38,14
You can get, feel a bit like a
wall of text.

194
0:10:39,02 --> 0:10:43,44
But a few of the probably rarer
problems, to be honest, but a

195
0:10:43,44 --> 0:10:49,54
few of the fairly common problems
are highlighted very much at

196
0:10:49,54 --> 0:10:50,22
the end.

197
0:10:50,22 --> 0:10:57,42
So things like, if your planning
time is 10X your execution time,

198
0:10:57,80 --> 0:11:00,40
there's very little point looking
through the rest of the query

199
0:11:00,40 --> 0:11:06,88
plan, looking for optimization
potential, because the vast majority

200
0:11:08,80 --> 0:11:11,14
of the total time was in planning.

201
0:11:11,5 --> 0:11:14,54
So that's only reported at the
end and the rest of the query

202
0:11:14,54 --> 0:11:15,8
plan won't help you.

203
0:11:17,1 --> 0:11:20,14
The other two things that are reported
at the end that also report

204
0:11:20,14 --> 0:11:22,1
a total time are triggers.

205
0:11:22,34 --> 0:11:27,54
So trigger executions, they can
be the performance issue.

206
0:11:27,54 --> 0:11:30,02
If you have triggers involved,
there's going to be some overhead

207
0:11:30,02 --> 0:11:30,3
there.

208
0:11:30,3 --> 0:11:31,82
They report a total time.

209
0:11:31,96 --> 0:11:36,34
If that's a high percentage of
your total execution time, you

210
0:11:36,34 --> 0:11:39,16
found your problem already without
looking through the whole

211
0:11:39,16 --> 0:11:40,1
execution plan.

212
0:11:40,12 --> 0:11:42,04
Same is true for just-in-time compilation.

213
0:11:42,74 --> 0:11:49,16
That can in certain cases be 99%
of execution time, which again,

214
0:11:49,9 --> 0:11:52,94
makes looking through the query
plan not as useful.

215
0:11:54,34 --> 0:11:54,84
Cool.

216
0:11:54,94 --> 0:11:57,42
So that's the third one.

217
0:11:57,62 --> 0:12:2,18
Fourth is then once you've looked
at those, then figure out,

218
0:12:2,56 --> 0:12:5,62
okay, we do need to look at the
rest of the query plan.

219
0:12:6,6 --> 0:12:8,42
Where is the time going?

220
0:12:8,42 --> 0:12:10,36
Which parts of these are the most
expensive?

221
0:12:10,38 --> 0:12:13,58
Or if you have BUFFERS on, which
of these scans is doing the

222
0:12:13,58 --> 0:12:15,04
most reads?

223
0:12:16,02 --> 0:12:19,04
The reason I mention that that's
like step 4 is because I think

224
0:12:19,04 --> 0:12:22,64
it's very easy at this point to
jump to other, like jump to looking

225
0:12:22,64 --> 0:12:25,76
to other issues and I've been guilty
of this in the past myself

226
0:12:25,76 --> 0:12:26,46
as well.

227
0:12:26,68 --> 0:12:31,24
So experienced people, I think
maybe even more susceptible to

228
0:12:31,24 --> 0:12:34,94
this than less experienced people,
if you notice a sequential

229
0:12:35,02 --> 0:12:38,68
scan, your eye gets drawn to it
immediately and you think, oh,

230
0:12:38,68 --> 0:12:43,24
maybe there's an issue there, or
you notice a large filter, or

231
0:12:43,38 --> 0:12:47,14
a few thousand rows being filtered,
or a lot of loops or something.

232
0:12:47,32 --> 0:12:50,08
If your eye gets drawn to these
problems that you're used to

233
0:12:50,08 --> 0:12:53,88
looking for, you can get tripped
up or get, you can end up going

234
0:12:53,88 --> 0:12:58,04
down a bit of a rabbit hole on
this.

235
0:12:58,04 --> 0:13:2,72
So my tip is to really check where
the timings go, like where

236
0:13:2,72 --> 0:13:5,74
which of these is really taking
up the most time and then focus

237
0:13:5,74 --> 0:13:6,5
your attention there.

238
0:13:6,5 --> 0:13:11,2
Only once you've worked that out
focus on that part of the query

239
0:13:11,2 --> 0:13:12,9
or that part of the query plan.

240
0:13:13,18 --> 0:13:15,04
Is there a bottleneck we can solve?

241
0:13:15,14 --> 0:13:18,76
And maybe we'll come back to the
other issue we spotted later,

242
0:13:18,76 --> 0:13:21,3
but also the query might be fast
enough by that point and we

243
0:13:21,3 --> 0:13:22,18
can move on.

244
0:13:23,1 --> 0:13:26,58
So, yeah, that's the fourth and
fifth.

245
0:13:26,58 --> 0:13:30,98
So work out where the what the
most expensive parts of the query

246
0:13:31,08 --> 0:13:35,1
plan are and then and only then
what you can do about it.

247
0:13:35,5 --> 0:13:40,58
So on the timings front, there
are a few things to be aware of.

248
0:13:40,92 --> 0:13:48,72
So the execution time is reported
inclusive of so 1 operation's

249
0:13:48,82 --> 0:13:51,84
execution time is inclusive of
all of its children.

250
0:13:52,24 --> 0:13:55,92
So a little bit of subtraction
you'd think might be enough there.

251
0:13:55.92 --> 0:14:00.04
to work out maybe which of the
operations is most expensive on

252
0:14:00.04 --> 0:14:00.74
its own.

253
0:14:01.56 --> 0:14:05.08
But you have to take into account
a bunch of other things as

254
0:14:05.08 --> 0:14:05.46
well.

255
0:14:05.46 --> 0:14:09.82
So, those execution times are a
per loop average.

256
0:14:10.32 --> 0:14:14.44
So, for example, if one of our operations
is being looped over

257
0:14:15.24 --> 0:14:19.7
multiple times, which is especially
common in join operations.

258
0:14:20.6 --> 0:14:28.1
But also, parallel execution utilizes
loops as how it reports.

259
0:14:28.1 --> 0:14:32.36
So, there's a few ways where we
have to then multiply it by the

260
0:14:32.36 --> 0:14:35.58
number of loops before subtracting
from the parent operation.

261
0:14:35.58 --> 0:14:38.22
So these things, you can see how
they can start to get a little

262
0:14:38.22 --> 0:14:38.86
bit complex.

263
0:14:39.22 --> 0:14:43.58
In addition, Postgres reports these
timings in milliseconds and

264
0:14:43.58 --> 0:14:45.82
doesn't include thousands separators.

265
0:14:46.28 --> 0:14:49.74
So I know I personally can start
to struggle with the arithmetic

266
0:14:49.74 --> 0:14:52.7
of all that once it starts getting
into the multiple seconds.

267
0:14:53.24 --> 0:14:56.0
As such, there's a ton of tools
that can help you with this.

268
0:14:56.0 --> 0:14:59.44
I work on one, but there are some
really good free open source

269
0:14:59.44 --> 0:15:0.14
ones available.

270
0:15:0.24 --> 0:15:3.48
Lots of editors include them by
default and pretty much all of

271
0:15:3.48 --> 0:15:7.48
them make at least some effort
to get those timings pretty good.

272
0:15:7.48 --> 0:15:12.34
So some popular ones are Depesz
EXPLAIN, a really nice

273
0:15:12.34 --> 0:15:15.04
thing about that is that it preserves
the text format.

274
0:15:15.04 --> 0:15:19.36
So it's really expert friendly
if you want to share an EXPLAIN

275
0:15:19.36 --> 0:15:23.2
plan with the Postgres mailing
lists, the one they'll be most familiar

276
0:15:23.2 --> 0:15:26.56
with and most comfortable with
will be the Depesz EXPLAIN one.

277
0:15:26.88 --> 0:15:30.32
But EXPLAIN Dalibo is really
popular these days, much more

278
0:15:30.32 --> 0:15:34.92
visual, includes more charts on,
for example, where the timings

279
0:15:34.92 --> 0:15:37.66
are going, where the BUFFERS, like
which operations do the most

280
0:15:37.66 --> 0:15:39.3
buffer hits, that kind of thing.

281
0:15:40.16 --> 0:15:44.6
Much more beginner friendly, and
as I would probably class my

282
0:15:44.6 --> 0:15:48.7
tool as well, pgMustard is visual as
well, but also helps with that

283
0:15:48.7 --> 0:15:52.16
next step a bit more on what you
can possibly do about things.

284
0:15:52.48 --> 0:15:55.2
But yeah, so the main reason I want to mention those

285
0:15:55.2 --> 0:15:57.18
is they can really help on the
timings front.

286
0:15:57.18 --> 0:16:00.8
So you can paste your query plan
in and quickly see where the

287
0:16:00.8 --> 0:16:01.86
timings are going.

288
0:16:01.96 --> 0:16:05.94
I would say that a lot of them
don't do that step 3.

289
0:16:05.94 --> 0:16:10.3
So a lot of them don't immediately
draw your attention if there's

290
0:16:10.3 --> 0:16:13.64
triggers taking up a lot of time
or just in time compilation

291
0:16:13.66 --> 0:16:16.14
taking up a lot of time or even
planning time.

292
0:16:16.28 --> 0:16:20.86
So it is still worth doing that
step first and then checking

293
0:16:20.86 --> 0:16:24.64
out a tool or two that you're familiar
with for the next steps.

294
0:16:25.44 --> 0:16:27.78
Yeah, that's pretty much it for
the process.

295
0:16:29.2 --> 0:16:33.18
Obviously, that fifth step, working
out what to do next, is a

296
0:16:33.18 --> 0:16:34.54
hugely deep topic.

297
0:16:34.54 --> 0:16:38.26
And it's actually where a lot of
the normal talks on EXPLAIN,

298
0:16:38.26 --> 0:16:40.76
if you go to a conference and see
a talk on EXPLAIN, that's normally

299
0:16:40.76 --> 0:16:41.98
where they'll pick up.

300
0:16:42.44 --> 0:16:46.0
So the best one I've seen is a bit
old now.

301
0:16:46,0 --> 0:16:47,580
It's from probably about 2016.

302
0:16:48,480 --> 0:16:50,940
But Josh Berkus gave it a few times.

303
0:16:50,940 --> 0:16:54,940
But I'll share one of the recordings
that I liked a lot.

304
0:16:55,340 --> 0:16:58,760
Yeah, he spent about an hour going
through a lot of the different

305
0:16:59,10 --> 0:17:03,540
join types, scan types, types of
issues you can see, things to

306
0:17:03,540 --> 0:17:04,660
be aware of.

307
0:17:04,780 --> 0:17:08,880
And yeah, it's a lot, even then,
probably only covers the first

308
0:17:09,00 --> 0:17:11,060
10% of what you could know.

309
0:17:11,100 --> 0:17:16,220
There are more recent talks, one
by Bruce Momjian actually, who'd

310
0:17:16,220 --> 0:17:18,240
covered some of the rarer scan
types.

311
0:17:18,240 --> 0:17:19,740
So I'll link that one up as well.

312
0:17:19,740 --> 0:17:23,00
But this is a deep, deep topic
that you can dive into.

313
0:17:23,00 --> 0:17:26,340
I'm a few years in and I don't
consider myself an expert yet.

314
0:17:26,520 --> 0:17:28,220
So that gives you an idea.

315
0:17:29,340 --> 0:17:33,060
But yeah, it's an extremely powerful
tool for query optimization

316
0:17:33,120 --> 0:17:33,820
in general.

317
0:17:34,60 --> 0:17:38,160
Naturally, to speed things up,
it really helps to know which

318
0:17:38,160 --> 0:17:39,060
part's slow.

319
0:17:39,40 --> 0:17:42,220
So please use it.

320
0:17:43,660 --> 0:17:44,760
Let me know how you get on.

321
0:17:44,760 --> 0:17:45,940
It'd be cool to hear.

322
0:17:47,480 --> 0:17:50,440
We've done a bunch of related episodes,
so a couple of those

323
0:17:50,440 --> 0:17:54,060
to check out are there's an extension
called auto_explain.

324
0:17:54,920 --> 0:18:01,160
Now, that will let you run your
queries as normal, let's say,

325
0:18:01,40 --> 0:18:07,880
maybe even on production, and the
users get the results of the

326
0:18:07,880 --> 0:18:09,780
query back, which we don't when
we run EXPLAIN.

327
0:18:09,780 --> 0:18:11,540
I don't know if I mentioned that
up top.

328
0:18:11,940 --> 0:18:16,40
But the people using your database
will continue to run their

329
0:18:16,40 --> 0:18:18,660
queries and get their results as
usual.

330
0:18:18,80 --> 0:18:22,960
But in the background, the execution
plans will be being logged,

331
0:18:22,960 --> 0:18:27,660
any beyond a certain timing threshold
that you set will get logged

332
0:18:27,660 --> 0:18:29,440
to your Postgres logs.

333
0:18:29,440 --> 0:18:31,10
So we've done a whole episode on
that.

334
0:18:31,10 --> 0:18:32,480
Can be extremely useful.

335
0:18:32,680 --> 0:18:33,840
And I'll link that up.

336
0:18:33,840 --> 0:18:35,880
We've also done one on row estimates.

337
0:18:36,040 --> 0:18:38,30
So that's another common issue.

338
0:18:38,480 --> 0:18:43,220
So with EXPLAIN, we get to see
how many rows Postgres expected

339
0:18:43,260 --> 0:18:45,040
to be returned at each stage.

340
0:18:45,560 --> 0:18:49,640
And when it's wrong, when it's
wrong by a lot especially, that

341
0:18:49,640 --> 0:18:54,960
can lead to all sorts of issues
with inefficient plans being

342
0:18:54,960 --> 0:18:55,460
chosen.

343
0:18:55,940 --> 0:18:58,160
So we did a whole episode on that.

344
0:18:58,660 --> 0:19:01,280
In fact, our first ever episode
was on slow queries and slow

345
0:19:01,280 --> 0:19:01,780
transactions.

346
0:19:01,880 --> 0:19:06,060
So that's worth checking out if
you haven't already.

347
0:19:06,580 --> 0:19:10,580
And we did a more advanced one on
query optimization, which again,

348
0:19:10,580 --> 0:19:12,980
we still didn't cover some of the
basics of explaining.

349
0:19:12,980 --> 0:19:15,080
So I'm glad to have done this one
now.

350
0:19:15,920 --> 0:19:16,920
Welcome any questions.

351
0:19:17.78 --> 0:19:24.01
And please do remember to send
your guest and topic suggestions

352
0:19:24.34 --> 0:19:24.84
through.

353
0:19:25.54 --> 0:19:29.7
Happy holidays, hope you all had
a good one and catch you next

354
0:19:29.7 --> 0:19:30.54
time, hopefully with a guest.

355
0:19:30.54 --> 0:19:30.5701
Bye.

356
0:19:30.5701 --> 0:19:31.16
Hopefully with a guest.
