1
0:00:00,58 --> 0:00:03,98
Nikolay: So, hello, hello, this
is PostgresFM episode number

2
0:00:04,2799997 --> 0:00:07,06
66 or 67, I don't remember.

3
0:00:07,8999996 --> 0:00:14,0199995
My name is Nikolay and I'm alone
today because Michael couldn't

4
0:00:14,0199995 --> 0:00:14,74
make it.

5
0:00:15,16 --> 0:00:21,3
So, I don't like to be alone, actually,
and I don't like to record

6
0:00:21,3 --> 0:00:23,18
myself alone in advance.

7
0:00:23,32 --> 0:00:24,7799997
So that's why it's live.

8
0:00:25,02 --> 0:00:27,84
But feel free to ask questions
in the chat.

9
0:00:28,779999 --> 0:00:32,059998
I also have my co-host today, 
ChatGPT.

10
0:00:34,18 --> 0:00:35,32
Hello, ChatGPT.

11
0:00:36,28 --> 0:00:37,22
ChatGPT: Hello, NikoLay.

12
0:00:43,5 --> 0:00:46,9
It's great to be here
co-hosting PostgresFM with you.

13
0:00:47,08 --> 0:00:50,84
To our listeners, I'm eager to
engage with your questions and

14
0:00:50,84 --> 0:00:52,06
thoughts in the chat.

15
0:00:52,42 --> 0:00:54,16
So, Nikolay, what's today's topic?

16
0:00:55,6 --> 0:01:00,46
Nikolay: Okay, I hope this ChatGPT
thing will pronounce PostgreSQL

17
0:01:00,88 --> 0:01:02,3
correctly, I doubt.

18
0:01:02,98 --> 0:01:08,6
But today we are going to talk
about how to start and stop Postgres,

19
0:01:08,86 --> 0:01:12,62
or stop and start Postgres faster,
and how to troubleshoot if

20
0:01:12,62 --> 0:01:13,66
it takes too long.

21
0:01:15,04 --> 0:01:22,16
I recently started a new series
of my small how-to articles.

22
0:01:22,82 --> 0:01:26,68
I do it on Twitter initially, but
then it spread to my LinkedIn,

23
0:01:26,72 --> 0:01:32,06
and also there is a markdown version
on GitLab repositories,

24
0:01:33,08 --> 0:01:35,82
where I have already accumulated 10.

25
0:01:37,5 --> 0:01:39,26
I started it 10 days ago.

26
0:01:40,6 --> 0:01:47,0
The idea is, too many things in
my head that could be on paper

27
0:01:47,5 --> 0:01:53,0
with examples and, you know, like
SQL queries or some snippets

28
0:01:53,479996 --> 0:01:53,979996
for...

29
0:01:54,86 --> 0:01:58,52
You can run in console, like, bash
snippets or something.

30
0:01:59,12 --> 0:02:02,92
And, for example, yesterday I talked
about how to create flame

31
0:02:02,92 --> 0:02:07,94
graphs if you have issues with
some Postgres running on your

32
0:02:08,04 --> 0:02:08,98
Linux machine.

33
0:02:09,12 --> 0:02:12,88
Of course, it wouldn't work on
RDS or other managed services,

34
0:02:13,32 --> 0:02:18,14
But you can, if you have, for example,
some issue on production,

35
0:02:18,14 --> 0:02:22,1
you can reproduce it on a standalone
Postgres on a Linux machine.

36
0:02:22,12 --> 0:02:29,44
And then, PRF or eBPF tooling give
you very powerful capabilities

37
0:02:30,06 --> 0:02:33,06
to see what's going on under the
hood.

38
0:02:33,26 --> 0:02:38,14
So FlameGraphs is a very good tool
to understand which functions

39
0:02:39,06 --> 0:02:42,84
in the source code take too much
time in CPU.

40
0:02:43,52 --> 0:02:49,54
But today we are going to discuss
stop and start duration of

41
0:02:49,54 --> 0:02:50,0
Postgres.

42
0:02:50,0 --> 0:02:53,44
And this, of course, can be applied
to any Postgres, any version

43
0:02:53,44 --> 0:02:57,42
of it, and including, of course,
RDS or any other managed services.

44
0:02:58,14 --> 0:03:01,66
So let's start with stop attempts.

45
0:03:01,98 --> 0:03:06,26
For example, you want to stop Postgres
and see it takes long.

46
0:03:07,36 --> 0:03:10,14
What kind of reasons can we have?

47
0:03:21,48 --> 0:03:24,6
ChatGPT: That sounds like a valuable
series of how-to articles

48
0:03:24,6 --> 0:03:25,98
you've been creating, Nikolay.

49
0:03:26,2 --> 0:03:30,66
Understanding the nuances of Postgres
and its operations is crucial

50
0:03:30,66 --> 0:03:32,38
for many developers and admins.

51
00:03:33,28 --> 00:03:36,48
Regarding your question on the
duration of stopping Postgres,

52
00:03:36,90 --> 00:03:39,88
there are several reasons it might
take longer than expected.

53
00:03:40,94 --> 00:03:42,76
First, active transactions.

54
00:03:43,62 --> 00:03:46,48
If there are active transactions
when you attempt to stop the

55
00:03:46,48 --> 00:03:49,92
service, Postgres will wait for
them to complete before shutting

56
00:03:49,92 --> 00:03:50,42
down.

57
00:03:50,98 --> 00:03:53,00
Second, write-ahead logs.

58
00:03:53,30 --> 00:03:56,62
Postgres could be flushing the
WAL to disk, which can sometimes

59
00:03:56,68 --> 00:03:58,12
delay the shutdown process.

60
00:03:58,26 --> 00:03:59,76
Third, background processes.

61
00:04:00,12 --> 00:04:03,54
Postgres runs several background
processes, like Autovacuum.

62
00:04:04,44 --> 00:04:07,96
These processes might delay the
shutdown if they're in the middle

63
00:04:07,96 --> 00:04:08,80
of their operations.

64
00:04:09,52 --> 00:04:11,18
Fourth, connection draining.

65
00:04:11,40 --> 00:04:14,34
If there are active connections,
Postgres will wait for them

66
00:04:14,34 --> 00:04:17,74
to disconnect or reach a safe state
to shut down.

67
00:04:18,22 --> 00:04:22,12
For speeding up the process,
you might consider issuing a fast

68
00:04:22,12 --> 00:04:25,62
shutdown command, which skips waiting
for active transactions

69
00:04:25,76 --> 00:04:26,44
to complete.

70
00:04:28,52 --> 00:04:32,08
Nikolay: Okay, here I can interrupt
without hesitating.

71
00:04:32,98 --> 00:04:36,96
Unlike when I'm with Michael, I'm
always hesitating to interrupt

72
00:04:36,96 --> 00:04:38,70
Michael, but do it all the time,
unfortunately.

73
00:04:39,16 --> 00:04:44,74
So I see, like, honestly, I didn't
prepare this thing beforehand,

74
00:04:44,80 --> 00:04:46,96
so I didn't provide any ideas.

75
00:04:46,96 --> 00:04:52,76
So we see obviously a couple of
the first reasons ChatGPT mentions

76
00:04:52,96 --> 00:04:55,42
are really correct.

77
00:04:55,44 --> 00:05:00,86
I mean, they are the same as I
list in my how-to article.

78
00:05:01,56 --> 00:05:04,74
I explain 3 reasons and how to
deal with them.

79
00:05:04,76 --> 00:05:10,44
And first of all, the very first
is indeed ongoing long running

80
00:05:11,06 --> 00:05:11,56
transactions.

81
00:05:12,28 --> 00:05:19,94
So if we, usually if we try to
stop using the default way, it's

82
00:05:19,94 --> 00:05:21,72
the so-called smart way.

83
00:05:22,36 --> 00:05:26,94
Postgres is going to wait until
ongoing work is done, and this

84
00:05:26,94 --> 00:05:29,24
is probably not what you want.

85
00:05:30,06 --> 00:05:34,16
Because, of course, maybe you need
to stop faster, so you need

86
00:05:34,16 --> 00:05:37,52
to use so-called fast shutdown.

87
00:05:38,24 --> 00:05:40,88
It's pg_ctl -m fast.

88
00:05:42,86 --> 00:05:44,28
Default is called smart.

89
00:05:45,06 --> 00:05:50,30
And in this case, it will issue
SIGINT instead of SIGTERM signal

90
00:05:50,66 --> 00:05:52,12
to the Postgres process.

91
00:05:52,92 --> 00:05:57,94
And it means that Postgres will
interrupt all ongoing transactions

92
00:05:58,26 --> 00:06:02,06
and proceed with the shutdown process.

93
00:06:02,30 --> 00:06:05,32
The shutdown process is quite
complicated.

94
00:06:06,04 --> 00:06:11,48
If you can study source code, I
provide links in my article.

95
00:06:11,48 --> 00:06:15,36
I will provide them here in the
show notes as well.

96
00:06:18,08 --> 00:06:19,08
This is simple.

97
00:06:19,08 --> 00:06:20,52
You just use fast shutdown,

98
00:06:20,92 --> 00:06:23,90
understand that ongoing transactions
will be interrupted,

99
00:06:24,40 --> 00:06:27,32
but two other reasons are much more
interesting.

100
00:06:27,38 --> 00:06:32,92
And let's start with the WAL-related
and checkpoint-related reason.

101
0:06:38,84 --> 0:06:39,62
I don't know.

102
0:06:39,62 --> 0:06:40,38
Is it working?

103
0:06:40,38 --> 0:06:41,16
I don't know.

104
0:06:41,84 --> 0:06:42,68
Are you working?

105
0:06:42,88 --> 0:06:43,78
ChatGPT: Certainly, Nikolay.

106
0:06:43,94 --> 0:06:44,44
Nikolay: Okay.

107
0:06:45,48 --> 0:06:48,48
ChatGPT: The interaction between
the write-ahead logs (WAL) and

108
0:06:48,48 --> 0:06:52,16
checkpoints plays a crucial role
in PostgreSQL's shutdown process.

109
0:06:53,7 --> 0:06:56,82
When you initiate a shutdown, if
there's a pending checkpoint,

110
0:06:57,44 --> 0:07:01,36
Postgres will attempt to complete
that checkpoint before shutting

111
0:07:01,36 --> 0:07:01,86
down.

112
0:07:13,14 --> 0:07:13,93
Checkpoints are most...

113
0:07:13,94 --> 0:07:14,39
Nikolay: No, no, no, no, no.

114
0:07:14,39 --> 0:07:15,34
When we try to shut down Postgres,
first of all, there is a thing

115
0:07:15,34 --> 0:07:16,20
which is

116
0:07:16,20 --> 0:07:18,82
called a shutdown checkpoint.

117
0:07:18,82 --> 0:07:23,9
So it's like always, So we try
to shut down and Postgres wants

118
0:07:23,9 --> 0:07:25,2
to perform shutdown checkpoint.

119
0:07:25,2 --> 0:07:29,96
That means that all dirty buffers
in the buffer pool need to

120
0:07:29,96 --> 0:07:31,58
be flushed to disk.

121
0:07:32,18 --> 0:07:36,36
So this is a clean shutdown.

122
0:07:36,72 --> 0:07:41,97
If you issue CKILL or KILL-9, which
is like kind of crashing

123
0:07:41,97 --> 0:07:46,30
your Postgres or just switching
off power of whole machine.

124
0:07:46,56 --> 0:07:50,24
In this case, of course, it won't
do it because it doesn't have

125
0:07:50,46 --> 0:07:53,10
ability and chances to do it.

126
0:07:53,18 --> 0:07:57,02
But in other cases, it tries to
perform shutdown checkpoint.

127
0:07:57,66 --> 0:08:01,22
And the trick with shutdown checkpoint
is while it's doing it,

128
0:08:01,22 --> 0:08:02,86
it cannot accept new connections.

129
0:08:02,98 --> 0:08:06,30
And this is bad, because if we
accumulated a lot of dirty buffers

130
0:08:06,30 --> 0:08:10,14
in the buffer pool, it means, you
know, like, I don't like it.

131
0:08:10,14 --> 0:08:11,54
I like Michael better.

132
0:08:12,20 --> 0:08:13,22
I don't like...

133
0:08:13,88 --> 0:08:16,50
It's, like, not working really
well.

134
0:08:17,50 --> 0:08:19,18
Let's do it alone completely.

135
0:08:20,66 --> 0:08:25,68
So if you issue shutdown, shutdown
checkpoint can be huge.

136
0:08:26,58 --> 0:08:29,76
Of course, if your settings are
default, it won't be huge because

137
0:08:29,76 --> 0:08:31,40
max wall size is 1 gigabyte.

138
0:08:31,42 --> 0:08:36,54
It means that only like roughly
half a gigabyte of dirty, of

139
0:08:36,54 --> 0:08:42,42
WAL data can be accumulated in
the WAL since the latest checkpoint.

140
0:08:43,66 --> 0:08:49,20
And also, if you have checkpoint
timeout default, it's also small,

141
0:08:49,20 --> 0:08:51,24
I think it's 5 minutes, right?

142
0:08:51,82 --> 0:08:55,66
In this case, we don't accumulate
a lot of dirty buffers.

143
0:08:55,92 --> 0:08:58,52
It means that shutdown checkpoint
will be fast, but still it's

144
0:08:58,52 --> 0:09:03,26
not really pleasant because while
Postgres is flushing dirty

145
0:09:03,26 --> 0:09:06,30
buffers on disk, it doesn't accept
connections.

146
0:09:06,68 --> 0:09:12,46
So the idea is we can run checkpoint
explicitly right before

147
0:09:12,66 --> 0:09:14,12
we shut down.

148
0:09:14,34 --> 0:09:19,66
It means that we issue fast checkpoint
using SQL command checkpoint,

149
0:09:20,74 --> 0:09:21,82
all dirty buffer.

150
0:09:21,82 --> 0:09:26,74
Dirty means changed buffers in
the buffer pool, but not yet synchronized

151
0:09:26,88 --> 0:09:27,54
to disk.

152
0:09:29,28 --> 0:09:33,82
We issue a checkpoint, Postgres
flushes all of them to disk. And

153
0:09:33,82 --> 0:09:37,54
during this checkpoint, we're still
working, still accepting

154
0:09:37,54 --> 0:09:40,02
connections, so nobody actually
notices it.

155
0:09:40,44 --> 0:09:44,56
And then when we try to shut down
immediately after our checkpoint,

156
0:09:45,06 --> 0:09:48,28
The shutdown checkpoint is going
to be there still, but it will

157
0:09:48,28 --> 0:09:52,32
be small because we just performed
another checkpoint.

158
0:09:53,62 --> 0:09:54,44
This is great.

159
0:09:54,52 --> 0:09:56,76
Sometimes you want to do 2 checkpoints,
actually.

160
0:09:56,76 --> 0:10:00,72
I never did 2 checkpoints, but
recently we discussed it in my team

161
0:10:00,72 --> 0:10:02,62
and it turned out some...

162
0:10:02,96 --> 0:10:05,98
There is an opinion that 2 checkpoints
is better because the

163
0:10:05,98 --> 0:10:09,58
first one can be really long and
during that you can accumulate

164
0:10:09,62 --> 0:10:10,68
a lot of dirty buffers.

165
0:10:10,68 --> 0:10:13,00
So, probably 2 checkpoints and then
shut down.

166
0:10:15,26 --> 0:10:21,05
And actually, why can we accumulate
a lot of and why would we want

167
0:10:21,05 --> 0:10:23,32
to accumulate a lot of dirty buffers?

168
0:10:23,32 --> 0:10:27,18
Because this is interesting, and
this will be related to our

169
0:10:28,42 --> 0:10:29,84
startup timing troubleshooting.

170
0:10:30,76 --> 0:10:35,42
So, if you have default settings,
max-wall-size is just 1 GB,

171
0:10:35,90 --> 0:10:40,06
current workloads are huge, and
1 GB is not a lot, actually.

172
0:10:40,52 --> 0:10:46,92
So you probably want to increase
the distance in time between

173
0:10:46,92 --> 0:10:50,58
your checkpoints, because if you
do checkpoints too frequently,

174
0:10:50,66 --> 0:10:52,62
you have two kinds of overhead.

175
0:10:53,50 --> 0:10:57,66
First kind of overhead. We actually
discussed it, I think we

176
0:10:57,66 --> 0:11:02,28
had a special episode about it,
but just in case, let me repeat.

177
0:11:03,26 --> 0:11:09,48
The first kind of overhead is that
you just flushed your dirty

178
0:11:09,48 --> 0:11:13,12
buffer, it became clean, and then
immediately after it, or slightly

179
0:11:13,22 --> 0:11:17,98
later, before the next checkpoint,
some backend again changed

180
0:11:17,98 --> 0:11:19,76
the content of this buffer.

181
0:11:20,54 --> 0:11:21,80
And you need to...

182
0:11:21,98 --> 0:11:23,98
Like, it became dirty once again.

183
0:11:24,44 --> 0:11:28,28
And if you have a longer distance
between two checkpoints, a few

184
0:11:28,28 --> 0:11:34,28
writes might happen in the same
buffer before you flush it.

185
0:11:34,28 --> 0:11:38,04
So instead of flushing it multiple
times, you could flush it

186
0:11:38,04 --> 0:11:39,56
just once, right?

187
0:11:39,66 --> 0:11:41,74
In this case, overhead becomes
smaller.

188
0:11:42,74 --> 0:11:45,86
And of course, if you have frequent
checkpoints, you have this

189
0:11:45,86 --> 0:11:49,46
chance to deal with the same buffer
multiple times in checkpoints.

190
0:11:50,74 --> 0:11:55,84
And what kind of workloads are
more likely to face this kind

191
0:11:55,84 --> 0:11:56,78
of overhead?

192
0:11:56,92 --> 0:11:58,92
More often, of course, random writes.

193
0:11:59,34 --> 0:12:02,90
So if you, for example, are performing
copy, it will be a quite kind

194
0:12:02,90 --> 0:12:09,66
of sequential load to your system,
so it's not going to experience

195
0:12:09,66 --> 0:12:11,56
this kind of problem a lot.

196
0:12:11,60 --> 0:12:15,78
But if you, for example, are trying
to update some records using

197
0:12:15,78 --> 0:12:20,94
some index scan, index scan can
give you tuples in very random

198
0:12:20,94 --> 0:12:22,94
places, in random buffers.

199
0:12:23,44 --> 0:12:29,26
And in this case, if it's a massive
random update, not random,

200
0:12:29,26 --> 0:12:33,90
I mean not absolutely random,
but just location of tuples

201
0:12:33,9 --> 0:12:39,96
It's not predictable as tuples are
randomly and sparsely stored, for

202
0:12:39,96 --> 0:12:41,0
example, and so on.

203
0:12:41,0 --> 0:12:44,38
In this case, there are good chances
that you will visit and

204
0:12:44,38 --> 0:12:48,4
revisit the same buffer during
this massive update multiple times.

205
0:12:48,4 --> 0:12:53,42
It can be not a single update,
it can be some background migration

206
0:12:53,46 --> 0:12:57,44
happening and you have updates in
batches, but still, you revisit

207
0:12:57,44 --> 0:13:1,1
the same buffer multiple times
and if the distance between checkpoints

208
0:13:1,1 --> 0:13:6,56
would be longer, in this case, you
would probably do multiple

209
0:13:6,56 --> 0:13:11,6
writes and just 1 synchronization
with disk when checkpoint is

210
0:13:11,6 --> 0:13:12,1
happening.

211
0:13:12,5 --> 0:13:15,72
But it's not the only problem.

212
0:13:15,72 --> 0:13:18,48
The second problem is full-page
writes.

213
0:13:19,14 --> 0:13:22,4
By default, full-page writes are
enabled because block size on

214
0:13:22,4 --> 0:13:24,52
disk is a file system.

215
0:13:24,86 --> 0:13:26,62
Ext4 is 4 kilobytes.

216
0:13:27,52 --> 0:13:30,12
Our buffer size is 8 kilobytes.

217
0:13:30,48 --> 0:13:37,34
So to avoid partial writes,
Postgres needs to write the whole

218
0:13:37,34 --> 0:13:43,38
buffer to WAL if it's the very
first change of the buffer after

219
0:13:43,38 --> 0:13:44,44
the latest checkpoint.

220
0:13:44,68 --> 0:13:50,28
So if right after our checkpoint
we change something in the buffer,

221
0:13:50,28 --> 0:13:54,02
for example, just, I don't know,
like, a single tuple, we change

222
0:13:54,02 --> 0:13:56,08
a single tuple, so new version is created.

223
0:13:57,7 --> 0:14:3,58
In this case, Postgres needs to
write the whole buffer to WAL, and

224
0:14:3,58 --> 0:14:7,12
if WAL compression is on, it will
compress it, which is probably

225
0:14:7,12 --> 0:14:11,62
good in many cases, maybe not always,
but sometimes it's good.

226
0:14:12,88 --> 0:14:14,94
Hello, chat, by the way, thank
you for joining.

227
0:14:17,0 --> 0:14:22,46
And subsequent changes of the same
buffer before next checkpoint

228
0:14:22,54 --> 0:14:27,42
are going to be much lighter, only
the tuple itself will be written,

229
0:14:27,52 --> 0:14:28,6
not the whole buffer.

230
0:14:29,48 --> 0:14:36,34
It means that if you have frequent
checkpoints, Postgres needs

231
0:14:36,34 --> 0:14:38,54
to write more full-page writes.

232
0:14:39,0 --> 0:14:42,54
It increases the size of WAL significantly,
right?

233
0:14:42,74 --> 0:14:51,28
And this overhead is propagated
to replication and to archiving,

234
0:14:51,66 --> 0:14:53,4
to a backup system, obviously.

235
0:14:53,66 --> 0:14:55,7
And also, of course, it takes more
disk space.

236
0:14:56,4 --> 0:15:0,36
Compression helps to some extent,
but of course, it's much better

237
0:15:0,36 --> 0:15:4,06
if you have infrequent checkpoints
and just write less.

238
0:15:4,94 --> 0:15:8,98
But infrequent checkpoints lead
to the problem we are discussing

239
0:15:8,98 --> 0:15:9,48
here.

240
0:15:9,52 --> 0:15:14,96
So we can accumulate a lot of dirty
pages, and the shutdown attempt

241
0:15:14,96 --> 0:15:19,1
will be long unless you issue an
explicit checkpoint or two.

242
0:15:19,82 --> 0:15:24,58
And we can actually see the number
of dirty pages in the buffer

243
0:15:24,58 --> 0:15:26,62
pool by just installing the standard extension.

244
0:15:27,56 --> 0:15:30,06
It's a contrib module shipped with
Postgres itself.

245
0:15:30,06 --> 0:15:31,16
It's called pg_buffercache.

246
0:15:32,16 --> 0:15:36,92
And there is, in this buffer cache
view, there is a flag, isDirty,

247
0:15:37,08 --> 0:15:38,42
I think, isDirty, right?

248
0:15:38,42 --> 0:15:39,94
It's a boolean flag.

249
0:15:40,08 --> 0:15:44,6
And you can just see how many buffers
are currently dirty.

250
0:15:44,8 --> 0:15:50,16
You can multiply them by 8 kibibytes
and understand the volume.

251
0:15:50,16 --> 0:15:56,82
of data that needs to be synchronized
with disk during the next checkpoint

252
0:15:56,82 --> 0:15:58,04
or shutdown checkpoint.

253
0:15:58,08 --> 0:16:0,68
So the idea of speeding up is simple.

254
0:16:0,72 --> 0:16:1,82
You just want...

255
0:16:2,46 --> 0:16:7,84
You cannot avoid shutdown checkpoint
unless you don't crash your

256
0:16:7,84 --> 0:16:8,34
Postgres.

257
0:16:9,32 --> 0:16:15,56
The idea is just to make its work
smaller, make its life simpler,

258
0:16:15,92 --> 0:16:20,28
just issuing 1 or 2 checkpoints
right before shutdown.

259
0:16:20,92 --> 0:16:22,06
This helps a lot.

260
0:16:22,06 --> 0:16:26,26
And this is actually, this should
be considered as a standard

261
0:16:26,26 --> 0:16:30,82
approach for fast restarts or fast
shutdown attempts when we

262
0:16:30,82 --> 0:16:35,82
need to, for example, perform
a minor upgrade, for example,

263
0:16:35,82 --> 0:16:39,78
or some other works, like other
kinds of upgrade or switchover

264
0:16:39,96 --> 0:16:40,66
or something.

265
0:16:40,94 --> 0:16:45,28
If you want to stop the current primary,
just do this.

266
0:16:46,5 --> 0:16:52,66
And finally the third reason for longer
stop, I discussed in my article,

267
0:16:52,66 --> 0:16:56,68
is lagging archiving.

268
0:16:58,08 --> 0:17:2,0
So lagging archiving of WALs,
It's related to backups.

269
0:17:2,12 --> 0:17:5,94
Archive command can be failing
or just to be slow.

270
0:17:6,42 --> 0:17:10,16
And we have accumulated some WALs which
are not yet archived, pending

271
0:17:10,16 --> 0:17:10,60
WALs.

272
0:17:10,6001 --> 0:17:15,74
You can see, for example, you can
see failures in LastLSN.

273
0:17:16,06 --> 0:17:18,54
I think it's possible to see last
lesson, right?

274
0:17:18,54 --> 0:17:22,92
In pg_stat_archiver, this is good
to monitor.

275
0:17:23,36 --> 0:17:27,52
Anyway, you want to monitor errors
there, and errors should be

276
0:17:27,52 --> 0:17:28,02
investigated.

277
0:17:28,62 --> 0:17:33,34
Because if you have accumulated a lot
of WALs which are not in the

278
0:17:33,34 --> 0:17:35,0
backups yet.

279
0:17:35,16 --> 0:17:39,18
It means that you might lose this
data in case of disaster, you

280
0:17:39,18 --> 0:17:43,2
lost all the nodes, you need to
recover from backups, but backups

281
0:17:43,2 --> 0:17:45,74
are lacking some WALs, right?

282
0:17:45,86 --> 0:17:46,72
It's not good.

283
0:17:46,72 --> 0:17:48,16
I mean, this is a serious situation.

284
0:17:48,6 --> 0:17:53,04
You want to monitor the lag of
archiving and to ensure that all

285
0:17:53,04 --> 0:17:55,4
WALs are archived in time.

286
0:17:55,94 --> 0:17:59,76
But here we deal with a slightly
different problem.

287
0:18:1,26 --> 0:18:6,3
One day I discovered it in production,
I discovered that Patroni

288
0:18:8,3 --> 0:18:13,12
failed to perform failover because
it was waiting, it was trying

289
0:18:13,12 --> 0:18:18,74
to stop Postgres and it was waiting
on archive command because

290
0:18:18,74 --> 0:18:22,32
a lot of WALs were accumulated
unnoticed.

291
0:18:23,16 --> 0:18:28,26
So one problem issue with backup
system and WAL archiving led

292
0:18:28,26 --> 0:18:29,26
to another problem.

293
0:18:29,26 --> 0:18:33,0
We cannot stop Postgres and perform
failover.

294
0:18:33,28 --> 0:18:36,66
Currently Patroni already fixed,
I think in version 2.1.2 it

295
0:18:36,66 --> 0:18:40,94
was fixed, thanks to Kukushkin
for the very...

296
0:18:41,04 --> 0:18:46,22
Kukushkin is the maintainer of Patroni,
kudos to Kukushkin for this

297
0:18:46,58 --> 0:18:48,98
very speedy reaction and bug fix.

298
0:18:50,46 --> 0:18:56,18
So right now Patroni will perform
failover and take care of WAL

299
0:18:56,18 --> 0:18:57,16
archiving separately.

300
0:18:58,2 --> 0:19:5,34
But the idea is that you just need
to monitor and make sure that all

301
0:19:5,38 --> 0:19:7,54
WAL archiving is not lagging.

302
0:19:7,72 --> 0:19:14,22
You can introduce the concept of
lag, of how many WALs are pending

303
0:19:14,22 --> 0:19:15,06
to be archived.

304
0:19:15,42 --> 0:19:19,44
And for example, say 5 is too much
already, too many.

305
0:19:19,7 --> 0:19:20,98
So we don't want it.

306
0:19:20,98 --> 0:19:25,12
We consider this as an incident
and we have alerts, we investigate,

307
0:19:25,38 --> 0:19:29,04
or you can measure the lag in bytes,
understanding that each

308
0:19:29,04 --> 0:19:35,08
WAL is 16 mebibyte, or it can
be adjusted actually, and RDS

309
0:19:35,08 --> 0:19:36,22
adjusts it.

310
0:19:36,22 --> 0:19:39,4
It has 64 megabytes there.

311
0:19:39,86 --> 0:19:44,68
So that's it for stop attempt,
stopping attempt.

312
0:19:44,96 --> 0:19:46,34
Let's talk about startup.

313
0:19:47,96 --> 0:19:52,62
By the way, interesting point that
sometimes like from my experience,

314
0:19:53,16 --> 0:19:57,56
people who don't understand what's
happening, they say, Oh, something

315
0:19:57,56 --> 0:19:58,52
wrong is happening.

316
0:19:58,52 --> 0:19:59,94
And they start to be nervous.

317
0:20:0,24 --> 0:20:4,0
And instead of diving into details
to understand what's happening

318
0:20:4,0 --> 0:20:9,1
with their ongoing attempt to stop
or start, they just try to

319
0:20:9,24 --> 0:20:15,32
redo it or to be more aggressive,
for example, kill minus nine SIG

320
0:20:15,74 --> 0:20:17,42
kill, which is not good at all.

321
0:20:17,78 --> 0:20:18,72
Don't do that.

322
0:20:18,84 --> 0:20:22,04
Just try to understand what's happening
and where we are currently

323
0:20:22,04 --> 0:20:22,8
in the process.

324
0:20:23,94 --> 0:20:28,38
So I think that's it with stop
action.

325
0:20:28,84 --> 0:20:30,14
What about start action?

326
0:20:30,84 --> 0:20:38,08
If Postgres was stopped without
issues and shutdown Checkpoint

327
0:20:38,16 --> 0:20:41,42
finished, everything should be
fine and if it's a primary it should

328
0:20:41,42 --> 0:20:42,68
start quite fast.

329
0:20:43,68 --> 0:20:49,54
But if it crashed and needs to
recover all WALs since the latest

330
0:20:49,54 --> 0:20:49,9
Checkpoint.

331
0:20:49,9 --> 0:20:52,22
This is the whole idea of Checkpoints
and WALs.

332
0:20:52,8 --> 0:20:57,42
After crash, we need to replay
WALs after the latest successful

333
0:20:57,5 --> 0:20:58,0
Checkpoint.

334
0:20:59,16 --> 0:21:3,7
And again, Of course, if Checkpoint
tuning was applied and the

335
0:21:3,7 --> 0:21:9,64
distance between Checkpoints is
bigger, then this startup, redo

336
0:21:9,66 --> 0:21:11,06
phase will take longer.

337
0:21:11,64 --> 0:21:15,1
Or if you restore from backups,
and this is a very, very common

338
0:21:15,1 --> 0:21:19,12
situation, people restore from
backups and don't understand why

339
0:21:19,12 --> 0:21:24,8
it's not starting, why it's not
accepting connections, and a

340
0:21:24,8 --> 0:21:27,74
lot of emotions, new attempts,
and so on.

341
0:21:29,44 --> 0:21:32,9
They see fatal, the database system
is not yet accepting connections.

342
0:21:32,9 --> 0:21:36,5
I think everyone who works with
Postgres long enough saw this

343
0:21:36,5 --> 0:21:37,0
error.

344
0:21:37,92 --> 0:21:40,9
Interesting that it's fatal, but
it's not fatal.

345
0:21:41,12 --> 0:21:43,02
It's normal, they just need...

346
0:21:43,14 --> 0:21:44,44
We just need to wait.

347
0:21:45,66 --> 0:21:49,54
And the problem here is that it's
not easy to understand how

348
0:21:49,54 --> 0:21:50,58
long to wait.

349
0:21:50,98 --> 0:21:55,64
I think if you're interested in
hacking Postgres, I think it's

350
0:21:55,64 --> 0:22:0,48
a good idea to improve this area
and try to deliver some good

351
00:22:00,48 --> 00:22:03,9
knowledge to users how long to
wait.

352
00:22:04,44 --> 00:22:09,4
For example, we can see in logs
that redo process has started,

353
00:22:09,9 --> 00:22:17,18
but we have no idea from which
position it started and which

354
00:22:17,18 --> 00:22:23,94
position will be considered as
consistent and ready for work.

355
00:22:28,38 --> 00:22:35,28
So, if we ended up in this situation
and we see such kind of

356
00:22:35,28 --> 00:22:40,58
error and understand that Postgres
is performing redo, we can

357
00:22:41,74 --> 00:22:48,48
find a few things, especially if
it's self-managed Postgres and

358
00:22:48,48 --> 00:22:50,82
everything is under our control.

359
00:22:51,6 --> 00:22:59,54
First of all, you can inspect the
process list and see the Postgres

360
00:22:59,54 --> 00:23:01,94
process which is performing redo.

361
00:23:03,28 --> 00:23:08,74
This will, just with ps command,
you can grep startup recovering

362
00:23:08,9 --> 00:23:12,48
and see, it will give you LSN,
basically.

363
00:23:13,08 --> 00:23:17,36
LSN, it's a log sequence number,
it's position inside WAL, sequential

364
00:23:17,44 --> 00:23:17,94
position.

365
00:23:18,74 --> 00:23:19,54
And this is great.

366
00:23:19,54 --> 00:23:22,28
So you understand, like you can
see, you can check it multiple

367
00:23:22,28 --> 00:23:23,66
times and see it's progressing.

368
00:23:24,06 --> 00:23:28,98
It's already a huge relief for
most people to understand that

369
00:23:29,06 --> 00:23:32,36
we are waiting for something which
is happening.

370
00:23:32,56 --> 00:23:36,82
Because the worst thing is that
nothing is happening and we are

371
00:23:36,82 --> 00:23:38,1
waiting for 10 minutes.

372
00:23:38,4 --> 00:23:39,3
It's not progressing.

373
00:23:40,84 --> 00:23:42,26
Like, what to do?

374
00:23:42,34 --> 00:23:46,28
But usually we see it's progressing,
meaning that redo is happening,

375
00:23:46,56 --> 00:23:47,92
we are moving forward.

376
00:23:48,74 --> 00:23:51,54
And next question, how long else
to wait?

377
00:23:52,12 --> 00:23:55,58
In this case, you need pg_controldata.

378
00:23:55,84 --> 00:24:00,46
pg_controldata is a standard program
shipped with Postgres, and

379
00:24:00,46 --> 00:24:06,58
you need to specify -D PGDATA, data directory, and

380
00:24:06,58 --> 00:24:12,18
it will show you a lot of metadata
about your Postgres cluster.

381
00:24:12,5 --> 00:24:16,44
Cluster here we mean, like, clusters
an overloaded term, but

382
00:24:16,44 --> 00:24:20,0
here we mean our data directory
basically.

383
00:24:20,54 --> 00:24:23,62
It will tell you a lot of interesting
stuff, but also it will

384
00:24:23,62 --> 00:24:30,56
tell you the position at which
Postgres will consider it as a consistency

385
00:24:30,86 --> 00:24:31,36
point.

386
00:24:32,22 --> 00:24:38,74
So you can see like redo location,
checkpoint location, and also

387
00:24:38,74 --> 00:24:40,52
minimum recovery ending location.

388
00:24:40,76 --> 00:24:46,14
Actually, to be precise, this minimum
recovery ending location

389
00:24:46,24 --> 00:24:50,24
is only related to cases when we
recover from backups.

390
00:24:50,68 --> 00:24:52,46
We store Postgres from backups.

391
00:24:54,52 --> 00:24:59,64
If we deal with post-crash recovery,
this unfortunately will

392
00:24:59,64 --> 00:25:00,32
be 0.0.

393
00:25:00,62 --> 00:25:05,1
So we don't have this information,
we don't know when Postgres

394
00:25:05,16 --> 00:25:08,3
will consider itself as successful.

395
00:25:08,4 --> 00:25:11,76
But in this case, we can inspect
pg_wal directory and understand,

396
00:25:12,44 --> 00:25:17,8
like just check the latest, the
highest value, and we can understand

397
00:25:18,08 --> 00:25:20,42
LSN from the file name.

398
00:25:20,68 --> 00:25:22,94
I have another how-to explaining
this.

399
00:25:24,56 --> 00:25:28,92
So we have 2 ways of understanding
here.

400
00:25:29,44 --> 00:25:34,4
And unfortunately, if it's managed
Postgres service like RDS

401
0:25:34,4 --> 0:25:38,74
and they don't provide you capability
to use pg_controldata.

402
0:25:39,96 --> 0:25:43,78
If they provide you only SQL, and
currently we cannot use SQL

403
0:25:43,78 --> 0:25:45,88
because Postgres is not accepting
connections, right?

404
0:25:45,88 --> 0:25:48,4
So, in this case, I don't know what
to do.

405
0:25:48,48 --> 0:25:54,34
Ask your vendor of managed Postgres,
and maybe they need to improve

406
0:25:54,34 --> 0:25:58,72
and provide, for example, some
interface, some API, CLI interface

407
0:25:59,34 --> 0:26:1,88
to pg_controldata and process
list.

408
0:26:2,08 --> 0:26:6,0
Because when Postgres is starting
long, because we tune checkpoints,

409
0:26:6,36 --> 0:26:7,48
because we want, right?

410
0:26:7,48 --> 0:26:8,6
And it's starting long.

411
0:26:8,6 --> 0:26:12,54
We want to understand where we
are and how long to wait.

412
0:26:12,6 --> 0:26:16,22
Again, Postgres could report this
in logs, and this is a good

413
0:26:16,22 --> 0:26:18,78
idea to think about implementing
this.

414
0:26:18,84 --> 0:26:20,46
I think it's not super hard.

415
0:26:21,28 --> 0:26:24,5
Unfortunately, I'm not a hacker
and I don't have a lot of time.

416
0:26:24,6 --> 0:26:28,44
I could probably do it, but I'm
not a hacker.

417
0:26:28,58 --> 0:26:34,54
But I have this idea and if you
have interest in this, I think

418
0:26:34,54 --> 0:26:37,06
it would be great to see in logs
LSN positions.

419
0:26:37,54 --> 0:26:43,32
I know recently in Postgres 16,
Checkpoint now reports LSN positions,

420
0:26:43,52 --> 0:26:46,12
and Checkpoint position is reported
to logs.

421
0:26:46,12 --> 0:26:48,54
It's a new feature of Postgres
16.

422
0:26:49,02 --> 0:26:55,22
So next logical development in
this area could be, if we recover,

423
0:26:55,32 --> 0:27:0,18
redo should report details about
where we started, where we are.

424
0:27:0,18 --> 0:27:5,02
For example, each 30 seconds it
could report, so we could see

425
0:27:5,02 --> 0:27:6,54
the progress right in the logs.

426
0:27:6,74 --> 0:27:12,3
And finally, where, like how long
to wait, where we will stop.

427
0:27:12,94 --> 0:27:15,92
Obviously, like if you have these
data points in terms of LSN

428
0:27:16,16 --> 0:27:19,44
and you observe the progress, you
can predict, you can have some

429
0:27:19,44 --> 0:27:22,08
forecast, it's easy, you understand
the speed.

430
0:27:22,82 --> 0:27:27,18
As a reminder, you can take 2 LSNs
and in working postgres you

431
0:27:27,18 --> 0:27:31,66
can just subtract 1 from another
and the difference will be in

432
0:27:31,66 --> 0:27:32,16
bytes.

433
0:27:32,22 --> 0:27:33,22
So it's good.

434
0:27:33,42 --> 0:27:36,62
And if we see the difference in
bytes, we can understand the

435
0:27:36,62 --> 0:27:37,12
speed.

436
0:27:37,66 --> 0:27:42,7
If we have timestamps of current
position, we understand, for

437
0:27:42,7 --> 0:27:47,38
example, each minute we replay,
I don't know, like 5 gigabytes,

438
0:27:47,38 --> 0:27:49,16
for example, or 2 gigabytes.

439
0:27:49,16 --> 0:27:53,4
Depends on your hardware resources.

440
0:27:55,24 --> 0:27:59,36
In this case, we understand the
speed and assuming the speed is basically...

441
0:28:0,04 --> 0:28:1,92
We are moving at the same speed
all the time.

442
0:28:1,92 --> 0:28:5,1
Of course, it's not so, but we
can assume it.

443
0:28:5,9 --> 0:28:10,6
In this case, knowing the final
point, we can predict when we

444
0:28:10,6 --> 0:28:13,04
will reach it.

445
0:28:13,38 --> 0:28:17,58
And this is, I did it many times
because I have systems with

446
0:28:17,8 --> 0:28:22,8
large max_wal_size and checkpoint
timeout, for example, 100 gigabytes

447
0:28:22,82 --> 0:28:26,66
max_wal_size and checkpoint duration
30 or 60 minutes.

448
0:28:27,26 --> 0:28:31,56
And of course, startup, when recovering
from backups or after

449
0:28:31,56 --> 0:28:34,58
crash, it takes quite long.

450
0:28:35,2 --> 0:28:39,28
And without understanding, I was
myself in that situation and

451
00:28:39,280 --> 00:28:43,840
I saw so many times people become
nervous, they restart the process

452
00:28:43,860 --> 00:28:47,820
and this is not an efficient way to
deal with this problem at all.

453
00:28:48,580 --> 00:28:51,240
Okay, I hope this is helpful.

454
00:28:51,050 --> 00:28:53,820
Now let's see if we have questions
in chat.

455
00:28:54,240 --> 00:28:54,740
No?

456
00:28:55,520 --> 00:28:56,540
Another hi.

457
00:28:56,680 --> 00:28:57,420
Hi again.

458
00:28:58,200 --> 00:29:03,420
I hope this was helpful and if
you have questions, just leave

459
00:29:03,420 --> 00:29:04,320
them in the comments.

460
00:29:05,280 --> 00:29:06,860
And again, thank you everyone.

461
00:29:07,440 --> 00:29:12,020
We do it already like 66 weeks
in a row.

462
00:29:12,180 --> 00:29:16,420
Sometimes I'm alone or Michael
is alone, but we continue.

463
00:29:16,500 --> 00:29:21,240
And now I have a daily commitment
to have this Postgres marathon,

464
00:29:21,500 --> 00:29:22,480
Postgres how-tos.

465
00:29:23,720 --> 00:29:28,980
I want to reach 365 days in a row,
it will be challenging, but

466
00:29:29,280 --> 00:29:32,220
so far, today is day 11.

467
00:29:32,900 --> 00:29:34,000
So far it's working.

468
00:29:34,000 --> 00:29:35,867
I hope it will work.

469
00:29:35,867 --> 00:29:41,840
So check out my Twitter, LinkedIn
or the GitLab repository where

470
00:29:41,840 --> 00:29:44,520
these articles are stored.

471
00:29:45,160 --> 00:29:46,520
Okay, thank you so much.

472
00:29:46,980 --> 00:29:47,860
Until next one.
