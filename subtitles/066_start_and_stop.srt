1
0:0:0,58 --> 0:0:3,98
Nikolay: So, hello, hello, this
is PostgresFM episode number

2
0:0:4,2799997 --> 0:0:7,06
66 or 67, I don't remember.

3
0:0:7,8999996 --> 0:0:14,0199995
My name is Nikolay and I'm alone
today because Michael couldn't

4
0:0:14,0199995 --> 0:0:14,74
make it.

5
0:0:15,16 --> 0:0:21,3
So I don't like to be alone, actually,
and I don't like to record

6
0:0:21,3 --> 0:0:23,18
myself alone in advance.

7
0:0:23,32 --> 0:0:24,779999
So that's why it's live.

8
0:0:25,02 --> 0:0:27,84
But feel free to ask questions
in the chat.

9
0:0:28,779999 --> 0:0:32,059998
I also have my co-host will be
today, Charjit P.T.

10
0:0:34,18 --> 0:0:35,32
Hello, Charjit P.T.

11
0:0:36,28 --> 0:0:37,22
Hello, Nikolai.

12
0:0:43,5 --> 0:0:46,9
ChatGPT: It's great to be here
co-hosting PostgreSFM with you.

13
0:0:47,08 --> 0:0:50,84
To our listeners, I'm eager to
engage with your questions and

14
0:0:50,84 --> 0:0:52,06
thoughts in the chat.

15
0:0:52,42 --> 0:0:54,16
So Nikolai, what's today's topic?

16
0:0:55,6 --> 0:1:0,46
Nikolay: Okay, I hope this ChargerPT
thing will pronounce PostgreSQL

17
0:1:0,88 --> 0:1:2,3
correctly, I doubt.

18
0:1:2,98 --> 0:1:8,6
But today we are going to talk
about how to start and stop Postgres,

19
0:1:8,86 --> 0:1:12,62
or stop and start Postgres faster,
and how to troubleshoot if

20
0:1:12,62 --> 0:1:13,66
it takes long.

21
0:1:15,04 --> 0:1:22,16
I recently started a new series
of my small how-to articles.

22
0:1:22,82 --> 0:1:26,68
I do it on Twitter initially, but
then it spread to my LinkedIn,

23
0:1:26,72 --> 0:1:32,06
and also there is a markdown version
on GitLab repositories,

24
0:1:33,08 --> 0:1:35,82
where I already accumulated 10.

25
0:1:37,5 --> 0:1:39,26
I started it 10 days ago.

26
0:1:40,6 --> 0:1:47,0
The idea is, too much things in
my head that could be on paper

27
0:1:47,5 --> 0:1:53,0
with examples and, you know, like
SQL queries or some snippets

28
0:1:53,479996 --> 0:1:53,979996
for...

29
0:1:54,86 --> 0:1:58,52
You can run in console, like, bash
snippets or something.

30
0:1:59,12 --> 0:2:2,92
And, for example, yesterday I talked
about how to create flame

31
0:2:2,92 --> 0:2:7,94
graphs if you have issues with
some Postgres running on your

32
0:2:8,04 --> 0:2:8,98
Linux machine.

33
0:2:9,12 --> 0:2:12,88
Of course, it wouldn't work on
RDS or other managed services,

34
0:2:13,32 --> 0:2:18,14
but you can, if you have, for example,
some issue on production,

35
0:2:18,14 --> 0:2:22,1
you can reproduce it on a standalone
Postgres on a Linux machine.

36
0:2:22,12 --> 0:2:29,44
And then, PRF or eBPF tooling gives
you very powerful capabilities

37
0:2:30,06 --> 0:2:33,06
to see what's going on under the
hood.

38
0:2:33,26 --> 0:2:38,14
So FlameGraphs is a very good tool
to understand which functions

39
0:2:39,06 --> 0:2:42,84
in the source code take too much
time in CPU.

40
0:2:43,52 --> 0:2:49,54
But today we are going to discuss
stop and start duration of

41
0:2:49,54 --> 0:2:50,0
Postgres.

42
0:2:50,0 --> 0:2:53,44
And this, of course, can be applied
to any Postgres, any version

43
0:2:53,44 --> 0:2:57,42
of it, and including, of course,
RDS or any other managed services.

44
0:2:58,14 --> 0:3:1,66
So let's start with stop attempts.

45
0:3:1,98 --> 0:3:6,26
For example, you want to stop Postgres
and see it takes long.

46
0:3:7,36 --> 0:3:10,14
What kind of reasons we can have?

47
0:3:21,48 --> 0:3:24,6
ChatGPT: That sounds like a valuable
series of how-to articles

48
0:3:24,6 --> 0:3:25,98
you've been creating, Nikolai.

49
0:3:26,2 --> 0:3:30,66
Understanding the nuances of Postgres
and its operations is crucial

50
0:3:30,66 --> 0:3:32,38
for many developers and admins.

51
0:3:33,28 --> 0:3:36,48
Regarding your question on the
duration of stopping Postgres,

52
0:3:36,9 --> 0:3:39,88
there are several reasons it might
take longer than expected.

53
0:3:40,94 --> 0:3:42,76
1, active transactions.

54
0:3:43,62 --> 0:3:46,48
If there are active transactions
when you attempt to stop the

55
0:3:46,48 --> 0:3:49,92
service, Postgres will wait for
them to complete before shutting

56
0:3:49,92 --> 0:3:50,42
down.

57
0:3:50,98 --> 0:3:53,0
2, write ahead logs.

58
0:3:53,3 --> 0:3:56,62
Postgres could be flushing the
wall to disk, which can sometimes

59
0:3:56,68 --> 0:3:58,12
delay the shutdown process.

60
0:3:58,26 --> 0:3:59,76
3, background processes.

61
0:4:0,12 --> 0:4:3,54
Postgres runs several background
processes, like auto vacuum.

62
0:4:4,44 --> 0:4:7,96
These processes might delay the
shutdown if they're in the middle

63
0:4:7,96 --> 0:4:8,8
of their operations.

64
0:4:9,52 --> 0:4:11,18
4, connection draining.

65
0:4:11,4 --> 0:4:14,34
If there are active connections,
Postgres will wait for them

66
0:4:14,34 --> 0:4:17,74
to disconnect or reach a safe state
to shut down.

67
0:4:18,22 --> 0:4:22,12
For speeding up the process, 1
might consider issuing a fast

68
0:4:22,12 --> 0:4:25,62
shutdown command, which skips waiting
for active transactions

69
0:4:25,76 --> 0:4:26,44
to complete.

70
0:4:28,52 --> 0:4:32,08
Nikolay: Okay, here I can interrupt
without hesitating.

71
0:4:32,98 --> 0:4:36,96
Unlike when I'm with Michael, I'm
always hesitating to interrupt

72
0:4:36,96 --> 0:4:38,7
Michael, but do it all the time,
unfortunately.

73
0:4:39,16 --> 0:4:44,74
So I see, like, honestly, I didn't
prepare this thing beforehand,

74
0:4:44,8 --> 0:4:46,96
so I didn't provide any ideas.

75
0:4:46,96 --> 0:4:52,76
So we see obviously a couple of
first reasons, CharterPT mentions

76
0:4:52,96 --> 0:4:55,42
are really correct.

77
0:4:55,44 --> 0:5:0,86
I mean, they are the same as I
list in my how-to article.

78
0:5:1,56 --> 0:5:4,74
I explain 3 reasons and how to
deal with them.

79
0:5:4,76 --> 0:5:10,44
And first of all, the very first
is indeed ongoing long running

80
0:5:11,06 --> 0:5:11,56
transactions.

81
0:5:12,28 --> 0:5:19,94
So if we, usually if we try to
stop using the default way, it's

82
0:5:19,94 --> 0:5:21,72
the so-called smart way.

83
0:5:22,36 --> 0:5:26,94
Postgres is going to wait until
ongoing work is done, and this

84
0:5:26,94 --> 0:5:29,24
is probably not what you want.

85
0:5:30,06 --> 0:5:34,16
Because, of course, maybe you need
to stop faster, so you need

86
0:5:34,16 --> 0:5:37,52
to use so-called fast shutdown.

87
0:5:38,24 --> 0:5:40,88
It's pgctl-amp fast.

88
0:5:42,86 --> 0:5:44,28
Default is called smart.

89
0:5:45,06 --> 0:5:50,3
And in this case, it will issue
SIGINT instead of SIGTERM signal

90
0:5:50,66 --> 0:5:52,12
to the Postgres process.

91
0:5:52,92 --> 0:5:57,94
And it means that Postgres will
interrupt all ongoing transactions

92
0:5:58,26 --> 0:6:2,06
and proceed with the shutdown process.

93
0:6:2,3 --> 0:6:5,32
Shutdown process is kind of quite
complicated.

94
0:6:6,04 --> 0:6:11,48
If you can study source code, I
provide links in my article.

95
0:6:11,48 --> 0:6:15,36
I will provide them here in the
show notes as well.

96
0:6:18,08 --> 0:6:19,08
This is simple.

97
0:6:19,08 --> 0:6:20,52
You just use fast shutdown.

98
0:6:20,92 --> 0:6:23,9
You understand that ongoing transactions
will be interrupted,

99
0:6:24,4 --> 0:6:27,32
but 2 other reasons are much more
interesting.

100
0:6:27,38 --> 0:6:32,92
And let's start with wall-related
and checkpoint-related reason.

101
0:6:38,84 --> 0:6:39,62
I don't know.

102
0:6:39,62 --> 0:6:40,38
Is it working?

103
0:6:40,38 --> 0:6:41,16
I don't know.

104
0:6:41,84 --> 0:6:42,68
Are you working?

105
0:6:42,88 --> 0:6:43,78
ChatGPT: Certainly, Nikolai.

106
0:6:43,94 --> 0:6:44,44
Nikolay: Okay.

107
0:6:45,48 --> 0:6:48,48
ChatGPT: The interaction between
the write-ahead logs well and

108
0:6:48,48 --> 0:6:52,16
checkpoints plays a crucial role
in PostgreSQL's shutdown process.

109
0:6:53,7 --> 0:6:56,82
When you initiate a shutdown, if
there's a pending checkpoint,

110
0:6:57,44 --> 0:7:1,36
Postgres will attempt to complete
that checkpoint before shutting

111
0:7:1,36 --> 0:7:1,86
down.

112
0:7:13,14 --> 0:7:13,19382
Nikolay: Checkpoints are most...

113
0:7:13,19382 --> 0:7:13,28345
No, no, no, no, no.

114
0:7:13,28345 --> 0:7:13,5345
When we try to shut down Postgres,
first of all, there is a thing

115
0:7:13,5345 --> 0:7:13,57034
which is

116
0:7:13,57034 --> 0:7:13,64206
Nikolay: called a shutdown checkpoint.

117
0:7:13,64206 --> 0:7:18,82
So it's like always, So we try
to shut down and Postgres wants

118
0:7:18,82 --> 0:7:20,28
to perform shutdown checkpoint.

119
0:7:20,28 --> 0:7:23,96
That means that all dirty buffers
in the buffer pool need to

120
0:7:23,96 --> 0:7:25,58
be flushed to disk.

121
0:7:26,18 --> 0:7:30,36
So this is a clean shutdown.

122
0:7:30,72 --> 0:7:41,44
If you issue CKILL or KILL-9, which
is like kind of crushing

123
0:7:41,44 --> 0:7:46,3
your Postgres or just switching
off power of whole machine.

124
0:7:46,56 --> 0:7:50,24
In this case, of course, it won't
do it because it doesn't have

125
0:7:50,46 --> 0:7:53,1
ability and chances to do it.

126
0:7:53,18 --> 0:7:57,02
But in other cases, it tries to
perform shutdown checkpoint.

127
0:7:57,66 --> 0:8:1,22
And the trick with shutdown checkpoint
is while it's doing it,

128
0:8:1,22 --> 0:8:2,86
it cannot accept new connections.

129
0:8:2,98 --> 0:8:6,3
And this is bad, because if we
accumulated a lot of dirty buffers

130
0:8:6,3 --> 0:8:10,14
in the buffer pool, it means, you
know, like, I don't like it.

131
0:8:10,14 --> 0:8:11,54
I like Michael better.

132
0:8:12,2 --> 0:8:13,22
I don't like...

133
0:8:13,88 --> 0:8:16,5
It's, like, not working really
well.

134
0:8:17,5 --> 0:8:19,18
Let's do it alone completely.

135
0:8:20,66 --> 0:8:25,68
So if you issue shutdown, shutdown
checkpoint can be huge.

136
0:8:26,58 --> 0:8:29,76
Of course, if your settings are
default, it won't be huge because

137
0:8:29,76 --> 0:8:31,4
max wall size is 1 gigabyte.

138
0:8:31,42 --> 0:8:36,54
It means that only like roughly
half a gigabyte of dirty, of

139
0:8:36,54 --> 0:8:42,42
wall data can be accumulated in
the wall since the latest checkpoint.

140
0:8:43,66 --> 0:8:49,2
And also, if you have checkpoint
timeout default, it's also small,

141
0:8:49,2 --> 0:8:51,24
I think it's 5 minutes, right?

142
0:8:51,82 --> 0:8:55,66
In this case, we don't accumulate
a lot of dirty buffers.

143
0:8:55,92 --> 0:8:58,52
It means that shutdown checkpoint
will be fast, but still it's

144
0:8:58,52 --> 0:9:3,26
not really pleasant because while
Postgres is flashing dirty

145
0:9:3,26 --> 0:9:6,3
buffers on disk, it doesn't accept
connections.

146
0:9:6,68 --> 0:9:12,46
So the idea is we can run checkpoint
explicitly right before

147
0:9:12,66 --> 0:9:14,12
we shut down.

148
0:9:14,34 --> 0:9:19,66
It means that we issue fast checkpoint
using SQL command checkpoint,

149
0:9:20,74 --> 0:9:21,82
all dirty buffer.

150
0:9:21,82 --> 0:9:26,74
Dirty means changed buffers in
the buffer pool, but not yet synchronized

151
0:9:26,88 --> 0:9:27,54
to disk.

152
0:9:29,28 --> 0:9:33,82
We issue a checkpoint, Postgres
flushes all them on disk, And

153
0:9:33,82 --> 0:9:37,54
during this checkpoint, we're still
working, still accepting

154
0:9:37,54 --> 0:9:40,02
connections, so nobody actually
notices it.

155
0:9:40,44 --> 0:9:44,56
And then when we try to shut down
immediately after our checkpoint,

156
0:9:45,06 --> 0:9:48,28
The shutdown checkpoint is going
to be there still, but it will

157
0:9:48,28 --> 0:9:52,32
be small because we just performed
another checkpoint.

158
0:9:53,62 --> 0:9:54,44
This is great.

159
0:9:54,52 --> 0:9:56,76
Sometimes you want to do 2 checkpoints,
actually.

160
0:9:56,76 --> 0:10:0,72
I never did 2 checkpoints, but
recently we discussed in my team

161
0:10:0,72 --> 0:10:2,62
and it turned out some...

162
0:10:2,96 --> 0:10:5,98
There is an opinion that 2 checkpoints
is better because the

163
0:10:5,98 --> 0:10:9,58
first 1 can be really long and
during that you again accumulated

164
0:10:9,62 --> 0:10:10,68
a lot of dirty buffers.

165
0:10:10,68 --> 0:10:13,0
So probably 2 checkpoints and then
shut down.

166
0:10:15,26 --> 0:10:21,5
And actually why we can accumulate
a lot of, and why we can want

167
0:10:21,5 --> 0:10:23,32
to accumulate a lot of dirty buffers?

168
0:10:23,32 --> 0:10:27,18
Because this is interesting, and
this will be related to our

169
0:10:28,42 --> 0:10:29,84
startup timing troubleshooting.

170
0:10:30,76 --> 0:10:35,42
So, if you have default settings,
max-wall-size is just 1 GB,

171
0:10:35,9 --> 0:10:40,06
current workloads are huge, and
1 GB is not a lot, actually.

172
0:10:40,52 --> 0:10:46,92
So you probably want to increase
the distance in time between

173
0:10:46,92 --> 0:10:50,58
your checkpoints, because if you
do checkpoints too frequently,

174
0:10:50,66 --> 0:10:52,62
you have 2 kinds of overhead.

175
0:10:53,5 --> 0:10:57,66
First kind of overhead, we actually
discussed it, I think we

176
0:10:57,66 --> 0:11:2,28
had a special episode about it,
but just in case, let me repeat.

177
0:11:3,26 --> 0:11:9,48
The first kind of overhead is that
you just flushed your dirty

178
0:11:9,48 --> 0:11:13,12
buffer, it became clean, and then
immediately after it, or slightly

179
0:11:13,22 --> 0:11:17,98
later, before the next checkpoint,
some backend again changed

180
0:11:17,98 --> 0:11:19,76
the content of this buffer.

181
0:11:20,54 --> 0:11:21,8
And you need to...

182
0:11:21,98 --> 0:11:23,98
Like it became dirty once again.

183
0:11:24,44 --> 0:11:28,28
And if you have longer distance
between 2 checkpoints, a few

184
0:11:28,28 --> 0:11:34,28
writes might happen in the same
buffer before you flush it.

185
0:11:34,28 --> 0:11:38,4
So instead of flushing it multiple
times, you could flush it

186
0:11:38,4 --> 0:11:39,56
just once, right?

187
0:11:39,66 --> 0:11:41,74
In this case, overhead becomes
smaller.

188
0:11:42,74 --> 0:11:45,86
And of course, if you have frequent
checkpoints, you have this

189
0:11:45,86 --> 0:11:49,46
chance to deal with the same buffer
multiple times in checkpoints.

190
0:11:50,74 --> 0:11:55,84
And what kind of workloads are
more experienced with this kind

191
0:11:55,84 --> 0:11:56,78
of overhead?

192
0:11:56,92 --> 0:11:58,92
More often, of course, random writes.

193
0:11:59,34 --> 0:12:2,9
So if you, for example, performing
copy, it will be quite kind

194
0:12:2,9 --> 0:12:9,66
of sequential load to your system,
so it's not going to experience

195
0:12:9,66 --> 0:12:11,56
such kind of problem a lot.

196
0:12:11,6 --> 0:12:15,78
But if you, for example, trying
to update some records using

197
0:12:15,78 --> 0:12:20,94
some index scan, Index scan can
give you tuples in very random

198
0:12:20,94 --> 0:12:22,94
places, in random buffers.

199
0:12:23,44 --> 0:12:29,26
And in this case, if it's massive
random update, not random,

200
0:12:29,26 --> 0:12:33,9
I mean not random, absolutely random,
but just location of tuples

201
0:12:33,9 --> 0:12:39,96
is not predictable and tuples are
randomly sparsely stored, for

202
0:12:39,96 --> 0:12:41,0
example, and so on.

203
0:12:41,0 --> 0:12:44,38
In this case, there are good chances
that you will visit and

204
0:12:44,38 --> 0:12:48,4
revisit the same buffer during
this massive update multiple times.

205
0:12:48,4 --> 0:12:53,42
It can be not a single update,
it can be some background migration

206
0:12:53,46 --> 0:12:57,44
happening and you have update in
batches, but still you revisit

207
0:12:57,44 --> 0:13:1,1
the same buffer multiple times
and if the distance between checkpoints

208
0:13:1,1 --> 0:13:6,56
would be longer, in this case you
would probably do multiple

209
0:13:6,56 --> 0:13:11,6
writes and just 1 synchronization
with disk when checkpoint is

210
0:13:11,6 --> 0:13:12,1
happening.

211
0:13:12,5 --> 0:13:15,72
But it's not the only problem.

212
0:13:15,72 --> 0:13:18,48
The second problem is full-page
writes.

213
0:13:19,14 --> 0:13:22,4
By default, full-page writes are
enabled because block size on

214
0:13:22,4 --> 0:13:24,52
disk is on file system.

215
0:13:24,86 --> 0:13:26,62
Ext4 is 4 kilobytes.

216
0:13:27,52 --> 0:13:30,12
Our buffer size is 8 kilobytes.

217
0:13:30,48 --> 0:13:37,34
So to avoid the partial writes,
Posgus needs to write the whole

218
0:13:37,34 --> 0:13:43,38
buffer to wall if it's the very
first change of the buffer after

219
0:13:43,38 --> 0:13:44,44
the latest checkpoint.

220
0:13:44,68 --> 0:13:50,28
So If right after our checkpoint
we change something in the buffer,

221
0:13:50,28 --> 0:13:54,02
for example, just, I don't know,
like, single tuple, we change

222
0:13:54,02 --> 0:13:56,08
single tuple, so new version created.

223
0:13:57,7 --> 0:14:3,58
In this case, Postgres needs to
write whole buffer to wall, and

224
0:14:3,58 --> 0:14:7,12
if wall compression is on, it will
compress it, which is probably

225
0:14:7,12 --> 0:14:11,62
good in many cases, maybe not always,
but sometimes it's good.

226
0:14:12,88 --> 0:14:14,94
Hello, Chad, by the way, thank
you for joining.

227
0:14:17,0 --> 0:14:22,46
And Subsequent changes of the same
buffer pool before next checkpoint

228
0:14:22,54 --> 0:14:27,42
are going to be much lighter, only
the tuple itself will be written,

229
0:14:27,52 --> 0:14:28,6
not the whole buffer.

230
0:14:29,48 --> 0:14:36,34
It means that if you have frequent
checkpoints, Postgres needs

231
0:14:36,34 --> 0:14:38,54
to write more full-page writes.

232
0:14:39,0 --> 0:14:42,54
It increases the side of all significantly,
right?

233
0:14:42,74 --> 0:14:51,28
And this overhead is propagated
to replication and to archiving,

234
0:14:51,66 --> 0:14:53,4
to a backup system, obviously.

235
0:14:53,66 --> 0:14:55,7
And also, of course, it takes more
disk.

236
0:14:56,4 --> 0:15:0,36
Compression helps to some extent,
but of course, it's much better

237
0:15:0,36 --> 0:15:4,06
if you have infrequent checkpoints
and just write less.

238
0:15:4,94 --> 0:15:8,98
But infrequent checkpoints lead
to the problem we are discussing

239
0:15:8,98 --> 0:15:9,48
here.

240
0:15:9,52 --> 0:15:14,96
So we can accumulate a lot of dirty
pages, and the shutdown attempt

241
0:15:14,96 --> 0:15:19,1
will be long unless you issue an
explicit checkpoint or 2.

242
0:15:19,82 --> 0:15:24,58
And we can actually see the number
of dirty pages in the buffer

243
0:15:24,58 --> 0:15:26,62
pool just installing standard extension.

244
0:15:27,56 --> 0:15:30,06
It's a contrib module shipped with
Postgres itself.

245
0:15:30,06 --> 0:15:31,16
It's called pgBufferCache.

246
0:15:32,16 --> 0:15:36,92
And there is, in this buffer cache
view, there is a flag, isDirty,

247
0:15:37,08 --> 0:15:38,42
I think, isDirty, right?

248
0:15:38,42 --> 0:15:39,94
It's some boolean flag.

249
0:15:40,08 --> 0:15:44,6
And you can just see how many buffers
are currently dirty.

250
0:15:44,8 --> 0:15:50,16
You can multiply them by 8 kibibytes
and understand the volume

251
0:15:50,16 --> 0:15:56,82
of data that needs to be synchronized
with disk during next checkpoint

252
0:15:56,82 --> 0:15:58,04
or shutdown checkpoint.

253
0:15:58,08 --> 0:16:0,68
So the idea of speeding up is simple.

254
0:16:0,72 --> 0:16:1,82
You just want...

255
0:16:2,46 --> 0:16:7,84
You cannot avoid shutdown checkpoint
unless you don't crash your

256
0:16:7,84 --> 0:16:8,34
Postgres.

257
0:16:9,32 --> 0:16:15,56
The idea is just to make its work
smaller, make its life simpler,

258
0:16:15,92 --> 0:16:20,28
just issuing 1 or 2 checkpoints
right before shutdown.

259
0:16:20,92 --> 0:16:22,06
This helps a lot.

260
0:16:22,06 --> 0:16:26,26
And this is actually, this should
be considered as a standard

261
0:16:26,26 --> 0:16:30,82
approach for fast restarts or fast
shutdown attempts when we

262
0:16:30,82 --> 0:16:35,82
need to, for example, to perform
a minor upgrade, for example,

263
0:16:35,82 --> 0:16:39,78
or some other works, like other
kinds of upgrade or switchover

264
0:16:39,96 --> 0:16:40,66
or something.

265
0:16:40,94 --> 0:16:45,28
If you want to stop current primary,
just do this.

266
0:16:46,5 --> 0:16:52,66
And final third reason of longer
stop, I discussed in my article,

267
0:16:52,66 --> 0:16:56,68
is lagging archiving.

268
0:16:58,08 --> 0:17:2,0
So lagging archiving of walls,
It's related to backups.

269
0:17:2,12 --> 0:17:5,94
Archive command can be failing
or just to be slow.

270
0:17:6,42 --> 0:17:10,16
And we accumulated some walls which
are not yet archived, pending

271
0:17:10,16 --> 0:17:10,6001
walls.

272
0:17:10,6001 --> 0:17:15,74
You can see, for example, you can
see failures in LastLSN.

273
0:17:16,06 --> 0:17:18,54
I think it's possible to see last
lesson, right?

274
0:17:18,54 --> 0:17:22,92
In pgstartarchiver, this is good
to monitor.

275
0:17:23,36 --> 0:17:27,52
Anyway, you want to monitor errors
there, and errors should be

276
0:17:27,52 --> 0:17:28,02
investigated.

277
0:17:28,62 --> 0:17:33,34
Because if you accumulated a lot
of walls which are not in the

278
0:17:33,34 --> 0:17:35,0
backups yet.

279
0:17:35,16 --> 0:17:39,18
It means that you might lose this
data in case of disaster, you

280
0:17:39,18 --> 0:17:43,2
lost all the nodes, you need to
recover from backups, but backups

281
0:17:43,2 --> 0:17:45,74
are lacking some walls, right?

282
0:17:45,86 --> 0:17:46,72
It's not good.

283
0:17:46,72 --> 0:17:48,16
I mean, this is a serious situation.

284
0:17:48,6 --> 0:17:53,04
You want to monitor the lag of
archiving and to ensure that all

285
0:17:53,04 --> 0:17:55,4
walls are archived in time.

286
0:17:55,94 --> 0:17:59,76
But here we deal with a slightly
different problem.

287
0:18:1,26 --> 0:18:6,3
1 day I discovered it on production,
I discovered that Patroni

288
0:18:8,3 --> 0:18:13,12
failed to perform fallover because
it was waiting, it was trying

289
0:18:13,12 --> 0:18:18,74
to stop Postgres and it was waiting
on archive command because

290
0:18:18,74 --> 0:18:22,32
a lot of walls were accumulated
unnoticed.

291
0:18:23,16 --> 0:18:28,26
So 1 problem issue with backup
system and wall archiving led

292
0:18:28,26 --> 0:18:29,26
to another problem.

293
0:18:29,26 --> 0:18:33,0
We cannot stop Postgres and perform
failover.

294
0:18:33,28 --> 0:18:36,66
Currently Patroni already fixed,
I think in version 2.1.2 it

295
0:18:36,66 --> 0:18:40,94
was fixed, thanks to Kukushkin
for the very...

296
0:18:41,04 --> 0:18:46,22
Kukushkin is maintainer of Patroni,
kudos to Kukushkin for this

297
0:18:46,58 --> 0:18:48,98
very speedy reaction and bug fix.

298
0:18:50,46 --> 0:18:56,18
So right now Patroni will perform
failover and take care of wall

299
0:18:56,18 --> 0:18:57,16
archiving separately.

300
0:18:58,2 --> 0:19:5,34
But the idea is that you just need
to monitor and make sure that

301
0:19:5,38 --> 0:19:7,54
wall archiving is not lagging.

302
0:19:7,72 --> 0:19:14,22
You can introduce the concept of
lag, of how many walls are pending

303
0:19:14,22 --> 0:19:15,06
to be archived.

304
0:19:15,42 --> 0:19:19,44
And for example, say 5 is too much
already, too many.

305
0:19:19,7 --> 0:19:20,98
So we don't want it.

306
0:19:20,98 --> 0:19:25,12
We consider this as an incident
and we have alerts, we investigate,

307
0:19:25,38 --> 0:19:29,04
or you can measure the lag in bytes,
understanding that each

308
0:19:29,04 --> 0:19:35,08
wall is 16 mebibyte, or it can
be adjusted actually, and RDS

309
0:19:35,08 --> 0:19:36,22
adjusts it.

310
0:19:36,22 --> 0:19:39,4
It has 64 megabytes there.

311
0:19:39,86 --> 0:19:44,68
So that's it for stop attempt,
stopping attempt.

312
0:19:44,96 --> 0:19:46,34
Let's talk about startup.

313
0:19:47,96 --> 0:19:52,62
By the way, interesting point that
sometimes like from my experience,

314
0:19:53,16 --> 0:19:57,56
people who don't understand what's
happening, they say, Oh, something

315
0:19:57,56 --> 0:19:58,52
wrong is happening.

316
0:19:58,52 --> 0:19:59,94
And they start to be nervous.

317
0:20:0,24 --> 0:20:4,0
And instead of diving into details
to understand what's happening

318
0:20:4,0 --> 0:20:9,1
with their ongoing attempt to stop
or start, they just try to

319
0:20:9,24 --> 0:20:15,32
redo it or to be more aggressive,
for example, kill minus non-sick

320
0:20:15,74 --> 0:20:17,42
kill, which is not good at all.

321
0:20:17,78 --> 0:20:18,72
Don't do that.

322
0:20:18,84 --> 0:20:22,04
Just try to understand what's happening
and where we are currently

323
0:20:22,04 --> 0:20:22,8
in the process.

324
0:20:23,94 --> 0:20:28,38
So I think that's it with stop
action.

325
0:20:28,84 --> 0:20:30,14
What about start action?

326
0:20:30,84 --> 0:20:38,08
If Postgres was stopped without
issues and shutdown checkpoint

327
0:20:38,16 --> 0:20:41,42
finished, everything should be
fine and if it's primary it should

328
0:20:41,42 --> 0:20:42,68
start quite fast.

329
0:20:43,68 --> 0:20:49,54
But if it crashed and needs to
recover all walls since the latest

330
0:20:49,54 --> 0:20:49,9
checkpoint.

331
0:20:49,9 --> 0:20:52,22
This is the whole idea of checkpoints
and walls.

332
0:20:52,8 --> 0:20:57,42
After crash, we need to replay
walls after the latest successful

333
0:20:57,5 --> 0:20:58,0
checkpoint.

334
0:20:59,16 --> 0:21:3,7
And again, Of course, if checkpoint
tuning was applied and the

335
0:21:3,7 --> 0:21:9,64
distance between checkpoints is
bigger, then this startup, redo

336
0:21:9,66 --> 0:21:11,06
phase will take longer.

337
0:21:11,64 --> 0:21:15,1
Or if you restore from backups,
and this is a very, very common

338
0:21:15,1 --> 0:21:19,12
situation, people restore from
backups and don't understand why

339
0:21:19,12 --> 0:21:24,8
it's not starting, why it's not
accepting connections, and a

340
0:21:24,8 --> 0:21:27,74
lot of emotions, new attempts,
and so on.

341
0:21:29,44 --> 0:21:32,9
They see fatal, the database system
is not yet accepting connections.

342
0:21:32,9 --> 0:21:36,5
I think everyone who works with
Postgres long enough saw this

343
0:21:36,5 --> 0:21:37,0
error.

344
0:21:37,92 --> 0:21:40,9
Interesting that it's fatal, but
it's not fatal.

345
0:21:41,12 --> 0:21:43,02
It's normal, they just need...

346
0:21:43,14 --> 0:21:44,44
We just need to wait.

347
0:21:45,66 --> 0:21:49,54
And the problem here is that it's
not easy to understand how

348
0:21:49,54 --> 0:21:50,58
long to wait.

349
0:21:50,98 --> 0:21:55,64
I think if you're interested in
hacking Postgres, I think it's

350
0:21:55,64 --> 0:22:0,48
a good idea to improve this area
and try to deliver some good

351
0:22:0,48 --> 0:22:3,9
knowledge to users how long to
wait.

352
0:22:4,44 --> 0:22:9,4
For example, we can see in logs
that redo process has started,

353
0:22:9,9 --> 0:22:17,18
but we have no idea from which
position it started and which

354
0:22:17,18 --> 0:22:23,94
position will be considered as
consistent and ready for work.

355
0:22:28,38 --> 0:22:35,28
So, If we ended up in this situation
and we see such kind of

356
0:22:35,28 --> 0:22:40,58
error and understand that Postgres
is performing redo, we can

357
0:22:41,74 --> 0:22:48,48
find a few things, Especially if
it's self-managed Postgres and

358
0:22:48,48 --> 0:22:50,82
everything is under our control.

359
0:22:51,6 --> 0:22:59,54
First of all, you can inspect the
process list and see the Postgres

360
0:22:59,54 --> 0:23:1,94
process which is performing redo.

361
0:23:3,28 --> 0:23:8,74
This will, just with ps command,
you can grab startup recovering

362
0:23:8,9 --> 0:23:12,48
and see, it will give you LSN,
basically.

363
0:23:13,08 --> 0:23:17,36
LSN, it's a log sequence number,
it's position inside wall, sequential

364
0:23:17,44 --> 0:23:17,94
position.

365
0:23:18,74 --> 0:23:19,54
And this is great.

366
0:23:19,54 --> 0:23:22,28
So you understand, like you can
see, you can check it multiple

367
0:23:22,28 --> 0:23:23,66
times and see it's progressing.

368
0:23:24,06 --> 0:23:28,98
It's already a huge relief for
most people to understand that

369
0:23:29,06 --> 0:23:32,36
we are waiting for something which
is happening.

370
0:23:32,56 --> 0:23:36,82
Because the worst thing is that
nothing is happening and we are

371
0:23:36,82 --> 0:23:38,1
waiting for 10 minutes.

372
0:23:38,4 --> 0:23:39,3
It's not progressing.

373
0:23:40,84 --> 0:23:42,26
Like, what to do?

374
0:23:42,34 --> 0:23:46,28
But usually we see it's progressing,
meaning that redo is happening,

375
0:23:46,56 --> 0:23:47,92
we are moving forward.

376
0:23:48,74 --> 0:23:51,54
And next question, how long else
to wait?

377
0:23:52,12 --> 0:23:55,58
In this case, you need pgcontrol
data.

378
0:23:55,84 --> 0:24:0,46
Pgcontrol data is a standard program
shipped with Postgres, and

379
0:24:0,46 --> 0:24:6,58
you need to specify dash capital
D pgData, data directory, and

380
0:24:6,58 --> 0:24:12,18
it will show you a lot of metadata
about your Postgres cluster.

381
0:24:12,5 --> 0:24:16,44
Cluster here we mean, like, clusters
to overload the term, but

382
0:24:16,44 --> 0:24:20,0
here we mean our data directory
basically.

383
0:24:20,54 --> 0:24:23,62
It will tell you a lot of interesting
stuff, but also it will

384
0:24:23,62 --> 0:24:30,56
tell you the position at which
Postgres will consider it as consistency

385
0:24:30,86 --> 0:24:31,36
point.

386
0:24:32,22 --> 0:24:38,74
So you can see like redo location,
checkpoint location, and also

387
0:24:38,74 --> 0:24:40,52
minimum recovery ending location.

388
0:24:40,76 --> 0:24:46,14
Actually, to be precise, this minimum
recovery ending location

389
0:24:46,24 --> 0:24:50,24
is only related to cases when we
recover from backups.

390
0:24:50,68 --> 0:24:52,46
We store Postgres from backups.

391
0:24:54,52 --> 0:24:59,64
If we deal with post-crash recovery,
this unfortunately will

392
0:24:59,64 --> 0:25:0,32
be 0.0.

393
0:25:0,62 --> 0:25:5,1
So we don't have this information,
we don't know when Postgres

394
0:25:5,16 --> 0:25:8,3
will consider itself as successful.

395
0:25:8,4 --> 0:25:11,76
But in this case, we can inspect
pgwall directory and understand,

396
0:25:12,44 --> 0:25:17,8
like just check the latest, the
highest value, and we can understand

397
0:25:18,08 --> 0:25:20,42
LSN from the file name.

398
0:25:20,68 --> 0:25:22,94
I have another how-to explaining
this.

399
0:25:24,56 --> 0:25:28,92
So we have 2 ways of understanding
here.

400
0:25:29,44 --> 0:25:34,4
And unfortunately, if it's managed
Postgres service like RDS

401
0:25:34,4 --> 0:25:38,74
and they don't provide you capability
to use PG control data.

402
0:25:39,96 --> 0:25:43,78
If they provide you only SQL, and
currently we cannot use SQL

403
0:25:43,78 --> 0:25:45,88
because Postgres is not accepting
connections, right?

404
0:25:45,88 --> 0:25:48,4
So In this case, I don't know what
to do.

405
0:25:48,48 --> 0:25:54,34
Ask your vendor of managed Postgres,
and maybe they need to improve

406
0:25:54,34 --> 0:25:58,72
and provide, for example, some
interface, some API, CLI interface

407
0:25:59,34 --> 0:26:1,88
to PG control data and process
list.

408
0:26:2,08 --> 0:26:6,0
Because when Postgres is starting
long, because we tune checkpoints,

409
0:26:6,36 --> 0:26:7,48
because we want, right?

410
0:26:7,48 --> 0:26:8,6
And it's starting long.

411
0:26:8,6 --> 0:26:12,54
We want to understand where we
are and how long to wait.

412
0:26:12,6 --> 0:26:16,22
Again, Postgres could report this
in logs, and this is a good

413
0:26:16,22 --> 0:26:18,78
idea to think about implementing
this.

414
0:26:18,84 --> 0:26:20,46
I think it's not super hard.

415
0:26:21,28 --> 0:26:24,5
Unfortunately, I'm not a hacker
and I don't have a lot of time.

416
0:26:24,6 --> 0:26:28,44
I could probably do it, but I'm
not a hacker.

417
0:26:28,58 --> 0:26:34,54
But I have this idea And if you
have interest in this, I think

418
0:26:34,54 --> 0:26:37,06
it would be great to see in logs
LSN position.

419
0:26:37,54 --> 0:26:43,32
I know recently in Postgres 16,
Checkpoint now reports LSN positions,

420
0:26:43,52 --> 0:26:46,12
and Checkpoint position is reported
to logs.

421
0:26:46,12 --> 0:26:48,54
It's a new feature of Postgres
16.

422
0:26:49,02 --> 0:26:55,22
So next logical development in
this area could be, if we recover,

423
0:26:55,32 --> 0:27:0,18
redo should report details about
where we started, where we are.

424
0:27:0,18 --> 0:27:5,02
For example, each 30 seconds it
could report, so we could see

425
0:27:5,02 --> 0:27:6,54
the progress right in the logs.

426
0:27:6,74 --> 0:27:12,3
And finally, where, like how long
to wait, where we will stop.

427
0:27:12,94 --> 0:27:15,92
Obviously, like if you have these
data points in terms of LSN

428
0:27:16,16 --> 0:27:19,44
and you observe the progress, you
can predict, you can have some

429
0:27:19,44 --> 0:27:22,08
forecast, it's easy, you understand
the speed.

430
0:27:22,82 --> 0:27:27,18
As a reminder, you can take 2 LSNs
and in working postgres you

431
0:27:27,18 --> 0:27:31,66
can just subtract 1 from another
and the difference will be in

432
0:27:31,66 --> 0:27:32,16
bytes.

433
0:27:32,22 --> 0:27:33,22
So it's good.

434
0:27:33,42 --> 0:27:36,62
And if we see the difference in
bytes, we can understand the

435
0:27:36,62 --> 0:27:37,12
speed.

436
0:27:37,66 --> 0:27:42,7
If we have timestamps of current
position, we understand, for

437
0:27:42,7 --> 0:27:47,38
example, each minute we replay,
I don't know, like 5 gigabytes,

438
0:27:47,38 --> 0:27:49,16
for example, or 2 gigabytes.

439
0:27:49,16 --> 0:27:53,4
Depends on your hardware resources.

440
0:27:55,24 --> 0:27:59,36
In this case, we understand the
speed and assuming speed is basically...

441
0:28:0,04 --> 0:28:1,92
We are moving at the same speed
all the time.

442
0:28:1,92 --> 0:28:5,1
Of course, it's not so, but we
can assume it.

443
0:28:5,9 --> 0:28:10,6
In this case, knowing the final
point, we can predict when we

444
0:28:10,6 --> 0:28:13,04
will reach it.

445
0:28:13,38 --> 0:28:17,58
And this is, I did it many times
because I have systems with

446
0:28:17,8 --> 0:28:22,8
huge max-vol size and checkpoint
timeout, for example, 100 gigabytes

447
0:28:22,82 --> 0:28:26,66
max-vol size and checkpoint amount
30 or 60 minutes.

448
0:28:27,26 --> 0:28:31,56
And of course, startup, when recovering
from backups or after

449
0:28:31,56 --> 0:28:34,58
crash, there it takes quite long.

450
0:28:35,2 --> 0:28:39,28
And without understanding, I was
myself in that situation and

451
0:28:39,28 --> 0:28:43,84
I saw so many times people become
nervous, they restart the process

452
0:28:43,86 --> 0:28:47,82
and this is not efficient way to
deal with this problem at all.

453
0:28:48,58 --> 0:28:51,24
Okay, I hope this is helpful.

454
0:28:51,5 --> 0:28:53,82
Now let's see if we have questions
in chat.

455
0:28:54,24 --> 0:28:54,74
No?

456
0:28:55,52 --> 0:28:56,54
Another hi.

457
0:28:56,68 --> 0:28:57,42
Hi again.

458
0:28:58,2 --> 0:29:3,42
I hope this was helpful and If
you have questions, just leave

459
0:29:3,42 --> 0:29:4,32
them in the comments.

460
0:29:5,28 --> 0:29:6,86
And again, thank you everyone.

461
0:29:7,44 --> 0:29:12,02
We do it already like 66 weeks
in a row.

462
0:29:12,18 --> 0:29:16,42
Sometimes I'm alone or Michael
is alone, but we continue.

463
0:29:16,5 --> 0:29:21,24
And now I have a daily commitment
to have this Postgres marathon,

464
0:29:21,5 --> 0:29:22,48
Postgres how-tos.

465
0:29:23,72 --> 0:29:28,98
I want to reach 365 days in a row,
it will be challenging, but

466
0:29:29,28 --> 0:29:32,22
so far, Today is day 11.

467
0:29:32,9 --> 0:29:34,0
So far it's working.

468
0:29:34,0 --> 0:29:35,4867
I hope it will work.

469
0:29:35,4867 --> 0:29:41,84
So check out my Twitter, LinkedIn
or the GitLab repository where

470
0:29:41,84 --> 0:29:44,52
these articles are stored.

471
0:29:45,16 --> 0:29:46,52
Okay, thank you so much.

472
0:29:46,98 --> 0:29:47,86
Until next 1.
