1
0:0:1,06 --> 0:0:4,94
Hello, hello, this is
PostgresFM episode number 78,

2
0:0:6,04 --> 0:0:9,0199995
live, because I'm alone today again.

3
0:0:9,719999 --> 0:0:15,36
Michael is on vacations, holidays,
but I cannot allow us to miss

4
0:0:15,9 --> 0:0:21,0
any weeks, because we do it already
1 year and a half.

5
0:0:21,279999 --> 0:0:24,64
Episode 78, so no weeks are missed.

6
0:0:24,84 --> 0:0:27,54
So this week we'll do it as well.

7
0:0:27,599998 --> 0:0:29,08
But it will be a small episode.

8
0:0:29,16 --> 0:0:31,6
First of all, happy holidays everyone!

9
0:0:33,1 --> 0:0:39,64
It's December 29th, so it's holiday
season.

10
0:0:40,84 --> 0:0:43,7
And let's have some small episode
about workman.

11
0:0:43,78 --> 0:0:46,74
Somebody asked me to cover this
topic.

12
0:0:47,78 --> 0:0:51,239998
And I actually wrote a how-to,
I just haven't published it yet.

13
0:0:51,82 --> 0:0:57,18
I'm in my Postgres Marathon series,
where I publish how-tos every

14
0:0:57,18 --> 0:0:57,68
day.

15
0:0:58,08 --> 0:1:1,68
Almost, I'm lagging as well a little
bit, because of holiday

16
0:1:1,68 --> 0:1:3,66
season, but I'm going to catch
up.

17
0:1:4,46 --> 0:1:5,2
So workmem.

18
0:1:5,46 --> 0:1:8,74
Workmem is that everyone uses,
right?

19
0:1:8,74 --> 0:1:10,06
We all run queries.

20
0:1:11,76 --> 0:1:14,02
We need to use workmem.

21
0:1:15,24 --> 0:1:21,34
But this is super tricky, super,
how to say, basic setting, because

22
0:1:21,34 --> 0:1:22,44
everyone needs it.

23
0:1:23,8 --> 0:1:28,82
And it's also super tricky because
every statement can utilize

24
0:1:30,8 --> 0:1:32,0
less than workmem.

25
0:1:33,16 --> 0:1:38,24
This is limit, but it defines upper
limit, right?

26
0:1:38,24 --> 0:1:41,94
So if we need less, we use less.

27
0:1:42,12 --> 0:1:46,54
It's not like allocated amount
of memory, like shared buffers,

28
0:1:46,96 --> 0:1:48,84
the size of the buffer pool.

29
0:1:49,44 --> 0:1:53,1
But, so we can use less, any query
can use less.

30
0:1:53,5 --> 0:1:57,9
But the trickiest part is that
we don't know, any statement can

31
0:1:57,9 --> 0:1:59,12
be used multiple times.

32
0:2:0,02 --> 0:2:4,4
Less may be up to work memory,
but multiple times, because this

33
0:2:4,4 --> 0:2:8,98
is the amount of memory needed
for hashing, sorting operations.

34
0:2:10,12 --> 0:2:14,16
So if we have, for example, multiple
hashing operations inside

35
0:2:14,16 --> 0:2:19,86
1 query, for example, multiple
hash joins, we can use it multiple

36
0:2:19,86 --> 0:2:20,22
times.

37
0:2:20,22 --> 0:2:29,76
And this adds the kind of, this
like unpredictable, it's hard

38
0:2:29,76 --> 0:2:35,16
to define good algorithm to tune
it clearly, because we don't

39
0:2:35,16 --> 0:2:35,96
know, right?

40
0:2:36,02 --> 0:2:41,18
For example, we have some amount
of memory and we know our buffer

41
0:2:41,18 --> 0:2:45,6
pool size, it's simple and it's
another topic, but You define

42
0:2:45,6 --> 0:2:48,34
it once and you cannot change it
without restart.

43
0:2:48,34 --> 0:2:51,72
So we say, like some rule, most
people use it 25%.

44
0:2:52,58 --> 0:2:56,7
Okay, we allocate 25% for the buffer
pool.

45
0:2:57,66 --> 0:3:0,94
What's left we can use, and also
operational system can use for

46
0:3:0,94 --> 0:3:2,62
operational system page cache.

47
0:3:4,2 --> 0:3:7,94
We should not forget also that
there is also maintenance workman

48
0:3:7,94 --> 0:3:9,44
which is more predictable.

49
0:3:10,26 --> 0:3:14,94
It has some trickiness as well
because there is auto vacuum workman

50
0:3:14,94 --> 0:3:16,48
which is by default minus 1.

51
0:3:16,48 --> 0:3:21,38
It means maintenance workman will
be used and we have multiple

52
0:3:21,38 --> 0:3:23,94
workers for auto vacuums.

53
0:3:24,34 --> 0:3:30,04
So if we set it, for example, I
see people say to 2 gigabytes,

54
0:3:30,94 --> 0:3:34,54
it's maybe not really, it's quite
a lot.

55
0:3:34,54 --> 0:3:38,36
You need to ensure that, for example,
when you create an index,

56
0:3:38,88 --> 0:3:42,54
2 gigabytes is indeed helpful.

57
0:3:43,58 --> 0:3:48,1
But we can have multiple auto vacuum
workers, and I usually advocate

58
0:3:48,16 --> 0:3:52,74
to raise the vacuum workers a lot
so we can have many of them

59
0:3:52,74 --> 0:3:57,46
say 10 if you have a lot of CPUs
or maybe 20 even.

60
0:3:57,78 --> 0:4:3,06
It means if you set maintenance
workman to 2 gigabytes, auto

61
0:4:3,06 --> 0:4:7,2
vacuum workman is minus 1, means
it inherits from maintenance

62
0:4:7,2 --> 0:4:7,7
workman.

63
0:4:8,56 --> 0:4:11,3
Auto vacuum alone can use up to
20 gigabytes.

64
0:4:11,64 --> 0:4:16,96
It also depends because not for
everything it will use it, but

65
0:4:16,96 --> 0:4:22,46
anyway, we need to subtract these
20GB from the remaining memory.

66
0:4:22,9 --> 0:4:25,76
We also have some overhead for
additional processes.

67
0:4:26,82 --> 0:4:30,94
And then what's left, we can just
say, okay, we have max connection,

68
0:4:30,98 --> 0:4:32,16
say, 200.

69
0:4:32,56 --> 0:4:39,44
So we just divide the remaining
memory by 200 and this is roughly

70
0:4:39,48 --> 0:4:41,2
per each backend what we can use.

71
0:4:41,2 --> 0:4:46,2
But we don't know how many times
backends will use workman, right?

72
0:4:47,06 --> 0:4:52,28
So let's discuss the approach I'm
like kind of developed just

73
0:4:53,0 --> 0:4:53,98
writing this how-to.

74
0:4:54,86 --> 0:4:59,18
First, my favorite rule is this
Pareto principle rule 2080.

75
0:5:0,56 --> 0:5:6,18
So we take for example PGTune Leopard,
I forgot the name, but

76
0:5:6,18 --> 0:5:13,58
this is a very simple tuning heuristic
based tool, which is quite

77
0:5:13,58 --> 0:5:14,06
good.

78
0:5:14,06 --> 0:5:15,1
I mean, it's good.

79
0:5:15,1 --> 0:5:16,56
Good enough in many cases.

80
0:5:16,56 --> 0:5:21,9
You just use it, and it will give
you some value for WorkMem.

81
0:5:22,06 --> 0:5:23,74
Quite safe value.

82
0:5:25,36 --> 0:5:30,86
Again, the main thing is with WorkMem
to see how your max connections,

83
0:5:30,86 --> 0:5:33,64
because if you set, if you increase
max connections you need

84
0:5:33,64 --> 0:5:39,64
to understand how, like, we don't
want to have out of memory.

85
0:5:39,68 --> 0:5:45,12
This is the main thing to be careful
with.

86
0:5:45,58 --> 0:5:50,86
So, okay, it will give you some
rough value, and I think let's

87
0:5:50,86 --> 0:5:52,54
go with this value, that's it.

88
0:5:53,04 --> 0:5:59,54
Then we run it for some time in
production, and the second important

89
0:5:59,54 --> 0:6:1,82
step is to have very good monitoring.

90
0:6:2,8 --> 0:6:9,1
Monitoring can provide you some
very useful insights about temporary

91
0:6:9,1 --> 0:6:9,86
file creation.

92
0:6:10,12 --> 0:6:13,58
When work memory is not enough,
it doesn't mean Postgres cannot

93
0:6:13,58 --> 0:6:14,44
execute query.

94
0:6:14,44 --> 0:6:17,32
Postgres will execute your query,
but it will involve temporary

95
0:6:17,32 --> 0:6:21,68
file creation, meaning that it
will use disk to have more memory.

96
0:6:22,64 --> 0:6:25,8
And this is of course very slow,
it will slow down query execution

97
0:6:25,8 --> 0:6:33,12
a lot, but it will eventually finish
unless a statement amount

98
0:6:33,12 --> 0:6:34,18
is reached, right?

99
0:6:34,78 --> 0:6:40,58
So, okay, we applied this rough
tuning.

100
0:6:40,84 --> 0:6:44,36
We started monitoring workmem,
oh, by the way, how to monitor

101
0:6:45,18 --> 0:6:46,32
these temporary files.

102
0:6:47,46 --> 0:6:51,54
I see 2 sources of temporary file
creation.

103
0:6:52,3 --> 0:6:55,38
First is like very high level is
pgstart database.

104
0:6:55,4 --> 0:6:59,8
For each database you can see the
number of temporary files already

105
0:6:59,8 --> 0:7:4,02
created and the size of them, total
size of them, columns, temp

106
0:7:4,02 --> 0:7:5,78
files, and temp bytes.

107
0:7:6,48 --> 0:7:12,1
So if your monitoring is good,
or if you can extend it to have

108
0:7:12,1 --> 0:7:16,46
this, you will see the rates of
temp reflocation and also size,

109
0:7:16,46 --> 0:7:17,72
size also interesting.

110
0:7:18,34 --> 0:7:23,26
We can talk about average size
or maybe maximum size for each

111
0:7:23,26 --> 0:7:23,76
file.

112
0:7:25,64 --> 0:7:31,0
Well, we can probably play with
this data more, but it's only

113
0:7:31,0 --> 0:7:31,82
2 numbers, right?

114
0:7:31,82 --> 0:7:34,18
So number of files and number of
bytes.

115
0:7:34,54 --> 0:7:39,34
It's not a lot, we cannot have
p95, for example, here, right?

116
0:7:39,86 --> 0:7:43,84
So next, more detailed information
is from Postgres logs.

117
0:7:44,62 --> 0:7:50,64
If we adjust log temp files setting,
we can have details about

118
0:7:50,86 --> 0:7:54,52
every occurrence of temporary file
creation in the Postgres logs.

119
0:7:54,52 --> 0:7:57,94
Of course we need to be careful
with observer effect because

120
0:7:58,04 --> 0:8:2,18
if we set it for example to 0 and
for example our work memory

121
0:8:2,18 --> 0:8:6,72
is very small and a lot of queries
need to create temporary files.

122
0:8:7,44 --> 0:8:10,76
Not only temporary files will slow
us down, but also we will

123
0:8:10,76 --> 0:8:12,02
produce a lot of logging.

124
0:8:12,84 --> 0:8:15,38
Observer effect can be bad here.

125
0:8:15,38 --> 0:8:19,44
So probably we should be careful
and not set it immediately to

126
0:8:19,44 --> 0:8:23,0
0, but to some same value first,
and go down a little bit and

127
0:8:23,0 --> 0:8:23,5
see.

128
0:8:24,06 --> 0:8:28,62
But eventually, if we know that
temporary files are not created

129
0:8:28,62 --> 0:8:31,06
often, we can go even to 0.

130
0:8:31,1 --> 0:8:32,62
Again, we should be careful.

131
0:8:33,08 --> 0:8:37,86
And finally, the third source of
important monitoring data here

132
0:8:37,9 --> 0:8:39,06
is PGSAS statements.

133
0:8:40,24 --> 0:8:46,88
It has a couple of columns, temp
blocks, BLKS read and temp blocks

134
0:8:46,88 --> 0:8:47,38
written.

135
0:8:47,94 --> 0:8:52,42
So we can understand for each normalized
query, I call it query

136
0:8:52,42 --> 0:8:52,92
group.

137
0:8:53,44 --> 0:8:57,98
For each query group, we can see
again, like same as for database

138
0:8:57,98 --> 0:9:2,22
level, we can see a number of,
oh no, not the same.

139
0:9:2,54 --> 0:9:4,4
We don't have a number of files
here.

140
0:9:4,4 --> 0:9:8,84
Instead we have block read, read
and written.

141
0:9:8,84 --> 0:9:11,9
So written blocks is interesting
here.

142
0:9:13,18 --> 0:9:19,54
But the good idea here is that
we can identify the parts of our

143
0:9:19,54 --> 0:9:26,28
whole workload and understand which
queries are most active in

144
0:9:26,28 --> 0:9:28,04
terms of temporary file creation.

145
0:9:28,04 --> 0:9:33,38
That means they need more work
memory, right?

146
0:9:33,38 --> 0:9:36,56
They lack work memory.

147
0:9:37,5 --> 0:9:42,68
So once we build our monitoring,
or we already have it, maybe.

148
0:9:43,14 --> 0:9:45,18
I'm not sure everyone has very
good.

149
0:9:45,18 --> 0:9:48,34
As usual, I'm very skeptical in
terms of the current state of

150
0:9:48,34 --> 0:9:50,04
Postgres monitoring in general.

151
0:9:51,1 --> 0:9:56,04
But assuming we have this covered
in our monitoring tools, and

152
0:9:56,04 --> 0:10:0,7
we have some details probably in
logs, the next thing, of course,

153
0:10:0,94 --> 0:10:6,28
we can identify parts of our code
and we can think about optimization

154
0:10:6,54 --> 0:10:6,96
first.

155
0:10:6,96 --> 0:10:11,88
Instead of raising our workmem,
we can have an idea, let's try

156
0:10:11,88 --> 0:10:16,02
to reduce, let's be less hungry
for workmem, right?

157
0:10:16,08 --> 0:10:19,9
Let's reduce the memory usage.

158
0:10:21,26 --> 0:10:24,72
Sometimes it's quite straightforward,
sometimes it's tricky.

159
0:10:24,96 --> 0:10:29,44
Again, here I recommend using this
Pareto principle and not to

160
0:10:29,44 --> 0:10:32,66
spend too much effort on this optimization.

161
0:10:32,9 --> 0:10:36,82
We just try, if it takes too much
time, too much effort, we just

162
0:10:36,82 --> 0:10:38,2
proceed to the next step.

163
0:10:38,68 --> 0:10:40,68
Next step is raising workman.

164
0:10:41,78 --> 0:10:47,26
From these, like monitoring already
can suggest us what is average

165
0:10:47,48 --> 0:10:51,8
temporary file size and what is
maximum temporary file size.

166
0:10:52,04 --> 0:10:55,88
And from that information we can
understand how much we need

167
0:10:55,88 --> 0:10:56,54
to raise.

168
0:10:56,94 --> 0:11:3,26
Of course, instead of jumping straight
to this new value, it

169
0:11:3,26 --> 0:11:4,12
may be risky.

170
0:11:4,82 --> 0:11:6,26
Sometimes I see people do it.

171
0:11:6,26 --> 0:11:9,14
I mean, we know our max connections
value.

172
0:11:9,14 --> 0:11:13,68
We know that each statement can
consume multiple times up to

173
0:11:13,86 --> 0:11:17,58
work mem size because of operations,
this approach.

174
0:11:17,78 --> 0:11:23,08
Also, since Postgres 13, there
is a new setting, which is...

175
0:11:24,24 --> 0:11:26,96
I always forget this name, but
there is a setting that tells

176
0:11:26,96 --> 0:11:33,28
you multiplier for hash operations.

177
0:11:33,74 --> 0:11:37,36
And as I remember, by default it's
2, meaning that you have workmem,

178
0:11:37,36 --> 0:11:42,88
but hash operations can use up
to 2 workmem, which adds complexity

179
0:11:43,04 --> 0:11:44,46
in the logic and tuning.

180
0:11:45,78 --> 0:11:49,74
And again, it makes it even trickier
to tune.

181
0:11:51,82 --> 0:11:54,68
So, like on the safe side, if you
want to be on the safe side,

182
0:11:54,68 --> 0:11:57,9
you understand the available memory,
you understand your max

183
0:11:57,9 --> 0:12:2,54
connections, and you add some multiplier,
like 2, 3, maybe 4,

184
0:12:2,72 --> 0:12:7,46
but usually this will lead us to
very low work memory.

185
0:12:7,64 --> 0:12:12,54
So this is why this iterative approach
and maybe raising understanding

186
0:12:12,78 --> 0:12:16,34
that, like our workload won't change
tomorrow suddenly, like

187
0:12:16,34 --> 0:12:17,5
a whole, usually.

188
0:12:17,54 --> 0:12:21,24
In our existing project, usually
we understand, okay, realistic

189
0:12:21,6 --> 0:12:23,34
consumption of memory is this.

190
0:12:23,54 --> 0:12:24,58
So we are fine.

191
0:12:24,58 --> 0:12:26,9
We can start raising this work
memory.

192
0:12:27,84 --> 0:12:32,08
But like, and If you apply the
formula, you will see, oh, we

193
0:12:32,08 --> 0:12:33,48
have risks of out of memory.

194
0:12:33,48 --> 0:12:36,98
But no, no, we, our workload is,
we know our workload, right?

195
0:12:36,98 --> 0:12:40,96
Of course, if we release it, release
changes in application,

196
0:12:41,48 --> 0:12:44,2
often workload can change as well,
right?

197
0:12:44,2 --> 0:12:46,38
So we should be careful with it.

198
0:12:46,56 --> 0:12:49,84
Especially we should be careful
raising max connections after

199
0:12:50,14 --> 0:12:55,12
this tuning of workman because
this can lead us to higher out

200
0:12:55,12 --> 0:12:56,12
of memory risks.

201
0:12:57,04 --> 0:13:1,72
So instead of raising globally,
I recommend trying to think about

202
0:13:1,72 --> 0:13:2,66
raising locally.

203
0:13:2,72 --> 0:13:6,5
For example, you can say, I want
to raise for a specific session

204
0:13:6,5 --> 0:13:9,06
because I know this is like heavy
report.

205
0:13:9,06 --> 0:13:10,74
It needs more memory.

206
0:13:10,8 --> 0:13:12,74
I want to avoid temporary files.

207
0:13:12,74 --> 0:13:16,48
I just set workmem to higher value
in this session and that's

208
0:13:16,48 --> 0:13:16,96
it.

209
0:13:16,96 --> 0:13:22,48
Other sessions still use global
setting of workmem.

210
0:13:22,82 --> 0:13:27,32
We can set even, say, even set
local workmem in a transaction,

211
0:13:27,44 --> 0:13:31,58
so transaction finished, workmem
kind of reset in the same session.

212
0:13:31,96 --> 0:13:35,94
Or we can identify some parts of
workload and this is good practice

213
0:13:35,94 --> 0:13:42,18
to to split workload by users and
for example we have a special

214
0:13:42,18 --> 0:13:47,36
user that runs heavier queries
like analytical like queries maybe

215
0:13:48,18 --> 0:13:52,72
and we know this user needs higher
workmem, so we can alter users'

216
0:13:52,72 --> 0:13:53,22
workmem.

217
0:13:54,16 --> 0:13:57,76
And this is also good practice
to avoid global arrays.

218
0:13:58,08 --> 0:14:3,76
But of course This will make the
logic complex.

219
0:14:4,4 --> 0:14:5,94
We need to document it properly.

220
0:14:6,62 --> 0:14:12,1
So, if we have a bigger team and
we need to think, other people

221
0:14:12,1 --> 0:14:13,1
will deal with it.

222
0:14:13,1 --> 0:14:16,9
Of course, this needs proper documentation.

223
0:14:17,66 --> 0:14:21,06
Set doesn't have comment unlike
database objects.

224
0:14:21,28 --> 0:14:25,52
So maybe by the way, I just realized
maybe it's a good idea to

225
0:14:25,52 --> 0:14:30,2
have some commenting capabilities
in Postgres for configuration

226
0:14:30,82 --> 0:14:31,92
settings, right?

227
0:14:32,86 --> 0:14:37,7
So anyway, as a final step, of
course, we consider raising it

228
0:14:37,7 --> 0:14:38,2
globally.

229
0:14:38,24 --> 0:14:39,52
And we do it all the time.

230
0:14:39,52 --> 0:14:45,6
I mean, we see max connections
quite high, and we raise workmem,

231
0:14:45,88 --> 0:14:49,64
so even if you multiply maxConnections
by workmem, you see that

232
0:14:49,64 --> 0:14:54,52
you already exceed the kind of
available memory.

233
0:14:55,16 --> 0:15:0,36
But this is tricky, I mean, this
is risky of course, but If we

234
0:15:0,36 --> 0:15:4,4
observe our workload for a very
long time, and we know we don't

235
0:15:4,4 --> 0:15:9,56
change everything drastically,
we change only parts of the workload,

236
0:15:9,96 --> 0:15:11,36
sometimes it's okay.

237
0:15:11,68 --> 0:15:16,02
But of course we understand there
are risks here, right?

238
0:15:16,02 --> 0:15:20,64
So raising work memory is kind
of risky and should be done with

239
0:15:20,64 --> 0:15:22,7
understanding details I just described.

240
0:15:23,44 --> 0:15:26,68
Okay, I think maybe that's it.

241
0:15:26,82 --> 0:15:31,02
Oh, there is also, since Postgres
14, there is a function pgGetBackendMemoryContexts.

242
0:15:34,9 --> 0:15:36,0
It's very useful.

243
0:15:37,12 --> 0:15:40,76
I mean, I don't use it myself yet,
because it's quite new.

244
0:15:40,76 --> 0:15:43,0
Postgres 14 is only a couple of
years.

245
0:15:44,54 --> 0:15:49,14
But, and there's the drawback of
it.

246
0:15:49,74 --> 0:15:51,86
It can be applied only to current
session.

247
0:15:51,96 --> 0:15:55,58
So this is only for troubleshooting,
detailed troubleshooting.

248
0:15:55,76 --> 0:16:0,52
If you deal with some queries,
you can see what's happening with

249
0:16:0,52 --> 0:16:2,18
memory for a particular session.

250
0:16:3,36 --> 0:16:8,72
I saw discussions to extend this
function to be able to use it

251
0:16:9,52 --> 0:16:14,48
for other backends, for any session,
and when I was preparing

252
0:16:15,32 --> 0:16:22,0
my how-to, These days I use our
new AI bot and of course it hallucinated

253
0:16:22,36 --> 0:16:24,74
thinking Oh, you can just pass
PID to it.

254
0:16:24,74 --> 0:16:26,34
No, it doesn't have any parameters.

255
0:16:26,36 --> 0:16:29,36
You cannot pass anything to it
I would expect it.

256
0:16:29,44 --> 0:16:31,72
So I will probably hallucinate
as well.

257
0:16:31,72 --> 0:16:34,68
But the reality is it supports
only the current session, that's

258
0:16:34,68 --> 0:16:34,92
it.

259
0:16:34,92 --> 0:16:37,16
Maybe in the future it will be
extended.

260
0:16:37,88 --> 0:16:43,16
So that discussion, as I understand,
didn't lead to patches accepted

261
0:16:43,18 --> 0:16:43,68
yet.

262
0:16:44,1 --> 0:16:47,12
But anyway, This is additional,
like, extra.

263
0:16:48,18 --> 0:16:51,5
I think what I just described is
already quite practical.

264
0:16:52,24 --> 0:16:56,54
Just remember that any session
can use, any query can use multiple

265
0:16:57,1 --> 0:16:59,32
workmems, but usually it's not
so.

266
0:17:0,06 --> 0:17:5,68
And so the approach based on temporary
files is the way to go

267
0:17:5,68 --> 0:17:6,4
these days.

268
0:17:6,82 --> 0:17:8,7
Just monitor temporary files.

269
0:17:9,62 --> 0:17:14,24
It's not like a big deal if we
have few of them happening sometimes,

270
0:17:15,2 --> 0:17:17,38
especially for queries, analytical
queries.

271
0:17:17,52 --> 0:17:20,44
They anyway are slow probably.

272
0:17:21,0 --> 0:17:26,64
And okay, temporary files, we can
check how much we can win if

273
0:17:26,64 --> 0:17:27,6
we raise workmem.

274
0:17:28,86 --> 0:17:32,52
But anyway, for WALTP, of course,
you want to avoid temporary

275
0:17:32,52 --> 0:17:33,16
file creation.

276
0:17:33,16 --> 0:17:38,9
And by default, I forgot to mention,
work memory is just 4 megabytes.

277
0:17:38,94 --> 0:17:39,9
It's quite low.

278
0:17:40,08 --> 0:17:41,62
These days, it's quite low.

279
0:17:42,04 --> 0:17:47,76
I see in practice for mobile web
apps on bigger servers with

280
0:17:47,76 --> 0:17:51,5
hundreds of gigabytes, we usually
raise it to 100 megabytes,

281
0:17:52,16 --> 0:17:57,18
having few hundred max connections
and connection pooler, we

282
0:17:57,18 --> 0:18:0,54
usually tend to have like 100 megabytes
work memory.

283
0:18:1,26 --> 0:18:4,1
Maybe even more sometimes, again,
depends.

284
0:18:5,98 --> 0:18:7,28
I think that's it.

285
0:18:8,56 --> 0:18:13,4
So hello chat, I see several people
joined, thank you for joining.

286
0:18:13,94 --> 0:18:17,48
Honestly, I recorded live just
because this is more convenient

287
0:18:17,48 --> 0:18:18,26
for me.

288
0:18:18,52 --> 0:18:23,5
So this is podcast anyway, this
will be distributed as usual.

289
0:18:24,76 --> 0:18:32,56
I want to again say thank you for
being a listener, happy holidays,

290
0:18:33,54 --> 0:18:40,18
and I hope we will have very good
topics in new year and I hope

291
0:18:40,38 --> 0:18:45,16
every Postgres production server
is up and running with very

292
0:18:45,16 --> 0:18:53,22
good uptime and with as few failovers
as possible and with as

293
0:18:53,72 --> 0:18:56,6
low temporary file numbers as possible
as well.

294
0:18:57,18 --> 0:19:2,78
So this is my wish for you in a
new year and thank you for listening

295
0:19:2,78 --> 0:19:3,46
and watching.
