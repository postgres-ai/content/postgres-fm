1
00:00:00,06 --> 00:00:04,3999996
Nikolay: Hello, hello, this is
PostgresFM, a podcast about Postgres.

2
00:00:04,66 --> 00:00:06,0
Hi, Michael.

3
00:00:06,2599998 --> 00:00:08,04
Sorry for Mike last time.

4
00:00:08,04 --> 00:00:08,76
Michael: That's all right, Nik.

5
00:00:08,76 --> 00:00:10,26
I don't even notice normally.

6
00:00:10,599999 --> 00:00:11,099999
Nikolay: Right.

7
00:00:11,12 --> 00:00:11,84
Nik is fine.

8
00:00:11,84 --> 00:00:12,74
Nik is fine.

9
00:00:12,94 --> 00:00:15,5199995
So, episode number almost 90.

10
00:00:15,5199995 --> 00:00:16,74
I don't remember exactly.

11
00:00:16,88 --> 00:00:17,64
Like, Do you remember?

12
00:00:17,64 --> 00:00:18,619999
You always remember.

13
00:00:19,14 --> 00:00:21,66
Michael: I remember, but you told
me not to remind you anymore

14
00:00:21,66 --> 00:00:23,16
until we get to Milestone.

15
00:00:23,439999 --> 00:00:26,08
And you're the only one that ever
brings up the episode number.

16
00:00:26,84 --> 00:00:27,34
Nikolay: Really?

17
00:00:27,98 --> 00:00:29,34
Michael: I never mention it.

18
00:00:30,52 --> 00:00:31,02
Nikolay: Okay.

19
00:00:31,28 --> 00:00:31,78
Interesting.

20
00:00:32,9 --> 00:00:35,64
So we are going to talk about what?

21
00:00:36,22 --> 00:00:39,62
Michael: Oh yes, so this week was
your suggestion, and it's a

22
00:00:39,62 --> 00:00:40,34
cool 1.

23
00:00:40,9 --> 00:00:44,18
It's going to be a new feature
in Postgres 17, hopefully.

24
00:00:44,34 --> 00:00:46,86
It's committed, but you never know
for sure.

25
00:00:46,92 --> 00:00:50,5
And it's transaction timeout, a
new parameter that we'll have

26
00:00:50,5 --> 00:00:52,08
hopefully in the next version.

27
00:00:53,239998 --> 00:00:55,52
Nikolay: I can show off, it was
my idea.

28
00:00:55,760002 --> 00:01:01,989998
But most of the work, Andrey did,
Andrey Borodin did.

29
00:01:01,989998 --> 00:01:05,7
So it started during our PostgresTV
hacking Postgres session.

30
00:01:06,88 --> 00:01:08,3
Michael: Which are back now, right?

31
00:01:08,4 --> 00:01:09,38
After a long pause.

32
00:01:09,38 --> 00:01:10,68
Nikolay: Well, yes, we'll try.

33
00:01:10,68 --> 00:01:11,54
Nobody knows.

34
00:01:11,96 --> 00:01:14,82
It's hard to arrange usually because
to do proper hacking, you

35
00:01:14,82 --> 00:01:15,8
need a lot of time.

36
00:01:16,5 --> 00:01:20,58
Last time we did restart it, we
talked about this transaction

37
00:01:20,64 --> 00:01:26,88
timeout and Andrei tried to write
tests because former tests

38
00:01:27,7 --> 00:01:28,5
needed to be removed.

39
00:01:28,5 --> 00:01:33,56
By the way, I also wanted to say
thanks to Alexander Korotkov

40
00:01:33,64 --> 00:01:35,04
who committed this patch.

41
00:01:35,74 --> 00:01:37,44
Honestly, I even didn't ask.

42
00:01:37,56 --> 00:01:40,14
So it was good to see that it's
committed.

43
00:01:40,44 --> 00:01:46,56
And so session finished after 90
minutes by timeout because we

44
00:01:46,56 --> 00:01:47,38
needed to go.

45
00:01:47,72 --> 00:01:49,4
Session about timeout finished.

46
00:01:49,4 --> 00:01:50,88
Okay, now you're smiling.

47
00:01:51,5 --> 00:01:52,84
So, transaction timeout.

48
00:01:53,479996 --> 00:01:58,44
It took a few years for me to understand
something is wrong.

49
00:01:58,78 --> 00:01:59,94
We don't have something.

50
00:02:01,3 --> 00:02:06,22
Then, last year, like a year ago,
I started thinking, it should

51
0:2:6,22 --> 0:2:6,72
be.

52
0:2:8,14 --> 0:2:12,16
My idea was maybe I'm very wrong
or I'm very right.

53
0:2:12,16 --> 0:2:17,7
So it's so strange in 2023 to think
about this basic setting.

54
0:2:18,58 --> 0:2:23,14
But honestly, the last few years,
I already suggested to everyone

55
0:2:23,14 --> 0:2:26,08
to consider the transaction timeout
to be applied on application level.

56
0:2:26,92 --> 0:2:32,52
In 100% of cases, I saw a very
strange look, if it was a video

57
0:2:32,52 --> 0:2:33,02
meeting.

58
0:2:33,14 --> 0:2:36,34
It was a very strange look, like
Postgres cannot do it?

59
0:2:36,34 --> 0:2:40,32
Yes, Postgres cannot do transaction
timeout, but you do need

60
0:2:40,32 --> 0:2:42,02
it and we can discuss why.

61
0:2:42,72 --> 0:2:46,62
So I always say, do it always on
the application side if you

62
0:2:46,62 --> 0:2:52,26
have LTP, because, you know, like
anyway, HTTP has timeout.

63
0:2:52,36 --> 0:2:56,34
A server, for example, NGINX, like
30 seconds, it's normal, right?

64
0:2:56,88 --> 0:3:0,56
Nobody will wait many minutes,
unless it's a specific request.

65
0:3:1,34 --> 0:3:5,88
Sometimes we can wait a few minutes,
but usually we have a limit.

66
0:3:5,92 --> 0:3:7,84
And Postgres doesn't have this
transaction timeout.

67
0:3:7,84 --> 0:3:11,34
It has only statement timeout and
idle transaction session timeout.

68
0:3:11,98 --> 0:3:13,52
But no transaction timeout.

69
0:3:14,54 --> 0:3:18,58
Michael: And also, so I looked
into when we got each of these,

70
0:3:18,58 --> 0:3:23,6
and statement timeout was ages
ago, like 7.3, would you believe?

71
0:3:24,02 --> 0:3:28,04
And then idle in transaction session
timeout was actually more

72
0:3:28,04 --> 0:3:29,06
recent than I realized---

73
0:3:29,06 --> 0:3:32,86
It's 9.6, which is probably only
about 7 or 8 years ago.

74
0:3:33,22 --> 0:3:38,44
And then we also got more recently
idle session timeout.

75
0:3:39,1 --> 0:3:40,02
Nikolay: Which is different.

76
0:3:40,92 --> 0:3:42,74
It's outside of normal work.

77
0:3:42,88 --> 0:3:46,06
It's for like, let's drop the connection
if it doesn't do anything.

78
0:3:46,68 --> 0:3:50,5
Michael: Yeah, but that was a recent,
like, version 14 edition.

79
0:3:50,5 --> 0:3:52,44
So still only a couple of years
ago.

80
0:3:52,44 --> 0:3:55,58
Nikolay: Ah, you want to say that
it's also a very, very basic

81
0:3:55,58 --> 0:3:57,54
thing and only added recently?

82
0:3:57,54 --> 0:3:58,68
Yeah, I agree with that.

83
0:3:58,68 --> 0:4:3,4
Usually people implemented it outside
Postgres as well, to drop

84
0:4:3,4 --> 0:4:3,9
connections.

85
0:4:5,02 --> 0:4:8,76
Michael: I've seen less need for
that one than I have seen for

86
0:4:8,76 --> 0:4:12,08
this one, but like you, I didn't,
it's one of those things that I

87
0:4:12,08 --> 0:4:15,44
kind of didn't realize I needed
because statement timeout can

88
0:4:15,44 --> 0:4:19,64
cover a lot of the base, like a
lot of the use cases can be just

89
0:4:19,64 --> 0:4:23,82
about coped with via statement
timeout, but one or two really can't.

90
0:4:23,84 --> 0:4:28,26
And I think I saw your post and
I'll link it up the mailing list

91
0:4:28,26 --> 0:4:32,74
thread that I think Andre started,
but your first reply to that

92
0:4:32,74 --> 0:4:36,54
included some really good examples
of when you do need this in

93
0:4:36,54 --> 0:4:40,22
addition to statement timeout or
like instead of statement timeout.

94
0:4:40,46 --> 0:4:42,62
So is it worth like covering a
couple of those?

95
0:4:42,62 --> 0:4:43,12
Yeah.

96
0:4:43,26 --> 0:4:47,86
Nikolay: Yeah, let's discuss why
we use statement timeout in

97
0:4:47,86 --> 0:4:51,76
OLTP like in regular the most common
cases like web and mobile

98
0:4:51,76 --> 0:4:52,26
apps.

99
0:4:52,38 --> 0:4:55,46
We had an episode about that and
I think it was the very first

100
0:4:55,46 --> 0:4:56,4
episode maybe.

101
0:4:57,98 --> 0:4:59,64
Speaking of counting.

102
0:5:0,3 --> 0:5:2,84
Michael: Yeah, we've done a few
other episodes related to this

103
0:5:2,84 --> 0:5:3,16
as well.

104
0:5:3,16 --> 0:5:5,84
We did one on connections, one on
zero downtime migrations.

105
0:5:5,84 --> 0:5:8,04
Nikolay: No, I'm talking about
the very first episode.

106
0:5:8,04 --> 0:5:8,94
What was it?

107
0:5:9,38 --> 0:5:10,46
Michael: Yeah, slow queries.

108
0:5:10,96 --> 0:5:12,16
Nikolay: Slow queries, yes.

109
0:5:12,16 --> 0:5:14,7
And this is about statement amount,
basically.

110
0:5:15,3 --> 0:5:18,54
But people don't do SQL queries.

111
0:5:18,54 --> 0:5:24,06
I mean, end users, web and mobile
app users, they don't do SQL

112
0:5:24,06 --> 0:5:24,56
queries.

113
0:5:26,0 --> 0:5:28,76
So they work at a higher level, HTTP
requests.

114
0:5:29,76 --> 0:5:33,34
So why do we need statement timeout
at all?

115
0:5:33,58 --> 0:5:34,08
Why?

116
0:5:34,74 --> 0:5:37,62
What is the problem we are solving?

117
0:5:39,72 --> 0:5:43,72
I have this honest question because
in my opinion, when we think

118
0:5:44,34 --> 0:5:47,48
about what we try to solve with
statement timeout, it's not the

119
0:5:47,48 --> 0:5:48,9
right tool usually.

120
0:5:49,7 --> 0:5:56,54
For example, again, users work
at a high level and we know some

121
0:5:56,54 --> 0:6:0,58
intermediate software settings,
middleware, which is usually

122
0:6:0,58 --> 0:6:3,58
written in some Python, Java, Ruby,
anything.

123
0:6:4,06 --> 0:6:7,48
These application servers and also
an HTTP server, like maybe front

124
0:6:7,48 --> 0:6:11,54
end server, like an NGINX or something,
and VoIP, right?

125
0:6:11,68 --> 0:6:17,22
They usually have some limits to
drop connections which last

126
0:6:17,22 --> 0:6:21,96
too long, if a server is taking too
long to respond, or communication

127
0:6:22,2 --> 0:6:24,98
taking too long, usually like 30
or 60 seconds.

128
0:6:25,92 --> 0:6:30,36
This is what users will get if
something which should take 100

129
0:6:30,36 --> 0:6:34,72
milliseconds or less, takes 30
seconds.

130
0:6:36,04 --> 0:6:39,36
It's time to admit there is a problem
and tell the user that,

131
0:6:40,08 --> 0:6:45,52
like, for 502 gateway timeout,
it's usually what NGINX was returning.

132
0:6:45,74 --> 0:6:48,94
And then we, back to database,
we think, OK, we need to limit

133
0:6:48,94 --> 0:6:49,62
as well.

134
0:6:49,9 --> 0:6:51,76
But what exactly do we need to limit?

135
0:6:52,0 --> 0:6:53,5
This is the question.

136
0:6:54,14 --> 0:6:56,34
Is the statement timeout the right
tool for that?

137
0:6:56,98 --> 0:7:0,14
Michael: I do think there's a like,
a lot of cases where it's

138
0:7:0,14 --> 0:7:0,64
enough.

139
0:7:1,0 --> 0:7:5,76
So if, for example, we're trying
to, in that case, let's say,

140
0:7:5,76 --> 0:7:9,48
for example, the users or like
the app has already given up,

141
0:7:9,48 --> 0:7:11,72
but the database carries on working.

142
0:7:11,72 --> 0:7:15,06
Yeah, that's doing pointless kind
of, that's pointless work.

143
0:7:15,06 --> 0:7:17,5
So avoiding pointless work would
be great.

144
0:7:17,52 --> 0:7:20,5
But there are other cases too, where
even if the app didn't have

145
0:7:20,5 --> 0:7:25,24
a timeout, is it okay for a few
users of a system to be using

146
0:7:25,24 --> 0:7:30,04
all the resources when, you know,
like hogging those resources 

147
0:7:30,48 --> 0:7:33,74
by running some really, really
slow or really, really heavy queries.

148
0:7:34,12 --> 0:7:37,08
Is that an acceptable trade-off
or do you want to limit that?

149
0:7:37,08 --> 0:7:40,08
So I think there are other slightly
different cases.

150
0:7:40,08 --> 0:7:40,88
Right, but

151
0:7:41,78 --> 0:7:42,88
Nikolay: Work is not...

152
0:7:43,28 --> 0:7:44,88
Statements are too small.

153
0:7:45,14 --> 0:7:47,2
It's too atomic a piece, right?

154
0:7:47,78 --> 0:7:49,12
Michael: Well, I think there's...

155
0:7:49,18 --> 0:7:53,1
Before we move on to why transaction
timeout, there's also one

156
0:7:53,1 --> 0:7:57,94
other case which I think or maybe
maybe this makes the point

157
0:7:57,94 --> 0:8:2,36
perfectly. These long-running transactions,
even if they're a single

158
0:8:2,36 --> 0:8:7,74
statement transaction, they can
block internal processes like

159
0:8:7,74 --> 0:8:11,72
autovacuum and cause issues that we've,
I mean, we've spent multiple

160
0:8:11,72 --> 0:8:14,86
episodes talking about the kind
of issues that long-running statements

161
0:8:14,86 --> 0:8:18,8
could cause, never mind long-running
multi-statement transactions.

162
0:8:18,8 --> 0:8:20,94
Nikolay: No, no, no, no, not statements
block it.

163
0:8:21,14 --> 0:8:21,64
Transactions.

164
0:8:22,66 --> 0:8:25,68
Michael: But as you've said multiple
times, you can't have a

165
0:8:25,68 --> 0:8:27,02
statement without a transaction.

166
0:8:27,12 --> 0:8:31,36
So like a long-running statement
is enough, but you're right

167
0:8:31,36 --> 0:8:31,86
that...

168
0:8:31,98 --> 0:8:35,02
Nikolay: You can have a statement
without transactions for example

169
0:8:35,02 --> 0:8:39,96
creating an index concurrently. Three transactions
I mean, you're okay

170
0:8:39,96 --> 0:8:41,12
without a transaction completely.

171
0:8:41,12 --> 0:8:42,18
No, no dirty reads.

172
0:8:42,18 --> 0:8:43,76
Michael: Exactly right,

173
0:8:43,82 --> 0:8:44,78
Nikolay: Right, right.

174
0:8:45,02 --> 0:8:51,86
So yeah, but like we want
to apply the right tool.

175
0:8:51,86 --> 0:8:57,68
It means that it should suit for
all edge and corner cases.

176
0:8:58,84 --> 0:9:3,76
And I'm saying statement timeout
is for a different hole to close.

177
0:9:5,14 --> 0:9:8,16
Michael: You gave a really great
example that completely sold

178
0:9:8,16 --> 0:9:10,08
me in that email thread I mentioned.

179
0:9:10,08 --> 0:9:10,58
Nikolay: Email?

180
0:9:10,94 --> 0:9:11,44
Michael: Yes.

181
0:9:11,82 --> 0:9:15,28
And that was a case I've seen actually
quite recently, which

182
0:9:15,28 --> 0:9:15,78
was...

183
0:9:16,02 --> 0:9:16,52
...taratatata.

184
0:9:16,96 --> 0:9:18,06
Nikolay: Yeah, I call it...

185
0:9:18,48 --> 0:9:20,52
It's like from a machine gun.

186
0:9:20,9 --> 0:9:22,92
Yeah, but - statements coming from
a machine gun.

187
0:9:22,92 --> 0:9:24,66
Brief statements with brief pause.

188
0:9:25,98 --> 0:9:28,78
Michael: And somebody setting a
transaction at the beginning,

189
0:9:29,44 --> 0:9:33,64
so in the case I saw, it was deliberately
taking out, like doing

190
0:9:33,64 --> 0:9:38,64
begin, then doing multiple updates
or like upsets in this case,

191
0:9:38,9 --> 0:9:41,18
and then only at the end committing
it.

192
0:9:41,38 --> 0:9:45,22
So any one of those statements was
really short, but because they

193
0:9:45,22 --> 0:9:47,56
were doing tens of thousands or
even hundreds of thousands, I

194
0:9:47,56 --> 0:9:51,9
think, in this case, the transaction
as a whole was longer than

195
0:9:51,9 --> 0:9:54,64
you'd probably want on a OLTP
system.

196
0:9:54,82 --> 0:9:57,8
So in that case, statement timeout
wouldn't have helped, or at

197
0:9:57,8 --> 0:10:1,6
least any sane statement timeout
wouldn't have cancelled that.

198
0:10:1,78 --> 0:10:3,24
But a transaction timeout...

199
0:10:3,8 --> 0:10:7,62
Nikolay: An idle transaction session
timeout wouldn't cancel

200
0:10:7,74 --> 0:10:10,58
this transaction because the breaks
are also short.

201
0:10:11,28 --> 0:10:13,72
Michael: Yeah, it's not idle, exactly.

202
0:10:14,34 --> 0:10:15,78
Nikolay: It's like from a machine
gun.

203
0:10:15,78 --> 0:10:19,16
It can be select plus 1, by the
way, if it's wrapped in a transaction

204
0:10:19,2 --> 0:10:19,54
block.

205
0:10:19,54 --> 0:10:23,32
Select plus 1, this anti-pattern,
which...

206
0:10:24,86 --> 0:10:26,1
Michael: Like n plus 1.

207
0:10:27,34 --> 0:10:28,0
Nikolay: 0, sorry.

208
0:10:28,08 --> 0:10:28,94
Yes, yes.

209
0:10:28,94 --> 0:10:29,64
Yeah, yeah.

210
0:10:30,06 --> 0:10:35,64
I mean, it's actually a terrible
name, but the idea is that it's

211
0:10:36,04 --> 0:10:38,6
n selects in a loop.

212
0:10:39,06 --> 0:10:41,76
So, we didn't notice that they
have a loop.

213
0:10:42,5 --> 0:10:47,8
It can be short updates with the
idea that we want to make them

214
0:10:47,8 --> 0:10:50,14
short because it's better, you
know, like to...

215
0:10:50,24 --> 0:10:55,12
But if it's wrapped into a transaction,
you cannot release any

216
0:10:55,12 --> 0:10:56,88
locks until the very end of the
transaction.

217
0:10:56,88 --> 0:10:57,78
This is the rule.

218
0:10:58,32 --> 0:11:2,06
All locks are released only if it's
a commit or rollback.

219
0:11:2,78 --> 0:11:3,48
That's it.

220
0:11:5,28 --> 0:11:11,98
And so it's a bad idea to split
into batches, updates or deletes.

221
0:11:12,88 --> 0:11:16,04
Insert is almost never, I think,
an issue.

222
0:11:16,12 --> 0:11:18,26
It makes sense to split.

223
0:11:18,26 --> 0:11:21,76
An insert should be a single massive
insert, usually.

224
0:11:22,36 --> 0:11:23,62
Like copy or insert.

225
0:11:24,18 --> 0:11:27,92
But if it's updates and deletes,
people know there's a rule to

226
0:11:27,92 --> 0:11:32,54
split, then they put it in a single
transaction and what's happening

227
0:11:32,54 --> 0:11:33,04
here.

228
0:11:33,7 --> 0:11:35,04
What do we do?

229
0:11:35,28 --> 0:11:38,58
We're accumulating a lot of logs
and not releasing them.

230
0:11:40,96 --> 0:11:44,2
So there are only 2 reasons, the
big reasons I see.

231
0:11:44,34 --> 0:11:47,18
Well, the source of the transition
is 1 reason, but there are 2 problems

232
0:11:47,26 --> 0:11:48,58
which are very bright.

233
0:11:50,86 --> 0:11:53,04
They scream let's have transaction
timeout.

234
0:11:53,1 --> 0:11:56,32
They did it for many years and
I always say, okay, this is a

235
0:11:56,32 --> 0:11:58,52
problem we must solve on the application
side.

236
0:11:58,52 --> 0:12:1,32
We cannot do it in PostgreSQL at
all.

237
0:12:1,72 --> 0:12:2,22
Surprise.

238
0:12:2,5 --> 0:12:6,76
Well, we can use pg-cron and put
maybe some, I call it terminator

239
0:12:7,08 --> 0:12:11,28
snippet, working with pg_stat_activity,
checking the exact start, like

240
0:12:11,28 --> 0:12:14,68
transaction start timestamp, and
then terminating.

241
0:12:15,48 --> 0:12:16,76
So terminator script.

242
0:12:16,78 --> 0:12:18,36
This name is derived from pg_terminate_backend.

243
0:12:20,38 --> 0:12:23,94
So was it renamed or no?

244
0:12:23,94 --> 0:12:24,82
I don't remember.

245
0:12:24,96 --> 0:12:26,92
pg_start_backup was
renamed.

246
0:12:27,66 --> 0:12:29,84
It's about naming and I support
it.

247
0:12:30,04 --> 0:12:33,98
Well, I'm having 3 in my speech,
sorry.

248
0:12:35,2 --> 0:12:38,3
So, pg_start_backup was renamed to
pg_basebackup start.

249
0:12:38,3 --> 0:12:41,34
So the same should happen with
pg_terminate_backend, I think.

250
0:12:42,74 --> 0:12:44,78
Now, pg_terminate_backend should be
pg_backend_terminate.

251
0:12:45,02 --> 0:12:47,86
So, this terminate script is also
like a workaround.

252
0:12:47,9 --> 0:12:50,82
It should be some simple solution.

253
0:12:51,18 --> 0:12:55,4
Let's not allow too long-running
transactions.

254
0:12:56,28 --> 0:12:59,2
Because we keep locks, this is
problem number 1 we just discussed.

255
0:12:59,2 --> 0:13:4,54
And second problem you mentioned,
keeping xmin horizon frozen

256
0:13:4,62 --> 0:13:5,98
basically, not shifting.

257
0:13:6,38 --> 0:13:12,74
Which means that all, like xmin
horizon is the hidden xmin value

258
0:13:12,74 --> 0:13:18,22
of a tuple which is the oldest
in the system and we usually cannot...

259
0:13:19,94 --> 0:13:23,18
Autovacuum usually comes and deletes
it at that time, we delete

260
0:13:23,18 --> 0:13:26,98
it but we cannot delete it if there
is some transaction which

261
0:13:26,98 --> 0:13:33,48
still can read from this because
xmin is like current time not

262
0:13:33,48 --> 0:13:38,24
past, not the past for this transaction
which means that if we

263
0:13:38,24 --> 0:13:43,38
keep our transaction we block this
xmin frozen and that means

264
0:13:43,38 --> 0:13:47,74
that Autovacuum cannot delete dead
tuples, more and more of them,

265
0:13:47,78 --> 0:13:49,94
freshly dead tuples, I call them.

266
0:13:50,5 --> 0:13:52,94
Or maybe it can be improved this
name.

267
0:13:53,32 --> 0:13:57,78
So these 2 problems, locks and
xmin horizon frozen.

268
0:13:58,36 --> 0:14:5,86
Locks are not released and they
both can hit and a lot of people

269
0:14:5,86 --> 0:14:9,88
can notice and you can even be
down down server can be down in

270
0:14:9,88 --> 0:14:16,16
some cases and you cannot limit
it inclusion in Postgres 16 I

271
0:14:16,16 --> 0:14:18,94
was so happy to realize I'm not
mad.

272
0:14:21,82 --> 0:14:26,26
It's obvious and I think it was
discussed maybe, so shouldn't

273
0:14:26,26 --> 0:14:30,38
discuss many times, but we just
went ahead, many things to Andrej,

274
0:14:30,38 --> 0:14:32,18
just went ahead and coded it.

275
0:14:32,64 --> 0:14:36,56
Then many strange cases started
to appear.

276
0:14:36,6 --> 0:14:41,66
For example, sub-transactions or
conflict with statement_timeout,

277
0:14:42,1 --> 0:14:44,62
which should fire first, for example.

278
0:14:46,32 --> 0:14:47,56
Michael: That's a good point.

279
0:14:47,62 --> 0:14:51,88
Do you want to talk a tour about
in this future, like once Postgres

280
0:14:51,9 --> 0:14:57,7
17 is out, if you're designing
a new OLTP system, are you even

281
0:14:57,7 --> 0:15:0,92
using, like, do you normally use
global statement timeouts?

282
0:15:0,92 --> 0:15:4,48
And if you did in the past would
you now replace that or would

283
0:15:4,48 --> 0:15:7,04
you use both with different settings
how would you

284
0:15:7,04 --> 0:15:7,7
Nikolay: do it?

285
0:15:7,9 --> 0:15:9,22
Good question, good question.

286
0:15:9,52 --> 0:15:12,98
So first of all in documentation
we can read that setting statement

287
0:15:12,98 --> 0:15:17,3
timeout globally is not a good
idea and I think it's a very bad

288
0:15:17,3 --> 0:15:22,28
idea to write this in documentation
because we discussed, even

289
0:15:22,28 --> 0:15:25,12
in HTTP server, application server,
they have their own timeouts

290
0:15:25,44 --> 0:15:29,02
database is usually the most like...

291
0:15:29,48 --> 0:15:33,08
Data intensive work is usually
in database unless we talk about

292
0:15:33,08 --> 0:15:36,9
some other stuff happening now
outside of web and mobile apps,

293
0:15:36,9 --> 0:15:37,4
right?

294
0:15:37,54 --> 0:15:40,96
I mean AI stuff like in GPU and
so on.

295
0:15:40,96 --> 0:15:46,96
So we want to limit in all OLTP systems,
we definitely want to limit

296
0:15:46,96 --> 0:15:49,8
our work to 30 seconds maybe.

297
0:15:50,5 --> 0:15:51,56
It's a good limit.

298
0:15:52,42 --> 0:15:56,98
This article and our very first
episode is about basics, which

299
0:15:57,98 --> 0:16:2,08
are so like 30 years, like 40 years
since the beginning of the

300
0:16:2,08 --> 0:16:2,58
Internet.

301
0:16:03.1 --> 0:16:07.58
People don't like to wait more
than a second, in a normal case.

302
0:16:08.22 --> 0:16:12.68
It means that we don't want to
limit globally and be protected.

303
0:16:12.78 --> 0:16:18.4
Only specific users which need
it, always can change it, unless

304
0:16:18.42 --> 0:16:19.24
it's restricted.

305
0:16:19.74 --> 0:16:23.68
So you just set statement timeout
or anything and go.

306
0:16:23.68 --> 0:16:27.18
Of course, if DDL is needed to
be executed, for example, create

307
0:16:27.18 --> 0:16:30.84
index concurrently, of course,
you also should remove this limit.

308
0:16:30.84 --> 0:16:35.58
But global setting is protection,
it should be there for OLTP

309
0:16:35.6 --> 0:16:35.9
case.

310
0:16:35.9 --> 0:16:39.14
And Postgres is like, OLTP is the
main case.

311
0:16:39.14 --> 0:16:43.18
We don't usually think about analytical
queries.

312
0:16:44.16 --> 0:16:48.28
So it means that I always recommend
setting this and documentation

313
0:16:48.74 --> 0:16:52.22
is having bad advice in this case.

314
0:16:53.54 --> 0:16:55.06
So 30 seconds, my recommendation.

315
0:16:55.44 --> 0:16:57.74
Sometimes 15, it's better.

316
0:16:58.14 --> 0:16:58.88
Michael: Oh, wow.

317
0:16:59.1 --> 0:17:01.36
Nikolay: Yeah, well, even 10 maybe.

318
0:17:01.52 --> 0:17:02.14
It depends.

319
0:17:03.94 --> 0:17:07.8
It raises the bar requirement for
queries that this is about

320
0:17:07.8 --> 0:17:08.74
quality actually.

321
0:17:08.74 --> 0:17:13.46
So if your query is poorly designed
or you forgot to create a proper

322
0:17:13.46 --> 0:17:17.42
index something like a bad plan,
be killed.

323
0:17:17.7 --> 0:17:20.88
But now with transaction timeout,
I would say statement timeout

324
0:17:20.88 --> 0:17:22.32
might be not needed at all.

325
0:17:23.26 --> 0:17:23.76
Michael: Interesting.

326
0:17:23.76 --> 0:17:25.68
I wondered if you were going to
say that.

327
0:17:26.14 --> 0:17:29.4
Nikolay: So again, my logic is
applied at a higher level.

328
0:17:30.94 --> 0:17:32.32
This is about each feature.

329
0:17:32.32 --> 0:17:35.86
If we go down, in both cases, database
specific, Postgres specific

330
0:17:35.86 --> 0:17:38.94
cases I mentioned, they also apply
to a transaction level at

331
0:17:38.94 --> 0:17:39.66
higher levels.

332
0:17:39.96 --> 0:17:43.58
We don't care about statement and
breaks between statements.

333
0:17:44.34 --> 0:17:46.76
Michael: And do you think like
same order of magnitude?

334
0:17:46.88 --> 0:17:50.14
Like you mentioned 30, I've seen
60 seconds quite often mentioned

335
0:17:50.14 --> 0:17:53.08
as a sensible default, so 30 seconds
not too different.

336
0:17:53.54 --> 0:17:57.44
Would you go similar, like for
any reason to double it or like

337
0:17:57.44 --> 0:18:01.46
increase it, like add a little
bit of buffer there or just similar

338
0:18:01.46 --> 0:18:01.62
number?

339
0:18:01.62 --> 0:18:03.04
Nikolay: No, no, no, Why buffer?

340
0:18:03.18 --> 0:18:07.34
Set it to 30, solve all problems
and then consider going even

341
0:18:07.34 --> 0:18:07.84
down.

342
0:18:08.04 --> 0:18:10.44
And if we talk about statement
timeout, why at all?

343
0:18:10.44 --> 0:18:13.94
Like we need it, like, okay, we
put it, but what if we have transaction

344
0:18:13.94 --> 0:18:17.28
timeout 30 seconds, for example,
which statement timeout would

345
0:18:17.28 --> 0:18:17.7
I use?

346
0:18:17.7 --> 0:18:19.26
30 seconds maybe as well.

347
0:18:19.36 --> 0:18:21.04
But it's already solved.

348
0:18:21.34 --> 0:18:23.26
Michael: I don't think there's
any, that wouldn't make sense

349
0:18:23.26 --> 0:18:23.6
to me.

350
0:18:23.6 --> 0:18:26.38
But I was wondering if you might
say you know statement timeout

351
0:18:26,38 --> 0:18:29,7
30 seconds transaction timeout,
60 seconds and

352
0:18:29,96 --> 0:18:32,86
Nikolay: Transaction? I don't... transaction
session timeout, 60 seconds.

353
0:18:32,86 --> 0:18:36,24
But when transaction timeout? What?

354
0:18:36,26 --> 0:18:37,7
Session timeout, you mean?

355
0:18:38,3 --> 0:18:41,84
Michael: No, no, no, transaction.
So, transactions can consist

356
0:18:41,84 --> 0:18:43,42
of multiple statements, right?

357
0:18:43,62 --> 0:18:50,24
Right. So, there's no need to have
a statement timeout that is

358
0:18:50,6 --> 0:18:52,66
bigger or equal to the transaction
timeout.

359
0:18:52,66 --> 0:18:53,74
Nikolay: It doesn't make sense.

360
0:18:54,12 --> 0:18:57,34
Michael: So the only thing that
could make sense would be a statement

361
0:18:57,34 --> 0:19:0,04
timeout that's less than the transaction
timeout.

362
0:19:0,82 --> 0:19:1,32
Nikolay: Right.

363
0:19:1,32 --> 0:19:2,52
Michael: But it seems pretty neat.

364
0:19:2,52 --> 0:19:3,02
Yeah.

365
0:19:3,82 --> 0:19:7,26
Nikolay: But again, like if you
take transaction timeout, which

366
0:19:7,26 --> 0:19:12,34
is already quite good, restrictive,
30 or 60 seconds, then I

367
0:19:12,34 --> 0:19:13,4
think statement amount...

368
0:19:13,62 --> 0:19:16,06
And I don't see logical to go down.

369
0:19:17,3 --> 0:19:19,08
Okay, it's about resources.

370
0:19:19,12 --> 0:19:24,02
If you go down, maybe you save
some resources by killing some statement

371
0:19:24,06 --> 0:19:25,94
earlier than the transaction.

372
0:19:27,74 --> 0:19:30,54
But it's not the main reason to
have transaction command for

373
0:19:30,54 --> 0:19:31,04
me.

374
0:19:31,34 --> 0:19:37,1
Well, of course. Maybe it's actually
super important.

375
0:19:38,1 --> 0:19:41,14
Maybe I just forgot that we use
it.

376
0:19:41,26 --> 0:19:42,58
Okay, I don't know.

377
0:19:43,66 --> 0:19:48,92
I saw so many incidents where even
statement timeout was not achieved,

378
0:19:49,02 --> 0:19:51,16
but resources were spent fully.

379
0:19:51,62 --> 0:19:52,64
So I don't know.

380
0:19:52,64 --> 0:19:55,64
I mean, we have very restrictive
statement timeout, very, very

381
0:19:55,64 --> 0:19:56,14
low.

382
0:19:56,72 --> 0:20:1,16
And still CPU 100% and everything
is down because a lot of work

383
0:20:1,16 --> 0:20:1,86
is happening.

384
0:20:1,92 --> 0:20:6,46
So statement timeout doesn't protect
you, even 15 seconds.

385
0:20:6,46 --> 0:20:9,86
I mean, to start protecting, maybe
I would like to have it 1

386
0:20:9,86 --> 0:20:10,36
second.

387
0:20:10,58 --> 0:20:13,22
In this case, I think it will start
protecting.

388
0:20:15,52 --> 0:20:16,02
Michael: Interesting.

389
0:20:16,56 --> 0:20:18,36
Nikolay: But 30 seconds is too high.

390
0:20:19,86 --> 0:20:22,04
Michael: Yeah, it's a tricky one,
right?

391
0:20:22,04 --> 0:20:26,38
Because yeah, I think there's a
growing, and I know you mentioned

392
0:20:26,38 --> 0:20:30,8
analytical isn't our primary case,
but there's a growing trend

393
0:20:30,8 --> 0:20:32,7
that I'm seeing of kind of hybrid.

394
0:20:33,04 --> 0:20:37,36
I've had it called HTAP databases
where people don't want to

395
0:20:37,36 --> 0:20:41,98
spin up a second analytical, like
a warehouse or something, and they're

396
0:20:41,98 --> 0:20:47,06
trying to run some small analytical
queries on their like OLTP

397
0:20:47,26 --> 0:20:51,38
database, which means you do get
these kind of the odd several

398
0:20:51,38 --> 0:20:55,34
second query or at least hundreds
of milliseconds running

399
0:20:55,34 --> 0:20:56,52
on the same database.

400
0:20:56,6 --> 0:20:59,76
So I can see how it could get tricky
for some folks.

401
0:20:59,76 --> 0:21:3,38
But I think that the 30 seconds
and the higher feels like a sane

402
0:21:3,74 --> 0:21:5,28
starting point, as you mentioned.

403
0:21:5,28 --> 0:21:8,6
And if you start to see some errors
from those, look into them.

404
0:21:8,6 --> 0:21:12,04
And if you don't, look into what
your slowest queries are and

405
0:21:12,04 --> 0:21:13,38
whether you can reduce it.

406
0:21:13,52 --> 0:21:16,9
Nikolay: In case of HTAP, I will
just have another database user

407
0:21:17,22 --> 0:21:21,48
specifically for long-running
queries. I will adjust this setting

408
0:21:21,82 --> 0:21:25,28
maybe setting it to a couple or maybe
5 minutes and allowing to

409
0:21:25,28 --> 0:21:26,64
last longer, that's it.

410
0:21:27,04 --> 0:21:29,34
Michael: Yeah, that's a good point
that I don't know if we mentioned

411
0:21:29,34 --> 0:21:33,24
yet that these can be set at the
role level, at a session level,

412
0:21:33,24 --> 0:21:34,02
and globally.

413
0:21:34,08 --> 0:21:36,76
Nikolay: Well, I never tried, but
statement timeout definitely

414
0:21:36,76 --> 0:21:38,16
can be set at user level.

415
0:21:38,16 --> 0:21:41,1
I think lock_timeout should
work as well.

416
0:21:41,28 --> 0:21:42,68
Worth checking by the way.

417
0:21:43,04 --> 0:21:45,66
Michael: I thought the doc said
that, but I was checking the...

418
0:21:45,66 --> 0:21:47,62
Because this is the development
doc.

419
0:21:47,86 --> 0:21:48,92
Nikolay: Maybe even it's...

420
0:21:49,2 --> 0:21:49,74
It should be

421
0:21:49,74 --> 0:21:50,26
Michael: in the

422
0:21:50,26 --> 0:21:51,68
Nikolay: testing tests maybe.

423
0:21:51,68 --> 0:21:52,44
Maybe it's...

424
0:21:52,54 --> 0:21:54,74
So I'm quite sure it should work,
yeah.

425
0:21:55,08 --> 0:21:55,82
So you...

426
0:21:56,04 --> 0:21:58,74
Anyway, we talk about statement
timeout right now.

427
0:21:58,74 --> 0:22:1,02
We don't have lock_timeout
until 17.

428
0:22:1,02 --> 0:22:4,2
So statement timeout can be set
for different users differently.

429
0:22:4,86 --> 0:22:11,2
And I also had a case when people
had very low global statement

430
0:22:11,2 --> 0:22:17,0
timeout, but then adjusted for people,
for human connections,

431
0:22:17,16 --> 0:22:19,44
they adjusted it to set to 0.

432
0:22:20,02 --> 0:22:21,36
Michael: How do you feel about
that?

433
0:22:21,6 --> 0:22:22,9
Nikolay: I feel this is dangerous.

434
0:22:23,74 --> 0:22:27,9
It's like someone can just run
something then drink some coffee

435
0:22:28,04 --> 0:22:30,36
and then go for some call.

436
0:22:31,16 --> 0:22:32,46
It's kind of dangerous.

437
0:22:34,62 --> 0:22:34,74
By the

438
0:22:34,74 --> 0:22:36,52
Michael: way, I've got bad news
for you.

439
0:22:37,66 --> 0:22:41,66
The warning seems to have been
copied over to transaction timeout

440
0:22:41,66 --> 0:22:42,84
documentation as well.

441
0:22:42,84 --> 0:22:46,12
And it says in the docs, in the
new docs, setting transaction

442
0:22:46,12 --> 0:22:48,94
timeout in postgresql.conf is not
recommended because it would

443
0:22:48,94 --> 0:22:50,14
affect all sessions.

444
0:22:50,82 --> 0:22:51,96
Nikolay: Yeah, actually it's a
good point.

445
0:22:51,96 --> 0:22:53,26
I need to raise this again.

446
0:22:53,26 --> 0:22:54,62
I don't agree with this.

447
0:22:54,68 --> 0:22:56,86
It's a bad, very bad statement.

448
0:22:57,28 --> 0:23:0,18
Michael: There's also 1 other note
in here that I had forgotten.

449
0:23:0,18 --> 0:23:3,04
I guess it's only a minor note,
but prepared transactions are

450
0:23:3,04 --> 0:23:4,58
not subject to this timeout.

451
0:23:4.76 --> 0:23:5.78
Thought worth mentioning.

452
0:23:6.68 --> 0:23:8.04
Nikolay: Yeah, it's so.

453
0:23:9.38 --> 0:23:10.44
Michael: So, yeah.

454
0:23:10.92 --> 0:23:12.04
Is there anything else?

455
0:23:12.7 --> 0:23:15.56
I guess the story for how this
came about is pretty interesting.

456
0:23:15.64 --> 0:23:18.84
The thread seems quite long, to
me at least.

457
0:23:18.84 --> 0:23:22.7
Nikolay: As usual, a lot of unpredictable
problems and then people

458
0:23:23.04 --> 0:23:25.26
seem to agree and then I think
okay, not 4.17.

459
0:23:25.52 --> 0:23:27.52
But it was a good surprise last
week, right?

460
0:23:27.52 --> 0:23:28.88
Or a couple of weeks ago.

461
0:23:30.3 --> 0:23:36.4
Yeah, I just wanted to say, if
our world survives, right?

462
0:23:37.4 --> 0:23:43.68
For folks who are listening to
this in 2025 or later, just start

463
0:23:43.68 --> 0:23:47.96
thinking about limiting at transaction
level first and then go

464
0:23:47.96 --> 0:23:48.46
down.

465
0:23:49.02 --> 0:23:52.98
This is the main advice, but for
others, limit the application

466
0:23:53.24 --> 0:23:56.28
side and limit statement amount
and idle transaction session

467
0:23:56.28 --> 0:24:0.04
timeout, of course, both are good
to have limited to below a minute

468
0:24:0.04 --> 0:24:1.8
maybe, at least 1 minute.

469
0:24:3.18 --> 0:24:8.68
I also saw people are afraid of
an idle transaction session

470
0:24:8.68 --> 0:24:10.24
timeout being low.

471
0:24:10.24 --> 0:24:11.68
I don't understand this.

472
0:24:12.34 --> 0:24:13.94
Put it very low, very low.

473
0:24:13.94 --> 0:24:16.86
Like if someone is not doing anything,
bye bye.

474
0:24:17.0 --> 0:24:21.9
OK, maybe for humans, you can limit
it to 1 or 2 minutes but

475
0:24:21.9 --> 0:24:26.4
still you're probably holding some
very important exclusive locks

476
0:24:26.4 --> 0:24:31.56
and blocking someone, it's not a
good idea so either commit or

477
0:24:31.56 --> 0:24:33.46
do something already, right or

478
0:24:33.46 --> 0:24:37.56
Michael: yeah the default
is no limit like there's absolutely

479
0:24:37.64 --> 0:24:38.72
no limit at all.

480
0:24:39.14 --> 0:24:39.74
So, any...

481
0:24:40.76 --> 0:24:42.88
Yeah, even if you're really...

482
0:24:43.18 --> 0:24:46.88
Even if you are concerned, what
would an hour limit look like?

483
0:24:46.88 --> 0:24:48.48
Several hour limit look like?

484
0:24:49.08 --> 0:24:52.32
Even that's going to be better
than none at all.

485
0:24:52.8 --> 0:24:56.64
So yeah, I like that advice for
people to be checking.

486
0:24:56.64 --> 0:24:58.28
Do you even have any of these limits
set?

487
0:24:58.28 --> 0:24:59.94
Because the default is not to.

488
0:25:0.06 --> 0:25:2.86
And even on a lot of cloud providers,
they don't set

489
0:25:2.9 --> 0:25:3.36
Nikolay: defaults for you.

490
0:25:3.36 --> 0:25:8.0
IGOR MINAROVICH-SHMAROVICH Yeah,
Nginx, if you work on web or

491
0:25:8.0 --> 0:25:13.64
mobile apps, Nginx, for example,
has a default proxy timeout limit

492
0:25:14.06 --> 0:25:15.34
of 60 seconds.

493
0:25:15.94 --> 0:25:18.54
This is a good starting point to
think about it.

494
0:25:19.54 --> 0:25:25.42
If 99% of your database work is
to serve HTTP requests, at least

495
0:25:25.6 --> 0:25:28.32
60 seconds should be there.

496
0:25:28.86 --> 0:25:30.52
And go down to details.

497
0:25:30.86 --> 0:25:32.52
Also, one more thing.

498
0:25:32.7 --> 0:25:34.7
Maybe some folks will be inspired.

499
0:25:35.32 --> 0:25:41.92
I'm not a good drawer of some pictures,
but imagine if we had

500
0:25:41.92 --> 0:25:47.72
a schema in documentation showing
the relationships between statement

501
0:25:48,66 --> 0:25:52,92
and breaks between statement and
some big transaction and timeouts

502
0:25:52,92 --> 0:25:54,12
will be visualized.

503
0:25:55,08 --> 0:25:58,74
So for better understanding, this
is transaction timeout, between

504
0:25:58,74 --> 0:26:1,48
them there are multiple statements,
they are also limited and

505
0:26:1,48 --> 0:26:2,42
breaks are limited.

506
0:26:2,64 --> 0:26:5,14
And also outside we have a session
timeout.

507
0:26:5,5 --> 0:26:8,04
So idle session, how is it called?

508
0:26:8,04 --> 0:26:8,94
Michael: Idle session timeout.

509
0:26:8,94 --> 0:26:9,84
We have both.

510
0:26:11,26 --> 0:26:13,04
Nikolay: No, session timeout, what
is it?

511
0:26:13,04 --> 0:26:13,32
I mean-

512
0:26:13,32 --> 0:26:15,52
Michael: We have idle in transaction
session timeout and we have

513
0:26:15,52 --> 0:26:16,62
idle session timeout.

514
0:26:16,72 --> 0:26:17,56
Nikolay: Idle session timeout.

515
0:26:17,56 --> 0:26:18,12
This is what...

516
0:26:18,12 --> 0:26:20,42
Like, it's outside of a transaction
also have limited.

517
0:26:21,6 --> 0:26:25,88
So all periods of workflow are
covered now.

518
0:26:25,92 --> 0:26:28,22
And to visualize this would be a
great idea.

519
0:26:28,82 --> 0:26:31,5
Documentation already has a couple
of pictures.

520
0:26:31,5 --> 0:26:32,66
It would be good picture to have.

521
0:26:32,66 --> 0:26:32,78
I

522
0:26:32,78 --> 0:26:35,16
Michael: think you'd be in the
first 10 still though if you got

523
0:26:35,16 --> 0:26:36,04
this one in.

524
0:26:36,28 --> 0:26:39,72
Nikolay: Right exactly, so maybe
someone will be inspired.

525
0:26:40,68 --> 0:26:41,12
Good.

526
0:26:41,12 --> 0:26:41,62
Michael: Nice.

527
0:26:42,04 --> 0:26:42,54
Yeah.

528
0:26:43,7 --> 0:26:44,78
Thanks so much Nikolay.

529
0:26:44,96 --> 0:26:45,5
Nikolay: Catch you next week.

530
0:26:45,5 --> 0:26:45,54
Bye.

531
0:26:45,54 --> 0:26:45,62
Catch you

532
0:26:45,62 --> 0:26:46,16
Michael: next week.

533
0:26:46,4 --> 0:26:46,65
Nikolay: Bye.
