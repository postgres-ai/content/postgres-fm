1
0:00:06,5 --> 0:00:09,4
Hello, welcome to PostgresFM episode
number 51.

2
0:00:09,96 --> 0:00:13,059999
And today I'm alone for the first time
doing it online.

3
0:00:13,219999 --> 0:00:16,58
I mean, I've done many things online
on PostgresTV.

4
0:00:17,4 --> 0:00:21,779999
If you only follow PostgresFM
podcast, I encourage you to check

5
0:00:21,779999 --> 0:00:24,439999
out our YouTube channel, PostgresTV
as well.

6
0:00:25,08 --> 0:00:30,52
Since we publish their podcast
episodes regularly, like weekly,

7
0:00:31,26 --> 0:00:33,58
And this is actually week number
51.

8
0:00:33,58 --> 0:00:34,3
It's insane.

9
0:00:34,3 --> 0:00:36,0
Next week we will have an anniversary.

10
0:00:37,04 --> 0:00:39,22
But also we have other sessions.

11
0:00:39,239998 --> 0:00:43,28
Sometimes we produce something
or we invite some guests and so

12
0:00:43,28 --> 0:00:43,78
on.

13
0:00:44,3 --> 0:00:47,14
But well, 51 is insane.

14
0:00:47,16 --> 0:00:53,4
It feels crazy to have one episode
each week, and this is a great

15
0:00:53,4 --> 0:00:53,9
place.

16
0:00:54,0 --> 0:00:57,88
But unfortunately, my co-host Michael
is right now on vacation,

17
0:00:57,88 --> 0:01:2,92
so it's very hard for me to choose
a topic, to coordinate everything.

18
0:01:3,28 --> 0:01:8,68
For me, it's easier to do it online
because I don't need to redo

19
0:01:8,76 --> 0:01:9,86
it and so on.

20
0:01:10,28 --> 0:01:17,78
But I apologize in advance for
some, some and sounds because

21
0:01:17,92 --> 0:01:20,92
usually Michael edits it very well.

22
0:01:20,94 --> 0:01:25,16
I don't know how it will sound
in the podcast, but I will do my best.

23
0:01:25,6 --> 0:01:30,24
So choosing topic today was hard,
as I said, but I went to Hacker

24
0:01:30,24 --> 0:01:30,74
News.

25
0:01:30,84 --> 0:01:33,54
I'm using usually Algolia search.

26
0:01:34,6 --> 0:01:36,42
I will provide the link below.

27
0:01:36,6 --> 0:01:42,24
So I'm using Algolia search and
I check just Postgres or PostgreSQL.

28
0:01:42,479996 --> 0:01:47,32
Unfortunately, this search considers
these words not synonyms.

29
0:01:48,34 --> 0:01:52,12
And this week was actually hot
in terms of Postgres topics on

30
0:01:52,12 --> 0:01:52,98
Hacker News.

31
0:01:54,02 --> 0:02:03,04
Many topics made it on the front
page, but the most popular one

32
0:02:03,04 --> 0:02:05,96
was about threads versus processes.

33
0:02:07,34 --> 0:02:11,54
Maybe you know Heikki after PGCon
started this discussion on the

34
0:02:11,54 --> 0:02:17,46
PostgreSQL hackers mailing list and
This topic became very popular,

35
0:02:18,06 --> 0:02:22,86
with more than 100 emails in the mailing
list itself, and on Hacker

36
0:02:22,86 --> 0:02:27,18
News more than 300, I think 400
maybe, comments.

37
0:02:27,7 --> 0:02:33,16
Of course the depth of discussions 
can be very different.

38
0:02:33,16 --> 0:02:34,94
But anyway, it's interesting to hear
a lot of opinions, but I'm not

39
0:02:35,82 --> 0:02:40,24
going to discuss it today, maybe
another time.

40
0:02:40,24 --> 0:02:43,4
And Next, the second most popular topic 
was about

41
0:02:43,78 --> 0:02:51,38
upgrades, major upgrades for
Postgres using logical replication,

42
0:02:51,38 --> 0:02:55,62
involving logical replication
to achieve 0 or near 0 downtime.

43
0:02:55,64 --> 0:02:59,14
This topic is very interesting
and I have a lot of things to

44
0:03:00,06 --> 0:03:04,24
say and to share, but maybe later
because there is some ongoing

45
0:03:04,24 --> 0:03:08,56
work which is not done yet.
Maybe later this year we will share

46
0:03:08,56 --> 0:03:10,32
something which will be very
interesting.

47
0:03:10,84 --> 0:03:15,8
So I chose the third topic.
The third topic was related to

48
0:03:15,8 --> 0:03:16,3
a blog post on Cybertech blog.

51
0:03:24,52 --> 0:03:28,02
by Anss Asma, and it was about UUID.

52
0:03:28,94 --> 0:03:36,5
It's a quite well-known problem
with traditional UUID, and it's

53
0:03:36,5 --> 0:03:37,48
related to performance.

54
0:03:37,96 --> 0:03:45,26
But this post was, as usual in
Cybertech blog, it was quite concrete

55
0:03:45,26 --> 0:03:50,52
in terms of practical examples
and numbers and so on.

56
0:03:50,66 --> 0:03:52,04
So it's worth discussing.

57
0:03:52,9 --> 0:03:58,22
But let me start from some history
and from some distance.

58
0:03:59,54 --> 0:04:05,5
When I co-founded my first social
network and chose Postgres

59
0:04:05,5 --> 0:04:06,6
for the first time.

60
0:04:06,6 --> 0:04:11,04
I started it on using MySQL, but
it took me just 1 week to realize

61
0:04:11,04 --> 0:04:14,7
that I'm not going to stay on MySQL
because it was a terrible

62
0:04:14,7 --> 0:04:15,2
time.

63
0:04:15,36 --> 0:04:21,44
When MySQL was version 3 and so
on and so on, not following what

64
0:04:21,44 --> 0:04:23,36
we learned at the university.

65
0:04:23,5 --> 0:04:27,04
So I quickly chose Postgres and
converted very quickly.

66
0:04:27,72 --> 0:04:33,3
And we had interesting, it was
2005 or so, very long ago.

67
0:04:33,9 --> 0:04:38,7
And we decided that we need to
hide real numbers from our competitors.

68
0:04:40,08 --> 0:04:45,48
And we wanted to, you know, like
when you decide to use surrogate

69
0:04:45,54 --> 0:04:54,26
key, you probably see, you probably
let your users see current

70
0:04:54,26 --> 0:04:58,2
number, for example, you assign
it sequentially using...

71
0:04:58,74 --> 0:05:00,9
Back that time, we didn't have
generators.

72
0:05:01,08 --> 0:05:06,88
We had only serial or big serial
data types, sequences, right?

73
0:05:07,78 --> 0:05:14,34
And any user when user is registered
is assigned, we have a number

74
0:05:14,38 --> 0:05:18,3
assigned and user can guess how
many other users already registered.

75
0:05:18,44 --> 0:05:22,36
And if this is your competitor,
they probably will start checking

76
0:05:22,36 --> 0:05:23,34
you every week.

77
0:05:23,72 --> 0:05:29,98
And they will see the growth, like
rates of your growth, and

78
0:05:30,06 --> 0:05:34,3
not only about users, but also
some blog posts, comments, everything.

79
0:05:34,9 --> 0:05:37,08
We decided that we want to hide
it.

80
0:05:37,2 --> 0:05:38,92
An interesting trick was chosen.

81
0:05:38,92 --> 0:05:43,14
It was not my idea, but I implemented
it quickly using simple

82
0:05:43,14 --> 0:05:44,32
defaults and sequences.

83
0:05:44,72 --> 0:05:53,4
So what we did, we wanted some
random numbers, but not real random

84
0:05:53,4 --> 0:05:53,8
numbers.

85
0:05:53,8 --> 0:05:58,58
So if you take 2 mutually prime
numbers and just multiply sequence

86
0:05:58,58 --> 0:06:05,9
value by 1 of them, and then use
modulo operation with another

87
0:06:05,9 --> 0:06:14,32
number, you will have like looking
like kind of random numbers,

88
0:06:14,54 --> 0:06:18,42
but it will not be really random
numbers and it will be extremely

89
0:06:18,48 --> 0:06:23,34
hard even if probably not possible
to understand, not knowing

90
0:06:23,34 --> 0:06:27,88
these 2 mutually prime numbers
to understand what is happening.

91
0:06:27,88 --> 0:06:29,86
So it will look like some random
numbers.

92
0:06:29,86 --> 0:06:33,04
Of course 1 of these 2 mutually
prime numbers should be very

93
0:06:33,04 --> 0:06:34,84
large, a very big number.

94
0:06:34,84 --> 0:06:42,04
So you have like ring or circle
and you're walking on this circle.

95
0:06:42,44 --> 0:06:45,14
So what we did, it was great.

96
0:06:45,28 --> 0:06:46,38
It was working great.

97
0:06:46,38 --> 0:06:52,26
We used this approach for all our
surrogate primary keys, and

98
0:06:52,68 --> 0:06:53,36
that's it.

99
0:06:53,36 --> 0:06:59,28
So it was a single primary, then
we dealt with various scalability

100
0:06:59,44 --> 0:07:02,38
issues using Slony, then Londiste,
and so on.

101
0:07:02,5 --> 0:07:07,98
But in the second social network
I co-founded a few years later,

102
0:07:08,56 --> 0:07:12,18
we used the same approach, and
then we ended up having a real

103
0:07:12,18 --> 0:07:13,94
nasty problem for the first time.

104
0:07:13,94 --> 0:07:16,74
It was 2008, I remember it.

105
0:07:16,84 --> 0:07:22,00
And we solved the problem when
you reach 2.1 billion records,

106
0:07:22,5 --> 0:07:27,1
if you use int4 primary key,
you don't...

107
0:07:28,28 --> 0:07:33,62
At some point, when your table
is growing very fast, 2.1 billion

108
0:07:33,62 --> 0:07:34,12
rows.

109
0:07:34,46 --> 0:07:37,62
I mean, not rows, but value 2.1
billion.

110
0:07:37,86 --> 0:07:41,02
You cannot insert, Postgres cannot
insert anymore, and you have

111
0:07:43,44 --> 0:07:45,36
a very big problem to solve.

112
0:07:45,90 --> 0:07:50,14
To solve it synchronously,
you need to have some downtime,

113
0:07:50,34 --> 0:07:51,96
many hours of downtime, unfortunately.

114
0:07:52,42 --> 0:07:58,18
So since then, I learned you never
should use int4 primary

115
0:07:58,18 --> 0:07:58,68
keys.

116
0:07:58,94 --> 0:08:04,42
What's interesting here is, we
used some kind of random

117
0:08:05,28 --> 0:08:06,64
distribution for numbers.

118
0:08:07,42 --> 0:08:11,28
And at that time, we didn't realize
that it's not good in terms

119
0:08:11,28 --> 0:08:11,92
of performance.

120
0:08:12,34 --> 0:08:13,94
Why it's not good in terms of performance?

121
0:08:13,94 --> 0:08:18,66
Because usually in social
networks, social media, most

122
0:08:18,66 --> 0:08:24,56
of the time you select users, and
you select from your database

123
0:08:25,24 --> 0:08:30,12
the most recent data.

124
0:08:30,56 --> 0:08:35,5
For example, provide the last 25
comments or 100 comments for

125
0:08:35,5 --> 0:08:41,7
this post, or show the freshest,
most interesting posts, and so on.

126
0:08:42,7 --> 0:08:47,8
And if you use kind of random numbers
or something that looks random

127
0:08:47,8 --> 0:08:54,24
but not really random numbers, you
end up using different pages

128
0:08:54,24 --> 0:08:56,78
for each insert in terms of heap.

129
0:08:56,8 --> 0:09:00,48
But not only in terms of heap,
but also for indexes. Because

130
0:09:00,48 --> 0:09:04,92
if you use an index, probably by
default, it's B-tree, you

131
0:09:04,92 --> 0:09:08,86
end up inserting in different subtrees
in B-tree.

132
0:09:09,62 --> 0:09:12,38
So sometimes it's good, but not
always.

133
0:09:12,4 --> 0:09:19,56
If you have just a single node and
your usage pattern is more like needing fresh

134
0:09:20,28 --> 0:09:22,62
data more often than
old data.

135
0:09:23,04 --> 0:09:24,74
In this case, it's not optimal.

136
0:09:26,32 --> 0:09:28,78
And we didn't realize that at that
time.

137
0:09:28,78 --> 0:09:32,94
And then we also saw our competitors
didn't care anymore about

138
0:09:33,82 --> 0:09:35,46
hiding some numbers and so on.

139
0:09:35,46 --> 0:09:37,72
And at some point I decided also
not...

140
0:09:37,74 --> 0:09:41,68
And my third social network, and
since then I don't care about

141
0:09:41,68 --> 0:09:46,58
masking real numbers and I just use...

142
0:09:46,58 --> 0:09:51,08
I used int8 primary keys and
all was good.

143
0:09:51,72 --> 0:09:58,58
But now we live in a world where
we use many, many, many nodes.

144
0:10:00,04 --> 0:10:05,38
For example, just yesterday I needed
to compare the behavior of 2

145
0:10:05,38 --> 0:10:11,24
major Postgres versions, and I
needed to dump some samples and

146
0:10:11,24 --> 0:10:15,56
plans and so on, and then I needed
to analyze plans' behavior,

147
0:10:16,22 --> 0:10:21,36
explain analyze buffers on 2 nodes,
and then to compare.

148
0:10:21,54 --> 0:10:27,94
And obviously, if I used just regular
int8 and sequentially

149
0:10:28,04 --> 0:10:32,66
assigned, I would have collisions
when merging data from 2 nodes.

150
0:10:32,66 --> 0:10:38,14
Because, for example, ID 1 is used
on both servers, so we cannot

151
00:10:38,14 --> 00:10:38,94
Merge this data.

152
00:10:38,94 --> 00:10:42,32
And it's like, it's solvable, but
it's not interesting.

153
00:10:42,38 --> 00:10:43,7
So I just used UUID.

154
00:10:44,76 --> 00:10:49,24
You install extension and you start
using UUID and you know collisions

155
00:10:49,24 --> 00:10:52,62
are extremely rare, so it's all
good.

156
00:10:53,26 --> 00:10:59,44
But regular approach, when you,
so-called traditional UUID, so-called

157
00:10:59,58 --> 00:11:06,64
version 4, it
behaves like randomly distributed.

158
00:11:07,46 --> 00:11:12,94
In this case, you know that collisions
are very unlikely.

159
00:11:13,38 --> 00:11:17,38
It's good to use it as a global
value.

160
00:11:17,92 --> 00:11:23,1
But since it's randomly distributed,
it's going to have not good

161
00:11:23,94 --> 00:11:28,04
state of B-tree, because when you
need again, like you need to

162
00:11:28,78 --> 00:11:35,1
check your latest results, you
will use a lot of buffers.

163
00:11:35,6 --> 00:11:41,9
And this particular post in CyberTech
blog discusses this problem.

164
00:11:42,18 --> 00:11:45,82
But it goes into an interesting
direction which probably was

165
00:11:45,82 --> 00:11:48,42
not explored by others before.

166
00:11:48,7 --> 00:11:54,18
It discusses the performance of
aggregates of count and index-only

167
00:11:54,2 --> 00:11:54,7
scans.

168
00:11:55,24 --> 00:12:01,24
So with index-only scans we utilize
visibility maps and of course

169
00:12:01,24 --> 00:12:06,8
in this post the author performed
a vacuum analyze and so visibility

170
00:12:06,94 --> 00:12:11,78
map was fresh, no heap fetches
involved, only work with index.

171
00:12:12,08 --> 00:12:15,96
So select count, blah blah blah,
and you check some fresh count

172
00:12:15,96 --> 00:12:16,185
of fresh records.

173
00:12:16,185 --> 00:12:17,06
Fresh, a count of fresh records.

174
00:12:17,66 --> 00:12:20,28
And the author compared 3 cases.

175
00:12:20,9 --> 00:12:26,02
Integer 8, UUID version 4, traditional
UUID most people use.

176
00:12:26,64 --> 00:12:31,48
In Postgres, this is what we
will use if you decide to use

177
00:12:31,48 --> 00:12:36,44
UUID, provided by Postgres itself,
by extension basically.

178
00:12:37,96 --> 00:12:44,4
And third option was a new kind
of UUID, so-called version 7,

179
00:12:44,64 --> 00:12:49,44
which is not standard yet, but
it is a work in progress standard

180
00:12:49,94 --> 00:12:51,1
being reviewed right now.

181
00:12:52,2 --> 00:12:55,94
And this UUID version 7 has interesting
property.

182
00:12:56,48 --> 00:13:03,22
Collisions are also extremely unlikely,
but values are generated

183
00:13:03,96 --> 00:13:08,8
in a kind of sequential like order.

184
00:13:08,8 --> 00:13:13,58
So if we order values generated,
so basically timestamp is involved

185
00:13:13,94 --> 00:13:14,94
under the hood.

186
00:13:15,16 --> 00:13:21,06
And if you generate values, you
can order by them and it will

187
00:13:21,06 --> 00:13:22,68
have correlation with time.

188
00:13:23,4 --> 00:13:29,18
So it keeps this good property
to be, like, you can use it globally,

189
00:13:29,28 --> 00:13:34,36
you can have many, many primary
servers generating this value,

190
00:13:34,6 --> 00:13:40,28
collisions almost in your life
will never happen, but you can

191
00:13:40,28 --> 00:13:45,22
order by this value and it will
be the same if you would order

192
00:13:45,52 --> 00:13:49,2
by created at column or any column.

193
00:13:49,94 --> 00:13:55,28
So similar to regular surrogate
integer 8 primary keys.

194
00:13:55,84 --> 00:14:01,1
And interesting result was that
if you create indexes, of course,

195
00:14:01,1 --> 00:14:07,12
B-tree indexes for all 3 options,
And then you, first of all, if

196
00:14:07,12 --> 00:14:11,06
you check, like, first thing interesting
was, like, I didn't

197
00:14:11,06 --> 00:14:12,94
pay attention to it, but it's interesting.

198
00:14:13,48 --> 00:14:18,7
Insert time itself already, where
version 4 traditional UUID

199
00:14:18,7 --> 00:14:19,4
is losing.

200
00:14:19,66 --> 00:14:20,32
It's interesting.

201
0:14:20,32 --> 0:14:24,36
It's not losing too much, but it's
already losing in terms of

202
0:14:24,36 --> 0:14:25,22
insert time.

203
0:14:28,36 --> 0:14:32,44
And then if you check SELECT COUNT,
this is the most interesting

204
0:14:32,44 --> 0:14:32,94
part.

205
0:14:32,98 --> 0:14:39,82
So timing for version 4 was much
worse, but the state was warmed,

206
0:14:40,24 --> 0:14:43,22
caches were warmed, so everything
was a buffer hit.

207
0:14:43,78 --> 0:14:48,48
And then, the author checks,
EXPLAIN plans to understand

208
0:14:48,48 --> 0:14:50,86
why it's slower, why version 4
is slower.

209
0:14:51,02 --> 0:14:55,82
As for int8 and UUID version
7, quite close.

210
0:14:56,0 --> 0:14:56,76
Why so?

211
0:14:57,44 --> 0:15:0,92
And there I expected to see it
and I saw it.

212
0:15:0,92 --> 0:15:6,92
I saw buffer's option used in EXPLAIN ANALYZE
analysis.

213
0:15:9,0 --> 0:15:14,12
We discussed it many, many times,
and I will repeat it many more

214
0:15:14,12 --> 0:15:14,62
times.

215
0:15:14,82 --> 0:15:17,74
Buffers should always be used when
we analyze plans.

216
0:15:17,86 --> 0:15:21,3
It should be default, but unfortunately,
there were 2 attempts

217
0:15:21,44 --> 0:15:26,74
to make it default during the last
few years, but unfortunately,

218
0:15:26,82 --> 0:15:31,16
both attempts failed and both hackers
decided not to continue.

219
0:15:33,34 --> 0:15:37,06
I think everyone agrees it should
be default, but somehow it's

220
0:15:37,06 --> 0:15:40,46
related to tests, Postgres,
automated tests Postgres has

221
0:15:40,46 --> 0:15:41,12
and so on.

222
0:15:41,12 --> 0:15:42,24
It's hard to implement.

223
0:15:42,94 --> 0:15:48,96
But I think if someone wants this
goal to be achieved, it's a great

224
0:15:48,96 --> 0:15:53,16
goal to make buffers option default
in EXPLAIN ANALYZE.

225
0:15:54,06 --> 0:16:0,98
So when we check buffers, surprise,
we see that for int4,

226
0:16:1,16 --> 0:16:6,76
for int8 and UUID version
7, it's almost the same in

227
0:16:6,76 --> 0:16:7,66
terms of buffers.

228
0:16:7,72 --> 0:16:10,46
This explains the similar timing.

229
0:16:11,18 --> 0:16:17,86
But for UUID version 4, it's almost
two orders of magnitude more

230
0:16:18,08 --> 0:16:18,98
buffer hits.

231
0:16:19,64 --> 0:16:23,68
And then Ofler explains why it's
related to visibility maps.

232
0:16:24,44 --> 0:16:30,42
The need for Postgres to check
many more pages, many more buffers,

233
0:16:30,72 --> 0:16:35,42
because we don't have data locality
anymore inside visibility

234
0:16:35,74 --> 0:16:36,5
maps as well.

235
0:16:36,5 --> 0:16:39,32
And we keep checking many, many,
many buffers.

236
0:16:40,08 --> 0:16:41,5
So this is super interesting.

237
0:16:41,98 --> 0:16:49,2
And it means not only for fetching
fresh data, sequential generation

238
0:16:49,5 --> 0:16:57,16
of surrogate keys, either integers
or complex values like UUID

239
0:16:57,38 --> 0:16:58,0
is better.

240
0:16:58,0 --> 0:17:2,42
But also for aggregates and counting,
you know, Postgres has

241
0:17:2,42 --> 0:17:3,16
a slow COUNT.

242
0:17:3,16 --> 0:17:4,54
It's always a problem.

243
0:17:4,54 --> 0:17:5,94
Like many people complain.

244
0:17:6,18 --> 0:17:10,7
It was improved with,
the improvement of index only scans.

245
0:17:10,84 --> 0:17:14,58
If you have autovacuum tuned,
index only scans perform

246
0:17:14,58 --> 0:17:18,9
quite well and counting is faster,
but still, this is an interesting

247
0:17:18,9 --> 0:17:19,4
example.

248
0:17:19,4 --> 0:17:27,02
So the bad recipe, forget about
autovacuum tuning and use UUID

249
0:17:27,1 --> 0:17:27,98
version 4.

250
0:17:28,18 --> 0:17:31,16
And you will end up having a very
slow count.

251
00:17:31,96 --> 00:17:38,04
So, a good advice is to not use version 4 UUID. Instead, use version

252
00:17:38,04 --> 00:17:43,1
7, which is not official yet, but you can write a simple function

253
00:17:43,1 --> 00:17:43,62
for that.

254
00:17:43,62 --> 00:17:48,76
It's not a big deal, you can find many examples on the internet, or you

255
00:17:48,76 --> 00:17:51,42
can even generate it on the application side.

256
00:17:52,04 --> 00:17:56,1
And then also keep your visibility maps up to date.

257
00:17:56,16 --> 00:17:59,24
For that, you need to fine-tune the autovacuum process, as autovacuum

258
00:17:59,24 --> 00:18:0,66
updates visibility maps.

259
00:18:0,92 --> 00:18:6,84
A visibility map remembers which page has all visible tuples.

260
00:18:7,26 --> 00:18:13,3
So, a page contains many tuples and a page that's fully visible means that

261
00:18:13,62 --> 00:18:17,28
we don't have any transactions that would need some tuple which

262
00:18:17,28 --> 00:18:18,22
might be dead, right?

263
00:18:18,22 --> 00:18:21,24
So all tuples are visible to all transactions.

264
00:18:21,96 --> 00:18:27,84
This means that if you make some recent delete or update,

265
00:18:28,66 --> 00:18:33,52
you produce some dead tuple, then autovacuum needs to clean

266
00:18:33,52 --> 00:18:38,3
it up. But also, in the visibility map, the all-visible flag

267
00:18:38,3 --> 00:18:41,92
will be set to false, meaning that in this page, we probably

268
00:18:41,92 --> 00:18:46,0
have some tuples which are visible to some transactions, not

269
00:18:46,0 --> 00:18:50,74
visible to some other transactions. But if you keep autovacuum

270
00:18:50,74 --> 00:18:56,7
settings quite aggressive, autovacuum is processing tables often.

271
00:18:57,1 --> 00:19:2,16
In this case, you will see fewer heap fetches in the plan for

272
00:19:2,16 --> 00:19:4,62
an index-only scan and count will be faster.

273
00:19:4,92 --> 00:19:8,0
But also, you need sequential ID allocation.

274
00:19:8,5 --> 00:19:13,4
So, UUID version 7 is looking very good.

275
00:19:14,38 --> 00:19:17,92
On a funny note, a few months ago on Postgres TV, we had a

276
00:19:17,92 --> 00:19:21,26
session with Andrei Borodin and Kirk Volokh.

277
00:19:22,2 --> 00:19:27,26
And it was Kirk's idea to work on the implementation of first,

278
00:19:27,26 --> 00:19:32,3
it was called UUID. But then we learned that there is a standard

279
00:19:32,78 --> 00:19:37,54
proposal, which describes UUID version 7.

280
00:19:37,8 --> 00:19:41,18
So Andrei implemented it and sent it to the hackers list.

281
00:19:41,18 --> 00:19:45,3
You can find it on Postgres TV's YouTube channel.

282
00:19:45,78 --> 00:19:52,2
We had good feedback that it's indeed a feature which should

283
00:19:52,2 --> 00:19:53,0
be in Postgres.

284
00:19:53,76 --> 00:19:59,24
But Peter Eisentraut noted that the standard is not finalized,

285
00:19:59,26 --> 00:20:0,9
it's not approved yet.

286
00:20:1,5 --> 00:20:4,48
Before it is approved, it's not good to implement it.

287
00:20:4,48 --> 00:20:5,96
We should wait until it's approved.

288
00:20:6,22 --> 00:20:10,84
And then one of the authors of the new version of the standard popped

289
00:20:10,84 --> 00:20:15,32
up in the hackers' mailing list, responding that it's great that

290
00:20:15,32 --> 00:20:18,78
we're doing it and provided us with some details.

291
00:20:19,02 --> 00:20:20,72
So, it was good work.

292
00:20:20,98 --> 00:20:27,0
Anyway, I checked the state of the standard and on June 10th,

293
00:20:29,18 --> 00:20:34,04
this proposal changed its state and now it's waiting for a new

294
00:20:34,04 --> 00:20:34,54
review.

295
00:20:35,08 --> 00:20:40,22
It looks like probably later this year, this standard update,

296
00:20:40,84 --> 00:20:46,3
standard change, RFC, I don't remember the number, can be approved.

297
00:20:46,52 --> 00:20:51,72
And if it's approved, I think it will not be a big deal to finalize

298
00:20:51,72 --> 00:20:58,62
the patch and in that case, it will probably make its way into Postgres

299
00:20:58,62 --> 00:20:59,12
17.

300
00:20:59,18 --> 00:21:1,44
If not, okay, then Postgres 18.

301
00:21:01,68 --> 00:21:03,07
But it's not a huge patch, right?

302
00:21:03,07 --> 00:21:08,76
So once the standard is approved,
I hope PostgreSQL will have it.

303
00:21:09,44 --> 00:21:10,82
That's probably it.

304
00:21:11,76 --> 00:21:13,68
Again, it was hard for me to be
alone.

305
00:21:13,68 --> 00:21:16,54
I hope next week I won't be alone.

306
00:21:17,36 --> 00:21:19,94
If I will, it will be an anniversary.

307
00:21:19,94 --> 00:21:20,78
It's interesting.

308
00:21:20,86 --> 00:21:22,42
We should do something special.

309
00:21:22,42 --> 00:21:26,58
If you have some questions, probably
we should have some session

310
00:21:26,58 --> 00:21:30,68
of Q&A, like an ask me anything
session.

311
00:21:30,92 --> 00:21:36,16
So send me anything to Twitter,
to YouTube, anywhere, and we

312
00:21:36,16 --> 00:21:38,66
will probably work on those questions.

313
00:21:40,16 --> 00:21:44,76
And in 2 weeks, Michael will return
and it will be a normal podcast.

314
00:21:44,92 --> 00:21:49,54
Sorry if I was not like, I was
doing a lot because there's no

315
00:21:49,54 --> 00:21:51,06
editing this time.

316
00:21:51,48 --> 00:21:53,04
Okay, thank you so much.

317
00:21:53,94 --> 00:22:00,94
Please share this episode on your
social networks and working

318
00:22:00,94 --> 00:22:05,08
groups and so on. Let me
know what you want next.
